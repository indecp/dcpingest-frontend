/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

export const ROLE = {
  1: 'admin',
  2: 'distributor',
  3: 'exhibitor'

};


export class User {
  id: number;
  login: string;
  name: string;
  role: number;
  email: string;
  URL: string;
  token: string;
  distributor: any;
  ingester: any;


 constructor(data) {
    Object.assign(this, data);
  }

  isAdministrator() {
    return this.role === 1;
  }

  isDistributor() {
    return this.role === 2;
  }

  isExhibitor() {
    return this.role === 3;
  }

  getRoleTxt() {
    return [this.role];
  }

}
