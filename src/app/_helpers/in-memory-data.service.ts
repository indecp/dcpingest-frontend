/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { InMemoryDbService } from 'angular-in-memory-web-api';

import { Movie } from '../_models';
import { Dcp } from '../_models';
import { Distributor } from '../_models';
import { Exhibitor } from '../_models';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {

    const movies = [

      {
        id: 11,
        title: 'Vertigo',
        synopsis: 'Scottie, inspecteur de police, a été limogé parce qu il est sujet au vertige. Un de ses vieux amis le charge de surveiller sa très belle femme, Madeleine, dont le comportement étrange lui fait craindre qu elle ne se suicide. Scottie la prend en filature, la sauve d une noyade volontaire puis s éprend d elle. Cependant, en raison de sa peur du vide, il ne parvient pas à l empêcher de se précipiter du haut d un clocher.',
        dcps : [
          { dcpid : 1},
          { dcpid : 2}
        ],
        distrib: [
          { distributorid: 2}
        ]

      },

      {
        id: 12,
        title: 'Pulp Fiction',
        synopsis: 'L odyssée sanglante et burlesque de petits malfrats dans la jungle de Hollywood à travers trois histoires qui s entremêlent. Dans un restaurant, un couple de jeunes braqueurs, Pumpkin et Yolanda, discutent des risques que comporte leur activité. Deux truands, Jules Winnfield et son ami Vincent Vega, qui revient d Amsterdam, ont pour mission de récupérer une mallette au contenu mystérieux et de la rapporter à Marsellus Wallace.',
        director: 'Quentin Tarantino',
        scenario: 'Quentin Tarantino et Roger Avary',
        actor : 'John Travolta, Samuel L.Jackson, Uma Thurman, Bruce Willis',
        country: 'Américan',
        dcps: [
          { dcpid : 3},
          { dcpid : 4},
          { dcpid : 5},
        ],
        distrib: [
          { distributorid: 1}
        ]
      },

      {
        id: 13,
        title: 'Reservoirs Dogs',
        synopsis: 'Six truands désignés par des noms de code entreprennent de dévaliser une importante bijouterie sous la direction du tyrannique Joe Cabot et de son fils Nice Guy Eddie. L opération échoue malencontreusement et se solde par un bain de sang. Les malfrats se réfugient alors dans un entrepôt sordide. Mr Orange est grièvement blessé. Mr Pink, quant à lui, est d avis qu il existe un traître parmi eux. Mr Blonde, un psychopathe, entend bien faire parler le policier qu il a réussi à capturer.',
        dcps: [
          { dcpid : 7},
          { dcpid : 8},
          { dcpid : 9},
        ],
        distrib: [
          { distributorid: 1}
        ]
      },

      {
        id: 14,
        title: 'STAR WARS EPISODE IV',
        synopsis: 'Il y a bien longtemps, dans une galaxie très lointaine. La guerre civile fait rage entre l Empire galactique et l Alliance rebelle. Capturée par les troupes de choc de l Empereur menées par le sombre et impitoyable Dark Vador, la princesse Leia Organa dissimule les plans de l Etoile Noire, une station spatiale invulnérable, à son droïde R2-D2 avec pour mission de les remettre au Jedi Obi-Wan Kenobi.',
        dcps: [
          { dcpid : 10},
          { dcpid : 11},
          { dcpid : 12},
        ],
        distrib: [
          { distributorid: 3}
        ]
      },

      {
        id: 15,
        title: 'Fours Lions',
        synopsis: 'Animé par des envies de grandeur, Omar est déterminé à devenir un soldat du djihad en Angleterre. Avec ses amis, il décide de monter le coup décisif qui fera parler d eux et de leur cause. Problème : il leur manque le mode d emploi.',
        distributorid : 1
      },

      {
        id: 16,
        title: 'Gouffre au chimère',
        synopsis: 'Journaliste sans scrupules, Charles Tatum a connu quelques déboires à New York. Sans travail, alcoolique, il trouve enfin une occasion de renouer avec le métier. Il est embauché au Sun Bulletin d Albuquerque, au Nouveau-Mexique. Parti avec Herbie Cook, un jeune photographe, pour faire un reportage sur la chasse aux serpents à sonnette, il s arrête en cours de route à Escudero, où un homme vient d être enseveli par un éboulement dans une grotte.',
        distributorid : 2
      },

      {
        id: 17,
        title: 'Le quatuor à cornes',
        synopsis: 'Au cours d un périple fantaisiste jusqu au bord de la mer, quatre vaches vont découvrir la liberté dans le monde inconnu qui s étend au-delà de leur pré. Cette odyssée riche en aventure, en rencontres burlesques, disputes, réconciliations et découvertes, confronte chacune à ce qui pouvait lui arriver de pire, et transforme le troupeau initial, trop lié par l habitude, en une irréductible bande d amies, solidaires et affranchies.',
        distributorid : 3
      },

      {
        id: 18,
        title: 'Sauvage',
        synopsis: 'Léo, 22 ans, se vend dans la rue pour un peu d’argent. Les hommes défilent. Lui reste là, en quête d’amour. Il ignore de quoi demain sera fait. Il s’élance dans les rues. Son cœur bat fort. ',
        distributorid : 2
      },

      {
        id: 19,
        title: 'Rafiki',
        synopsis: 'À Nairobi, Kena et Ziki mènent deux vies de jeunes lycéennes bien différentes, mais cherchent chacune à leur façon à poursuivre leurs rêves. Leurs chemins se croisent en pleine campagne électorale au cours de laquelle s’affrontent leurs pères respectifs. Attirées l’une vers l’autre dans une société kenyane conservatrice, les deux jeunes femmes vont être contraintes de choisir entre amour et sécurité...',
        distributorid : 3
      }
    ];


    const dcps  = [

      {
        id: 1,
        dcpname: 'VERTIGO_TLR',
        contentkind: 'TLR',
        size: 1500000,
        movie: [
          { movieid : 11},
        ],
      },

      {
        id: 2,
        dcpname: 'VERTIGO_FTR',
        contentkind: 'FTR',
        size: 130000000,
        movie: [
          { movieid : 11},
        ],
      },

      {
        id: 3,
        dcpname: 'PULP_FICTION_FTR',
        contentkind: 'FTR',
        size: 140000000,
        movie: [
          { movieid : 12},
        ],
        distrib: [
          { distributorid: 1}
        ]
      },

      {
        id: 4,
        dcpname: 'PULP_FICTION_TLR',
        contentkind: 'TLR',
        size: 12000000,
        movie: [
          { movieid : 12},
        ],
        distrib: [
          { distributorid: 1}
        ]
      },

      {
        id: 5,
        dcpname: 'PULP_FICTION_FTR_OCAP',
        contentkind: 'FTR',
        size: 2000000,
        movie: [
          { movieid : 12},
        ],
        distrib: [
          { distributorid: 1}
        ]
      },

      {
        id: 6,
        dcpname: 'SISTER_BOTHERS',
        contentkind: 'FTR',
        size: 240000000
      },

      {
        id: 7,
        dcpname: 'RESERV_DOGS_FTR',
        contentkind: 'FTR',
        size: 240000000,
        movie: [
          { movieid : 13},
        ],
        distrib: [
          { distributorid: 1}
        ]
      },

      {
        id: 8,
        dcpname: 'RESERV_DOGS_TLR_1',
        contentkind: 'TLR',
        size: 320000,
        movie: [
          { movieid : 13},
        ],
        distrib: [
          { distributorid: 1}
        ]
      },

      {
        id: 9,
        dcpname: 'RESERV_DOGS_TLR_2',
        contentkind: 'TLR',
        size: 320000,
        movie: [
          { movieid : 13},
        ],
        distrib: [
          { distributorid: 1}
        ]
      },

      {
        id: 10,
        dcpname: 'STARWARS_IV_TLR_1',
        contentkind: 'TLR',
        size: 1500000,
        movie: [
          { movieid : 14},
        ],
        distrib: [
          { distributorid: 3}
        ]
      },

      {
        id: 11,
        dcpname: 'STARWARS_IV_TLR21',
        contentkind: 'TLR',
        size: 1100000,
        movie: [
          { movieid : 14},
        ],
        distrib: [
          { distributorid: 3}
        ]
      },

      {
        id: 12,
        dcpname: 'STARWARS_IV_FTR',
        contentkind: 'FTR',
        size: 130000000,
        movie: [
          { movieid : 14},
        ],
        distrib: [
          { distributorid: 3}
        ]
      }
		];

    const tdistributors = [

  {
    id: 1,
    distributorname: 'MIRAMAX Distribution',
    partners: 'YES',
    mail: 'miramax@contact.com',
    user_contact: 'John Doe',
    number_contact: '06 00 00 00 00',
    CNC_Code: 4666,
    user_Id: 'MiramaxDistribution',
    mail_as_user_id : 'miramaxdistribution@contact.com'
  },


  {
    id: 2,
    distributorname: 'Paramont Pictures',
    partners: 'NO',
    mail: 'paramont@contact.com',
    user_contact: 'M. Paramont',
    number_contact: '06 06 07 06 66',
    CNC_Code: 6879,
    user_Id: 'ParamontPictures',
    mail_as_user_id : 'paramontpictures@contact.com'
  },


  {
    id: 3,
    distributorname: 'Lucas Films',
    partners: 'NO',
    mail: 'lucasfilms@contact.com',
    user_contact: 'JJ Abrams',
    number_contact: '07 89 12 06 66',
    CNC_Code: 2456,
    user_Id: 'LucasProd',
    mail_as_user_id : 'lucasfilms-abrams@contact.com'
  }


  ];

    const texhibitors  = [

  {
    id : 1,
    name :  'Stars',
    CNC_Code_autorised : 65841,
    log : 'stars41',
    pass : 'Liberty_Star41',
    mail: 'strars41@contact.com',
    name_contact : 'John Doe',
    number_contact: '06 00 00 00 00',
    address_contact: '123 rue liberty',
    ZIP_Code : 6689,
    town : 'LibertyCity',
    department : 10,
    Nb_screen : 3,
    ip_tinc : '10.0.0.0',
    free_space : 78
  }

  ];
    return {movies, dcps, tdistributors, texhibitors};
  }
}

  /*
  genId(movies: Movie[]): number
  {
    movies.length > 0 ? Math.max(...movies.map(movie => movie.id)) + 1 : 11;
=======


  genId(movies: Movie[]):number{
    return movies.length > 0 ? Math.max
    (...movies.map(movie => movie.id)) + 1 : 11;
>>>>>>> UPDATE dcps,movies component, in-memory-data.service.ts
  }
   */





// A GARDER

/*{
        id: 13,
        name: 'Reservoirs Dogs',
        synopsis: 'Six truands désignés par des noms de code entreprennent de dévaliser une importante bijouterie sous la direction du tyrannique Joe Cabot et de son fils Nice Guy Eddie. L opération échoue malencontreusement et se solde par un bain de sang. Les malfrats se réfugient alors dans un entrepôt sordide. Mr Orange est grièvement blessé. Mr Pink, quant à lui, est d avis qu il existe un traître parmi eux. Mr Blonde, un psychopathe, entend bien faire parler le policier qu il a réussi à capturer.',
        distributorid : 1
      },

      {
        id: 14,
        name: 'STAR WARS EPISODE IV',
        synopsis: 'Il y a bien longtemps, dans une galaxie très lointaine. La guerre civile fait rage entre l Empire galactique et l Alliance rebelle. Capturée par les troupes de choc de l Empereur menées par le sombre et impitoyable Dark Vador, la princesse Leia Organa dissimule les plans de l Etoile Noire, une station spatiale invulnérable, à son droïde R2-D2 avec pour mission de les remettre au Jedi Obi-Wan Kenobi.',
        distributorid : 2
      },

     {
        id: 15,
        name: 'Fours Lions',
        synopsis: 'Animé par des envies de grandeur, Omar est déterminé à devenir un soldat du djihad en Angleterre. Avec ses amis, il décide de monter le coup décisif qui fera parler d eux et de leur cause. Problème : il leur manque le mode d emploi.',
        distributorid : 1
      },
      {
        id: 16,
        name: 'Gouffre au chimère',
        synopsis: 'Journaliste sans scrupules, Charles Tatum a connu quelques déboires à New York. Sans travail, alcoolique, il trouve enfin une occasion de renouer avec le métier. Il est embauché au Sun Bulletin d Albuquerque, au Nouveau-Mexique. Parti avec Herbie Cook, un jeune photographe, pour faire un reportage sur la chasse aux serpents à sonnette, il s arrête en cours de route à Escudero, où un homme vient d être enseveli par un éboulement dans une grotte.',
        distributorid : 2
      },
      {
        id: 17,
        name: 'Le quatuor à cornes',
        synopsis: 'Au cours d un périple fantaisiste jusqu au bord de la mer, quatre vaches vont découvrir la liberté dans le monde inconnu qui s étend au-delà de leur pré. Cette odyssée riche en aventure, en rencontres burlesques, disputes, réconciliations et découvertes, confronte chacune à ce qui pouvait lui arriver de pire, et transforme le troupeau initial, trop lié par l habitude, en une irréductible bande d amies, solidaires et affranchies.',
        distributorid : 3
      },
      {
        id: 18,
        name: 'Sauvage',
        synopsis: 'Léo, 22 ans, se vend dans la rue pour un peu d’argent. Les hommes défilent. Lui reste là, en quête d’amour. Il ignore de quoi demain sera fait. Il s’élance dans les rues. Son cœur bat fort. ',
        distributorid : 2
      },
      {
        id: 19,
        name: 'Rafiki',
        synopsis: 'À Nairobi, Kena et Ziki mènent deux vies de jeunes lycéennes bien différentes, mais cherchent chacune à leur façon à poursuivre leurs rêves. Leurs chemins se croisent en pleine campagne électorale au cours de laquelle s’affrontent leurs pères respectifs. Attirées l’une vers l’autre dans une société kenyane conservatrice, les deux jeunes femmes vont être contraintes de choisir entre amour et sécurité...',
        distributorid : 3
      }

      */
