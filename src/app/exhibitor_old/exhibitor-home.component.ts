/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { MatCardModule } from '@angular/material';

import { Exhibitor }  from '../_models';
import { User } from 'app/_models';
import { ExhibitorsService } from '../_services';

@Component({
  selector: 'app-exhibitor-home',
  templateUrl: './exhibitor-home.component.html',
  styleUrls: ['./exhibitor-home.component.css']
})

export class ExhibitorHomeComponent implements OnInit {

  @Input() exhibitorid: number;
  @Input() currentUser: User;
  exhibitor: Exhibitor;

constructor(
            private exhibitorsService: ExhibitorsService,
            private location: Location
           ) {}

  ngOnInit(): void {
    this.getExhibitorsContent();

  }// Fin ngOnInit

  getExhibitorsContent(): void {
  this.exhibitorsService.getExhibitorsContent(this.exhibitorid)
    .subscribe(exhibitor => this.exhibitor = exhibitor);
  }// Fin getDistributorsContent

  goBack(): void {
    this.location.back();
  }// Fin goBack

  save(): void {
    this.exhibitorsService.updateExhibitors(this.exhibitor)
      .subscribe(() => this.goBack());
  }// Fin save


}// Fin
