/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';

import {Movie} from './movies';
import {MoviesService} from './movies.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {catchError, finalize} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';



export class MoviesDataSource implements DataSource<Movie> {

  private moviesSubject = new BehaviorSubject<Movie[]>([]);
  private moviesTotalSubject = new BehaviorSubject<Number>(0);
  moviesTotal = this.moviesTotalSubject.asObservable();

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

 constructor(private moviesService: MoviesService) { }

  loadMovies(filter: string,
    sortDirection: string,
    pageIndex: number,
    pageSize: number) {

    this.loadingSubject.next(true);

    this.moviesService.findMovies(filter, sortDirection,
      pageIndex, pageSize).pipe(
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe(movies => {
        this.moviesSubject.next(movies['payload']);
        this.moviesTotalSubject.next(movies['total']);
      });

  }

  connect(collectionViewer: CollectionViewer): Observable<Movie[]> {
    console.log('Connecting data source movie');
    return this.moviesSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.moviesSubject.complete();
    this.moviesTotalSubject.complete();
    this.loadingSubject.complete();
  }

}

