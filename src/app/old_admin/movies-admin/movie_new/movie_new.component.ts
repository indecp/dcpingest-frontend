/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input , ElementRef} from '@angular/core';
import { FormControl, FormGroup, FormBuilder, FormArray, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { Movie } from '../movies';
import { MoviesService } from '../movies.service';

import { DISTRIBUTORS } from '../../distributors-admin/distributors';
import { DistributorsService } from '../../distributors-admin/distributors.service';

import { User, ROLE } from 'app/_models';
import { UserService } from 'app/_services';

import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'app-form-movie',
  templateUrl: './movie_new.component.html',
  styleUrls: ['./movie_new.component.css']
})

export class FormMovieComponent implements OnInit {

  signMovie: FormGroup;
  dataSource = new FormData();
  dataSourceDistributors: DISTRIBUTORS[];

  userSubscription: any;
  isLogged = false;
  user: User;

constructor(private formBuilder: FormBuilder,
            private movieService: MoviesService,
            private distributorsService: DistributorsService,
            private http: HttpClient,
            private userService: UserService,
            private route: ActivatedRoute,
            private router: Router) {
    this.signMovie = this.createFromGroup();
    this.userSubscription = this.userService.user$.subscribe(user => {
    this.isLogged = user ? true : false;
    this.user = user;
  });
  }

  createFromGroup() {
    return new FormGroup({
      title: new FormControl(),
      synopsis: new FormControl(),
      director: new FormControl(),
      scenario: new FormControl(),
      actor: new FormControl(),
      country: new FormControl(),
      distributorsList : new FormControl()
    });
  }// Fin createFromGroup

  createFormGroupWithBuilder(formBuilder: FormBuilder) {
    return formBuilder.group({
      title: '',
      synopsis: '',
      director: '',
      scenario: '',
      actor: '',
      country: '',
      distributorsList : ''
    });
  }// FIN  createFormGroupWithBuilder

  createFormGroupWithBuilderAndModel( formBuilder: FormBuilder) {
    return formBuilder.group({
      DataMovie: formBuilder.group(new Movie()),
    });
  }// FIN  createFormGroupWithBuilderAndMode

  onSubmit() {
    this.movieService.addMovie(this.signMovie.value)
      .subscribe(  { complete: () => this.dataSource });
  }// FIN onSubmit


  ngOnInit() {
    this.distributorsService
        .getDistributors()
        .subscribe(
          distrib => (this.dataSourceDistributors = distrib)
        );
  }// Fin ngOnInit

  getRole(user) {
   if (!user || !user.ID) {
      console.debug('No user');
      return;
    }
     return ROLE[user.role];

  }

}// Fin
