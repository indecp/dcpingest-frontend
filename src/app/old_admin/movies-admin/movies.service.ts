/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Subject } from 'rxjs/Subject';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Movie } from './movies';
import { MessageService } from '../../message.service';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type' : 'application/json'})
};

@Injectable({ providedIn: 'root'})
export class MoviesService {
  private MoviesUrl = 'http://localhost:5000/ingestapi/movies';
  constructor(private http: HttpClient,
              private message_Service: MessageService) { }


  findMovies(
    filter = '', sortOrder = 'asc',
    pageNumber = 1, pageSize = 50):  Observable<Movie> {
      return this.http.get(this.MoviesUrl, {
        params: new HttpParams()
        .set('filter', filter)
        .set('sortOrder', sortOrder)
        .set('pageNumber', pageNumber.toString())
        .set('pageSize', pageSize.toString())
      }).pipe(
        map( (res: Movie) => res)
      );
    }// Fin findMovies

// A GARDER
  getMovies(): Observable<Movie[]> {
    return this.http.
    get<Movie[]>(this.MoviesUrl)
     .pipe(tap(movies => this.log('fetched movies')),
       catchError(this.handleError('getMovies', [])));

  }// Fin GetMovies


  getMovie(id: number): Observable<Movie> {
    const url = `${this.MoviesUrl}/${id}`;
    return this.http.get<Movie>(url)
    .pipe(tap(_ => this.log(`fetched movie_s id=${id}`)),
      catchError(this.handleError<Movie>(`getMovies id=${id}`)));
  }

  getFTR(): Observable<Movie[]> {
    return this.http.
    get<Movie[]>(this.MoviesUrl)
     .pipe(tap(movies => this.log('fetched movies')),
       catchError(this.handleError('getFTR', [])));
  }

  getFTRValid(id: number): Observable<Movie> {
    const url = `${this.MoviesUrl}/${id}/nb_valid_ftr`;
    return this.http.get<Movie>(url)
    .pipe(tap(_ => this.log(`fetched movie_s id=${id}`)),
      catchError(this.handleError<Movie>(`getFTR id=${id}`)));
  }

  getTLR(): Observable<Movie[]> {
    return this.http.
    get<Movie[]>(this.MoviesUrl)
     .pipe(tap(movies => this.log('fetched movies')),
       catchError(this.handleError('getTLR', [])));
  }

  getTLRValid(id: number): Observable<Movie> {
    const url = `${this.MoviesUrl}/${id}/nb_valid_tlr`;
    return this.http.get<Movie>(url)
    .pipe(tap(_ => this.log(`fetched movie_s id=${id}`)),
      catchError(this.handleError<Movie>(`getTLR id=${id}`)));
  }




    addMovie(movie_s: Movie): Observable<Movie> {
      return this.http.post<Movie>(this.MoviesUrl, movie_s, httpOptions)
      .pipe(tap((movie_s: Movie) => this.log(`added movie_s w/ id=${movie_s.id}`)),
      catchError(this.handleError<Movie>('addMovie')));
     }// Fin addMovie


    deleteMovie(movie_s: Movie | number): Observable<Movie> {
      const id = typeof movie_s === 'number' ? movie_s : movie_s.id;
      const url = `${this.MoviesUrl}/${id}`;
      return this.http.delete<Movie>(url, httpOptions)
      .pipe(tap(_ => this.log(`delete movie_s id=${id}`)),
      catchError(this.handleError<Movie>('deleteMovie')));
     }// Fin deleteMovie

    updateMovies(movie_s: Movie): Observable<any> {
      return this.http.put(this.MoviesUrl, movie_s, httpOptions)
      .pipe(tap(_ => this.log(`updated movie_s id=${movie_s.id}`)),
      catchError(this.handleError<any>('updateMovies')));
    }// Fin updateMovies

    private handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of (result as T);
      };
    }// fin handleError

    private log(message: string) {
      this.message_Service.add(`MoviesService: ${message}`);
    }// Fin log

}



