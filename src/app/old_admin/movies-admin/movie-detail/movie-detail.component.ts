/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input, ViewChild} from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { Location } from '@angular/common';
import { MatCardModule, MatButton} from '@angular/material';
import { Movie }  from '../movies';
import { MoviesService } from '../movies.service';
import { User, ROLE } from 'app/_models';
import { UserService } from 'app/_services';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})

export class MovieDetailComponent implements OnInit {

  nbMovies: Number = 80;

  @Input() movie: Movie;
  displayedColumns = ['dcpid' , 'dcpname', 'contentkind', 'size'];

  user: User;
  userSubscription: any;
  isLogged = false;

constructor(private route: ActivatedRoute,
            private movieService: MoviesService,
            private location: Location,
            private userService: UserService,
            private router: Router,
           ) {this.userSubscription = this.userService.user$.subscribe(user => {
           this.isLogged = user ? true : false;
           this.user = user;
          });
          }




  ngOnInit(): void {
    this.getFilm();
  }// Fin ngOnInit

  getFilm(): void {
    const id = +this.route.snapshot.paramMap.get('id');

    this.movieService.getMovie(id)
      .subscribe(movie => this.movie = movie);
  }// Fin getFilm


  goBack(): void {
    this.location.back();
  }// Fin goBack

getRole(user) {
   if (!user || !user.ID) {
      console.debug('No user');
      return;
    }
     return ROLE[user.role];

  }



}// Fin
