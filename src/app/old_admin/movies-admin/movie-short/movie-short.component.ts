/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input } from '@angular/core';
import { MoviesService } from '../movies.service';
import { Movie } from '../movies';

@Component({
  selector: 'app-movie-short',
  templateUrl: './movie-short.component.html',
  styleUrls: ['./movie-short.component.css']
})

export class MovieShortComponent implements OnInit {

  @Input() movieid: number;
  movie: Movie;

constructor(private movieService: MoviesService) { }

  ngOnInit() {
  	this.movieService.getMovie(this.movieid)
  		.subscribe(movie => this.movie = movie);
  }// Fin ngOnInit

}// Fin
