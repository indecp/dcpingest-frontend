/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit } from '@angular/core';

import { Observable, Subject } from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';

import { Movie } from '../movies';
import { MoviesService } from '../movies.service';


@Component({
  selector: 'app-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.css']
})

export class MovieSearchComponent implements OnInit {
	  movies$: Observable<Movie[]>;
	  private searchTerms = new Subject<string>();

constructor(private movies_Service: MoviesService) { }

  ngOnInit(): void {
   /* this.movies$ = this.searchTerms.pipe(
  	  debounceTime(300),
  	  distinctUntilChanged(),
  	  switchMap((term: string) => this.movies_Service.searchMovies(term)));
  */}// Fin ngOnInit

 /* search(term : string): void{
    this.searchTerms.next(term);
  }//Fin search
*/
}// Fin
