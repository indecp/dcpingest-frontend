/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Subject } from 'rxjs/Subject';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { DISTRIBUTORS } from './distributors';

import { MessageService } from '../../message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type' : 'application/json'})
};

@Injectable({providedIn: 'root'})
export class DistributorsService {
 private DistributorsUrl = 'http://localhost:5000/ingestapi/distributors';

 constructor(private http: HttpClient,
             private message_Service: MessageService) { }

   getDistributors(): Observable<DISTRIBUTORS[]> {

   return this.http
   .get<DISTRIBUTORS[]>(this.DistributorsUrl)
    .pipe(tap(distributors => this.log('fetched distributors')),
      catchError(this.handleError('getDistributors', [])));
  }// Fin getDistributors

  getDistributorsContent(id: number): Observable<DISTRIBUTORS> {
   const url = `${this.DistributorsUrl}/${id}`;
    return this.http.get<DISTRIBUTORS>(url)
     .pipe(tap(_ => this.log(`fetched distrib_s id=${id}`)),
       catchError(this.handleError<DISTRIBUTORS>(`getDistributorsContent id=${id}`)));
  }// Fin getDistributorsContent

  searchDistributors(term: string): Observable<DISTRIBUTORS[]> {
    if (!term.trim()) {
      return of([]);
    }
      return this.http.get<DISTRIBUTORS[]>(`${this.DistributorsUrl}/?name=${term}`)
       .pipe(tap(_ => this.log(`found distributors matching "$(term)"`)),
         catchError(this.handleError<DISTRIBUTORS[]>('searchDistributors', [])));
  }// Fin searchDistributors

 addDistributors(distrib_s: DISTRIBUTORS): Observable<DISTRIBUTORS> {
    return this.http.post<DISTRIBUTORS>(this.DistributorsUrl, distrib_s, httpOptions)
    .pipe(tap((distrib_s: DISTRIBUTORS) => this.log(`added distrib_s w/ id=${distrib_s.name}`)),
      catchError(this.handleError<DISTRIBUTORS>('addDistributors')));
  }// Fin addDistributors

  deleteDistributors(distrib_s: DISTRIBUTORS | number): Observable<DISTRIBUTORS> {
   const id = typeof distrib_s === 'number' ? distrib_s : distrib_s.name;
   const url = `${this.DistributorsUrl}/${id}`;
    return this.http.delete<DISTRIBUTORS>(url, httpOptions)
    .pipe(tap(_ => this.log(`delete distrib_s id=${id}`)),
      catchError(this.handleError<DISTRIBUTORS>('deleteDistributors')));
  }// Fin deleteDistributors

  updateDistributors(distrib_s: DISTRIBUTORS): Observable<any> {
    return this.http.put(this.DistributorsUrl, distrib_s, httpOptions)
     .pipe(tap(_ => this.log(`updated distrib_s: id=${distrib_s.name}`)),
       catchError(this.handleError<any>('updateDistributors')));
  }// Fin updateDistributors

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
    //  console.error(error);
       this.log(`${operation} failed: ${error.message}`);
        return of (result as T);
    }; }// fin handleError

  private log(message: string) {
    this.message_Service.add('DistributorsService: ${message}');
  }// Fin log

}// Fin
