/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input, ViewChild} from '@angular/core';
import { Location } from '@angular/common';
import { MatCardModule, MatTableDataSource, MatPaginator, MatSort} from '@angular/material';

import { User, ROLE } from 'app/_models';
import { UserService } from 'app/_services';

import { DISTRIBUTORS }  from '../distributors';
import { DistributorsService } from '../distributors.service';

import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-distributors-detail',
  templateUrl: './distributors-detail.component.html',
  styleUrls: ['./distributors-detail.component.css']
})

export class DistributorsDetailComponent implements OnInit {

  distributors: DISTRIBUTORS[];
  @Input() distributor: DISTRIBUTORS;
  displayedColumns: string[] = ['distribid', 'name', 'partners', 'mail', 'user_contact', 'number_contact', 'CNC_Code', 'user_Id', 'mail_as_user_id'];
  dataSource = new MatTableDataSource<DISTRIBUTORS>(this.distributors);
  @ViewChild(MatPaginator) paginator: MatPaginator;

  user: User;
  userSubscription: any;
  isLogged = false;

constructor(private route: ActivatedRoute,
            private Distrib_Service: DistributorsService,
            private location: Location,
            private userService: UserService,
            private router: Router,
           ) {this.userSubscription = this.userService.user$.subscribe(user => {
           this.isLogged = user ? true : false;
           this.user = user;
          });
          }



  ngOnInit(): void {
    this.getDistributorsContent();
    this.dataSource.paginator = this.paginator;
  }// Fin ngOnInit

  getDistributorsContent(): void {

  const id = +this.route.snapshot.paramMap.get('id');

  this.Distrib_Service.getDistributorsContent(id)
    .subscribe(distributor => this.distributor = distributor);
  }// Fin getDistributorsContent

  goBack(): void {
    this.location.back();
  }// Fin goBack

  save(): void {
    this.Distrib_Service.updateDistributors(this.distributor)
      .subscribe(() => this.goBack());
  }// Fin save

getRole(user) {
   if (!user || !user.ID) {
      console.debug('No user');
      return;
    }
     return ROLE[user.role];

  }

}// Fin
