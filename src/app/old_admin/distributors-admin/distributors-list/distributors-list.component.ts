/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatButton} from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';

import { User, ROLE } from 'app/_models';
import { UserService } from 'app/_services';

import { DistributorsService } from '../distributors.service';
import { DISTRIBUTORS } from '../distributors';

@Component({
  selector: 'app-distributors-list',
  templateUrl: './distributors-list.component.html',
  styleUrls: ['./distributors-list.component.css']
})

export class DistributorsListComponent implements OnInit {

  user: User;
  userSubscription: any;
  isLogged = false;

  displayedColumns: string[] = ['distributorname', 'cnc_code', 'contact', 'dcpbay', 'delete'];
  dataSource = new MatTableDataSource<DISTRIBUTORS>();
  distributors: DISTRIBUTORS[];
  error: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

constructor(private distrib_services: DistributorsService,
            private userService: UserService,
            private router: Router,
            private route: ActivatedRoute) {this.userSubscription = this.userService.user$.subscribe(user => {
           this.isLogged = user ? true : false;
           this.user = user;
          });
          }

  ngOnInit() {
    this.distrib_services
      .getDistributors()
      .subscribe(
        distributors => (this.dataSource.data = distributors),
        error => (this.error = error)
      );
  this.dataSource.paginator = this.paginator;

  }// Fin ngOnInit

  delete(distrib: DISTRIBUTORS): void {
   // this.displayedColumns.filter(m => m !== movie);
   this.distrib_services.deleteDistributors(distrib).subscribe();
  }// Fin delete

  getRole(user) {
     if (!user || !user.ID) {
        console.debug('No user');
        return;
      }
       return ROLE[user.role];

    }
}// Fin
