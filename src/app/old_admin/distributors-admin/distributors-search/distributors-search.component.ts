/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { DistributorsService } from '../distributors.service';
import { DISTRIBUTORS } from '../distributors';

@Component({
  selector: 'app-distributors-search',
  templateUrl: './distributors-search.component.html',
  styleUrls: ['./distributors-search.component.css']
})

export class DistributorsSearchComponent implements OnInit {
	distrib$: Observable<DISTRIBUTORS[]>;
	private searchTerms = new Subject<string>();

  constructor(private distrib_Service: DistributorsService) { }

  ngOnInit(): void {
  	  /* this.distrib$ = this.searchTerms.pipe(
  		debounceTime(300),
  		distinctUntilChanged(),
  		switchMap((term: string) =>this.distrib_Service.searchDistributors(term)));
  }//fin ngOnInit

 search(term : string): void{
    this.searchTerms.next(term);
  }//Fin search

*/}
}
