/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, FormArray, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { DISTRIBUTORS } from '../distributors';
import { DistributorsService } from '../distributors.service';
import { Location } from '@angular/common';

import { User, ROLE } from 'app/_models';
import { UserService } from 'app/_services';

import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-edit-distributors',
  templateUrl: './edit-distributors.component.html',
  styleUrls: ['./edit-distributors.component.css']
})
export class EditDistributorsComponent implements OnInit {

  distributorsForm: FormGroup;
  Partenaire = ['OUI', 'NON'];

  userSubscription: any;
  isLogged = false;
  user: User;

  constructor(private formBuilder: FormBuilder,
    private distributorsService: DistributorsService,
    private http: HttpClient,
    private location: Location,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router) {
    this.distributorsForm = this.createFromGroup();
    this.userSubscription = this.userService.user$.subscribe(user => {
    this.isLogged = user ? true : false;
    this.user = user;
  });
  }

  createFromGroup() {
    return new FormGroup({
      id: new FormControl(),
	  distributorname: new FormControl(),
	  partners : new FormControl(),
	  mail: new FormControl(),
	  user_contact: new FormControl(),
	  number_contact: new FormControl(),
	  CNC_Code : new FormControl(),
	  user_Id : new FormControl(),
	  mail_as_user_id : new FormControl()
    });
  }// Fin createFromGroup

  goBack(): void {

    this.location.back();
  }

  onSubmit(): void {
    this.distributorsService
    .updateDistributors(this.distributorsForm.value as DISTRIBUTORS)
    .subscribe(() => this.goBack());
  }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.distributorsService.getDistributorsContent(id)
      .subscribe( distrib => {this.distributorsForm.patchValue(distrib);

  	});
  }

   getRole(user) {
   if (!user || !user.ID) {
      console.debug('No user');
      return;
    }
     return ROLE[user.role];

  }
}
