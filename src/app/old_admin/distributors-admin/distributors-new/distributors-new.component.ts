/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, FormArray, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { DistributorsService } from '../distributors.service';
import { DISTRIBUTORS } from '../distributors';

import { User, ROLE } from 'app/_models';
import { UserService } from 'app/_services';

import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-distributors-new',
  templateUrl: './distributors-new.component.html',
  styleUrls: ['./distributors-new.component.css']
})


export class DistributorsNewComponent implements OnInit {
   signDistrib: FormGroup;
   Partenaire = ['OUI', 'NON'];
   dataSource = new FormData();

   userSubscription: any;
   isLogged = false;
   user: User;

constructor(private formBuilder: FormBuilder,
            private distrib_Service: DistributorsService,
            private http: HttpClient,
            private userService: UserService,
            private route: ActivatedRoute,
            private router: Router) {
    this.signDistrib = this.createFromGroup();
    this.userSubscription = this.userService.user$.subscribe(user => {
    this.isLogged = user ? true : false;
    this.user = user;
  });
  }

  createFromGroup() {
    return new FormGroup({
      name: new FormControl(),
      user_contact: new FormControl(),
      partners: new FormControl(),
      number_contact: new FormControl(),
      mail: new FormControl(),
      CNC_Code: new FormControl()
    });
   }// Fin createFromGroup

  createFormGroupWithBuilderAndModel( formBuilder: FormBuilder) {
    return formBuilder.group({
      DataDistrib: formBuilder.group(new DISTRIBUTORS()),
    });
  }// FIN   createFormGroupWithBuilderAndModel

  ngOnInit() {}

  onSubmit() {
    this.distrib_Service.addDistributors(this.signDistrib.value)
      .subscribe( { complete: () => this.dataSource });
  }

  getRole(user) {
   if (!user || !user.ID) {
      console.debug('No user');
      return;
    }
     return ROLE[user.role];

  }

}// Fin

