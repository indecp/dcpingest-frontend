/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit } from '@angular/core';

import { Observable, Subject } from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';

import { Exhibitors } from '../exhibitors';
import { ExhibitorsService} from '../exhibitors.service';

@Component({
  selector: 'app-exhibitors-search',
  templateUrl: './exhibitors-search.component.html',
  styleUrls: ['./exhibitors-search.component.css']
})
export class ExhibitorsSearchComponent implements OnInit {

	exhib$: Observable<Exhibitors[]>;
	private searchTerms = new Subject<string>();

 constructor(private exhibitors_Service: ExhibitorsService) { }

  ngOnInit(): void {
   this.exhib$ = this.searchTerms.pipe(
  	debounceTime(300),
  	distinctUntilChanged(),
  	switchMap((term: string) => this.exhibitors_Service.searchExhibitors(term)));
  }// Fin ngOnInit

  search(term: string): void {
   this.searchTerms.next(term);
  }// Fin search

}// Fin
