/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';

import { User, ROLE } from 'app/_models';
import { UserService } from 'app/_services';

import { ExhibitorsService } from '../exhibitors.service';
import { Exhibitors } from '../exhibitors';

@Component({
  selector: 'app-exhibitors-list',
  templateUrl: './exhibitors-list.component.html',
  styleUrls: ['./exhibitors-list.component.css']
})

export class ExhibitorsListComponent implements OnInit {

   user: User;
   userSubscription: any;
   isLogged = false;

   displayedColumns: string[] = ['id', 'name', 'department', 'CNC_Code_autorised', 'ip_tinc', 'Nb_screen', 'free_space', 'delete'];
   dataSource = new MatTableDataSource<Exhibitors>();
   exhib: Exhibitors[];
   error: any;

   @ViewChild(MatPaginator) paginator: MatPaginator;
   @ViewChild(MatSort) sort: MatSort;

constructor(private exhibitors_service: ExhibitorsService,
            private userService: UserService,
            private router: Router,
            private route: ActivatedRoute) {this.userSubscription = this.userService.user$.subscribe(user => {
           this.isLogged = user ? true : false;
           this.user = user;
          });
          }

  ngOnInit() {
  this.exhibitors_service
    .getExhibitors()
    .subscribe(
      movies => (this.dataSource.data = movies),
      error => (this.error = error)
    );
    this.dataSource.paginator = this.paginator;
  }// Fin ngOnInit

  getRole(user) {
     if (!user || !user.ID) {
        console.debug('No user');
        return;
      }
       return ROLE[user.role];

    }


 }// Fin
