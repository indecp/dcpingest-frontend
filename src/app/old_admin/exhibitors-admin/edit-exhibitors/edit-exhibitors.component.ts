/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, FormArray, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

import { Exhibitors } from '../exhibitors';
import { ExhibitorsService } from '../exhibitors.service';
import { Location } from '@angular/common';

import { User, ROLE } from 'app/_models';
import { UserService } from 'app/_services';



@Component({
  selector: 'app-edit-exhibitors',
  templateUrl: './edit-exhibitors.component.html',
  styleUrls: ['./edit-exhibitors.component.css']
})
export class EditExhibitorsComponent implements OnInit {

  exhibitorsForm: FormGroup;
  userSubscription: any;
  isLogged = false;
  user: User;

  constructor(private formBuilder: FormBuilder,
              private exhibitorsService: ExhibitorsService,
              private userService: UserService,
              private http: HttpClient,
              private router: Router,
              private location: Location,
              private route: ActivatedRoute ) {
    this.exhibitorsForm = this.createFromGroup();
    this.userSubscription = this.userService.user$.subscribe(user => {
    this.isLogged = user ? true : false;
    this.user = user;
  });
  }

  createFromGroup() {
    return new FormGroup({
      id: new FormControl(),
      name : new FormControl(),
	  CNC_Code_autorised : new FormControl(),
	  log : new FormControl(),
	  pass : new FormControl(),
	  mail: new FormControl(),
	  name_contact: new FormControl(),
	  number_contact: new FormControl(),
	  address_contact: new FormControl(),
	  ZIP_Code : new FormControl(),
	  town : new FormControl(),
	  department : new FormControl(),
	  Nb_screen : new FormControl(),
	  ip_tinc : new FormControl(),
	  free_space : new FormControl()
    });
  }// Fin createFromGroup


  goBack(): void {

    this.location.back();
  }

  onSubmit(): void {
    this.exhibitorsService
    .updateExhibitors(this.exhibitorsForm.value as Exhibitors )
    .subscribe(() => this.goBack());
  }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.exhibitorsService.getExhibitorsContent(id)
      .subscribe( exhib => {this.exhibitorsForm.patchValue( exhib );

  	});
  }

  getRole(user) {
   if (!user || !user.ID) {
      console.debug('No user');
      return;
    }
     return ROLE[user.role];

  }
}
