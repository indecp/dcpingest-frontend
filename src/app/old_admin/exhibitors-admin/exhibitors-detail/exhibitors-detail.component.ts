/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input, ViewChild} from '@angular/core';
import { Location } from '@angular/common';
import { MatCardModule,
         MatTableDataSource,
         MatPaginator, MatSort} from '@angular/material';

import { Exhibitors }  from '../exhibitors';
import { ExhibitorsService } from '../exhibitors.service';

import { ActivatedRoute , Router} from '@angular/router';

import { User, ROLE } from 'app/_models';
import { UserService } from 'app/_services';

@Component({
  selector: 'app-exhibitors-detail',
  templateUrl: './exhibitors-detail.component.html',
  styleUrls: ['./exhibitors-detail.component.css']
})
export class ExhibitorsDetailComponent implements OnInit {

  @Input() exhibitors: Exhibitors;
  displayedColumns: string[] = ['name', 'CNC_Code_autorised',
  								              'log', 'mail', 'number_contact',
  								              'name_contact', 'address_contact',
  					                     'ZIP_Code', 'town', 'department',
  					                      'Nb_screen', 'ip_tinc', 'free_space'];
  dataSource = new MatTableDataSource<Exhibitors>();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  user: User;
  userSubscription: any;
  isLogged = false;

  constructor(private route: ActivatedRoute,
  	          private Exhibitors_Service: ExhibitorsService,
  	          private location: Location,
              private userService: UserService,
              private router: Router,
           ) {this.userSubscription = this.userService.user$.subscribe(user => {
           this.isLogged = user ? true : false;
           this.user = user;
          });
          }

  ngOnInit(): void {
  	this.getExhibitorsContent();
    // this.dataSource.paginator = this.paginator;
  }// Fin ngOnInit

  getExhibitorsContent(): void {
  const id = +this.route.snapshot.paramMap.get('id');
  	this.Exhibitors_Service.getExhibitorsContent(id)
  	 .subscribe(exhib => this.exhibitors = exhib);
  }// Fin getFilm

  goBack(): void {
  	this.location.back();
  }// Fin goBack

  getRole(user) {
     if (!user || !user.ID) {
        console.debug('No user');
        return;
      }
       return ROLE[user.role];

    }

}// Fin
