/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Subject } from 'rxjs/Subject';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Exhibitors } from './exhibitors';
import { MessageService } from '../../message.service';


const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type' : 'application/json'})
 };


@Injectable({ providedIn: 'root'})
export class ExhibitorsService {
	private ExhibitorsUrl = 'http://localhost:5000/ingestapi/exhibitors';


  constructor(private http: HttpClient,
  	          private message_Service: MessageService) { }

  getExhibitors(): Observable<Exhibitors[]> {
  	return this.http.get<Exhibitors[]>(this.ExhibitorsUrl)
  	 .pipe(tap(Exhibitors => this.log('fetched Exhibitors')),
  		 catchError(this.handleError('getExhibitors', [])));
  }// Fin GetExhibitors

  getExhibitorsContent(id: number): Observable<Exhibitors> {
   const url = `${this.ExhibitorsUrl}/${id}`;
  	return this.http.get<Exhibitors>(url)
     .pipe(tap(_ => this.log(`fetched Exhibitors_s id=${id}`)),
  		 catchError(this.handleError<Exhibitors>(`getExhibitors id=${id}`)));
  }// Fin getFilm

  searchExhibitors(term: string): Observable<Exhibitors[]> {
  	if (!term.trim()) {
  	 return of([]);
    }
  	  return this.http.get<Exhibitors[]>(`${this.ExhibitorsUrl}/?name=${term}`)
  	   .pipe(tap(_ => this.log(`found Exhibitors matching "$(term)"`)),
  		  catchError(this.handleError<Exhibitors[]>('searchExhibitors', [])));
  }// Fin searchExhibitorss

   addExhibitors(Exhibitors_s: Exhibitors): Observable<Exhibitors> {
  	 return this.http.post<Exhibitors>(this.ExhibitorsUrl, Exhibitors_s, httpOptions)
  	  .pipe(tap((Exhibitors_s: Exhibitors) => this.log(`added Exhibitors_s w/ id=${Exhibitors_s.id}`)),
  		  catchError(this.handleError<Exhibitors>('addExhibitors')));
  }// Fin addExhibitors

  deleteExhibitors(Exhibitors_s: Exhibitors | number): Observable<Exhibitors> {
	 const id = typeof Exhibitors_s === 'number' ? Exhibitors_s : Exhibitors_s.id;
	 const url = `${this.ExhibitorsUrl}/${id}`;
    return this.http.delete<Exhibitors>(url, httpOptions)
	   .pipe(tap(_ => this.log(`delete Exhibitors_s id=${id}`)),
	     catchError(this.handleError<Exhibitors>('deleteExhibitors')));
  }// Fin deleteExhibitors

  updateExhibitors(Exhibitors_s: Exhibitors): Observable<any> {
   return this.http.put(this.ExhibitorsUrl, Exhibitors_s, httpOptions)
  	.pipe(tap(_ => this.log(`updated Exhibitors_s id=${Exhibitors_s.id}`)),
  		catchError(this.handleError<any>('updateExhibitors')));
  }// Fin updateExhibitorss

  private handleError<T> (operation = 'operation', result?: T) {
  	return (error: any): Observable<T> => {
  		console.error(error);
  		 this.log(`${operation} failed: ${error.message}`);
  		  return of (result as T);
   }; }// fin handleError

  private log(message: string) {
   this.message_Service.add('ExhibitorsService: ${message}');
  }// Fin log


}// Fin
