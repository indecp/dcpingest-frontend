/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { Exhibitors } from '../exhibitors';
import { ExhibitorsService } from '../exhibitors.service';

import { User, ROLE } from 'app/_models';
import { UserService } from 'app/_services';

import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-exhibitors-new',
  templateUrl: './exhibitors-new.component.html',
  styleUrls: ['./exhibitors-new.component.css']
})
export class ExhibitorsNewComponent implements OnInit {

  signExhibitors: FormGroup;
  dataSource = new FormData();
  dataSourceExhibitors: Exhibitors[];

  userSubscription: any;
  isLogged = false;
  user: User;

constructor(private formBuilder: FormBuilder,
            private exhibitors_Service: ExhibitorsService,
            private http: HttpClient,
            private userService: UserService,
            private route: ActivatedRoute,
            private router: Router) {
	this.signExhibitors = this.createFromGroup();
 	this.userSubscription = this.userService.user$.subscribe(user => {
    this.isLogged = user ? true : false;
    this.user = user;
  });
  }


 createFromGroup() {
	 return new FormGroup({
	  name: new FormControl(),
	  CNC_Code_autorised: new FormControl(),
	  log: new FormControl(),
	  pass: new FormControl(),
      mail: new FormControl(),
      name_contact: new FormControl(),
	  number_contact: new FormControl(),
	  address_contact: new FormControl(),
	  ZIP_Code: new FormControl(),
	  town: new FormControl(),
	  department: new FormControl(),
	  Nb_screen: new FormControl(),
	  ip_tinc: new FormControl(),
	  free_space: new FormControl()
	 });
    }// Fin createFromGroup

createFormGroupWithBuilder(formBuilder: FormBuilder) {
   return formBuilder.group({
	  name: '',
	  CNC_Code_autorised: '',
	  log: '',
	  pass: '',
      mail: '',
      name_contact: '',
	  number_contact: '',
	  address_contact: '',
	  ZIP_Code: '',
	  town: '',
	  department: '',
	  Nb_screen: '',
	  ip_tinc: '',
	  free_space: ''
    });
  }// FIN  createFormGroupWithBuilder

 createFormGroupWithBuilderAndModel( formBuilder: FormBuilder) {
   return formBuilder.group({
      DataExhibitors : formBuilder.group(new Exhibitors()),
    });
  }// FIN  createFormGroupWithBuilderAndModel

 onSubmit() {
	 this.exhibitors_Service.addExhibitors(this.signExhibitors.value)
	.subscribe({ complete: () => this.dataSource });
	}

  ngOnInit() {
    this.exhibitors_Service
        .getExhibitors()
        .subscribe(
          exhib => (this.dataSourceExhibitors = exhib)
        );
  }//


   getRole(user) {
   if (!user || !user.ID) {
      console.debug('No user');
      return;
    }
     return ROLE[user.role];

  }

}
