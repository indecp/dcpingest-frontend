/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { Dcp } from '../dcps';
import { DcpService } from '../dcps.service';

@Component({
  selector: 'app-dcps-search',
  templateUrl: './dcps-search.component.html',
  styleUrls: ['./dcps-search.component.css']
})

export class DcpsSearchComponent implements OnInit {
	dcps$: Observable<Dcp[]>;
	private searchTerms = new Subject<string>();

constructor(private dcps_Service: DcpService) { }

  ngOnInit(): void {
  /*  this.dcps$ = this.searchTerms.pipe(
    debounceTime(300),
    distinctUntilChanged(),
    switchMap((term: string) =>this.dcps_Service.searchDcps(term)));
  }//fin ngOnInit

  search(term : string): void{
    this.searchTerms.next(term);
  }//Fin search
*/
  }// Fin
}
