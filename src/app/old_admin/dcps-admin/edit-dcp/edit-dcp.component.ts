/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, FormArray, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';

import { Dcp } from '../dcps';
import { DcpService } from '../dcps.service';

import { User, ROLE } from 'app/_models';
import { UserService } from 'app/_services';

import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'app-edit-dcp',
  templateUrl: './edit-dcp.component.html',
  styleUrls: ['./edit-dcp.component.css']
})

export class EditDcpComponent implements OnInit {

  dcpForm: FormGroup;
  TypeDCP = ['FTR', 'TLR'];

  userSubscription: any;
  isLogged = false;
  user: User;

constructor(private formBuilder: FormBuilder,
            private dcpService: DcpService,
            private http: HttpClient,
            private location: Location,
            private userService: UserService,
            private route: ActivatedRoute,
            private router: Router) {
    this.dcpForm = this.createFromGroup();
    this.userSubscription = this.userService.user$.subscribe(user => {
    this.isLogged = user ? true : false;
    this.user = user;
  });
  }

  createFromGroup() {
    return new FormGroup({
      id: new FormControl(),
      dcpname: new FormControl(),
      contentkind: new FormControl(),
      size: new FormControl(),
    });
  }// Fin createFromGroup

  goBack(): void {
    this.location.back();
  }// Fin goBack

  onSubmit(): void {
    this.dcpService
    .updateDcps(this.dcpForm.value as Dcp )
    .subscribe(() => this.goBack());
  }// Fin onSubmit

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');

    this.dcpService.getDcp(id)
      .subscribe( dcp => {this.dcpForm.patchValue(dcp);
    });
  }// Fin ngOnInit

getRole(user) {
   if (!user || !user.ID) {
      console.debug('No user');
      return;
    }
     return ROLE[user.role];

  }


}// Fin
