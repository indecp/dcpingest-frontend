/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input, ViewChild} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MatFormField, MatCardModule, MatTableDataSource, MatPaginator, MatSort} from '@angular/material';

import { Dcp }  from '../dcps';
import { DcpService } from '../dcps.service';

import { registerLocaleData } from '@angular/common';

import { User, ROLE } from 'app/_models';
import { UserService } from 'app/_services';

import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';

registerLocaleData(localeFr, 'fr-FR', localeFrExtra);

@Component({
  selector: 'app-dcps-detail',
  templateUrl: './dcps-detail.component.html',
  styleUrls: ['./dcps-detail.component.css']
})

export class DcpsDetailComponent implements OnInit {

  user: User;
  userSubscription: any;
  isLogged = false;
  @Input() dcp: Dcp;
  error: any;
  interval: any;
  displayedColumns: string[] = ['dcpid', 'dcpname', 'contentkind', 'size'];
  dataSource = new MatTableDataSource<Dcp>();
  @ViewChild(MatPaginator) paginator: MatPaginator;

constructor( private route: ActivatedRoute,
             private location: Location,
             private userService: UserService,
             private dcpService: DcpService) {this.userSubscription = this.userService.user$.subscribe(user => {
             this.isLogged = user ? true : false;
             this.user = user;
            });
            }


  ngOnInit()  {
    this.getDcpsContent();
    this.onCheckLong();

    // this.OnTorrentCreate();
    // this.OnDateCheck();

    this.interval = setInterval(() => {
              this.getDcpsContent();
          }, 5000);
   }// Fin ngOnInit


  getDcpsContent() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.dcpService.getDcp(id)
    .subscribe(
      dcp => (this.dcp = dcp),
			error => (this.error = error)
     );
  }// Fin getDcpsContent


  goBack(): void {
  	this.location.back();
  }// Fin goBack

  onCheckShort() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.dcpService.getShortCheckStatus(id)
      .subscribe(
        dcp => (this.dcp = dcp),
        error => (this.error = error)
     );
  }

  onCheckLong() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.dcpService.getLongCheckStatus(id)
      .subscribe(
        dcp => (this.dcp = dcp),
        error => (this.error = error)
     );
  }

  OnTorrentCreate() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.dcpService.getTheTorrent(id)
      .subscribe(
        dcp => (this.dcp = dcp),
        error => (this.error = error)
     );
  }

  OnDcpIngest() {
    // Not implemented
    }

getRole(user) {
   if (!user || !user.ID) {
      console.debug('No user');
      return;
    }
     return ROLE[user.role];

  }


}// Fin
