/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { debounceTime, distinctUntilChanged, startWith, tap, delay } from 'rxjs/operators';
import { merge } from 'rxjs/observable/merge';

import { User, ROLE } from 'app/_models';
import { UserService } from 'app/_services';

import { DcpService } from '../dcps.service';
import { Dcp } from '../dcps';
import { DcpsDataSource } from '../dcps.datasource';

@Component({
  selector: 'app-dcps-list',
  templateUrl: './dcps-list.component.html',
  styleUrls: ['./dcps-list.component.css']
})

export class DcpsListComponent implements OnInit {

  user: User;
  userSubscription: any;
  isLogged = false;

  dcp: Dcp;
	nbDcps: Number = 80;

  dataSource: DcpsDataSource;
	displayedColumns: string[] = ['id', 'name', 'delete'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;

	error: any;


constructor(private dcpService: DcpService,
            private userService: UserService) {this.userSubscription = this.userService.user$.subscribe(user => {
            this.isLogged = user ? true : false;
            this.user = user;
            });
            }

  ngOnInit() {
      this.dataSource = new DcpsDataSource(this.dcpService);
      this.dataSource.loadDcps('', 'asc', 1, 50);
      this.dataSource.dcpsTotal.subscribe
      ( total => { this.nbDcps = total; } );
  }




  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 1);

    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;

          this.loadDcpsPage();
        })
      )
      .subscribe();

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadDcpsPage())
      )
      .subscribe();
  }

  loadDcpsPage() {
    this.dataSource.loadDcps(
      this.input.nativeElement.value,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

  getRole(user) {
   if (!user || !user.ID) {
      console.debug('No user');
      return;
    }
     return ROLE[user.role];

  }

}


/*	delete(dcps : Dcp): void{
  	 	//this.displayedColumns.filter(m => m !== movie);
   		this.dcpService.deleteDcps(dcps).subscribe();
 	}//Fin delete

}//Fin
*/
