/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, FormArray, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatPaginator, MatSort, MatSelectModule , MatSelect, MatTableDataSource, MatAutocompleteModule} from '@angular/material';
import { Observable, of } from 'rxjs';
import { map, catchError, switchMap, debounceTime, distinctUntilChanged, startWith, tap, delay} from 'rxjs/operators';
import {fromEvent} from 'rxjs/observable/fromEvent';
import {finalize} from 'rxjs/operators';
import {merge} from 'rxjs/observable/merge';

import { Dcp } from '../dcps';
import { DcpService } from '../dcps.service';

import { Movie } from '../../movies-admin/movies';
import { MoviesService } from '../../movies-admin/movies.service';
import { MoviesDataSource }  from '../../movies-admin/movies.datasource';
import { Subject } from 'rxjs/Subject';

import { User, ROLE } from 'app/_models';
import { UserService } from 'app/_services';

import { ActivatedRoute, Router } from '@angular/router';

import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-dcps-new',
  templateUrl: './dcps-new.component.html',
  styleUrls: ['./dcps-new.component.css']
})


export class DcpsNewComponent implements OnInit {

  userSubscription: any;
  isLogged = false;
  user: User;

  CtrlTitle = new FormControl();

  nbMovies: Number = 80;

  TitleMov: Observable<Movie[]>;
  signDcps: FormGroup;
  TypeDCP = ['FTR', 'TLR'];
  formData = new FormData();
  dataSourceMovie: MoviesDataSource;

  @ViewChild('input') input: ElementRef;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  error: any;

constructor(private formBuilder: FormBuilder,
            private DcpService: DcpService,
            private http: HttpClient,
            private movieService: MoviesService,
            private userService: UserService,
            private route: ActivatedRoute,
            private router: Router) {
    this.signDcps = this.createFromGroup();
    this.userSubscription = this.userService.user$.subscribe(user => {
    this.isLogged = user ? true : false;
    this.user = user;
  });
  }


  createFromGroup() {
    return new FormGroup({
      name: new FormControl(),
      contentkind: new FormControl(),
      size: new FormControl(),
      moviesList: new FormControl(),
    });
  }// Fin createFromGroup

  createFormGroupWithBuilderAndModel( formBuilder: FormBuilder) {
    return formBuilder.group({
      DataDCP: formBuilder.group(new Dcp()),
    });
  }// FIN   createFormGroupWithBuilderAndModel

  onSubmit() {
    this.DcpService.addDcp(this.signDcps.value)
         .subscribe( {complete: () => this.formData});
  }// FIN onSubmit


  ngOnInit() {
      this.dataSourceMovie = new MoviesDataSource(this.movieService);
      this.dataSourceMovie.loadMovies('', 'asc', 1, 50);
      this.dataSourceMovie.moviesTotal.subscribe
    ( total => { this.nbMovies = total; } );

  }

  ngAfterViewInit() {

     this.signDcps
      .get('moviesList')
      .valueChanges
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.loadMoviesPage();
        })
      )
      .subscribe();

  }

  loadMoviesPage() {
    this.dataSourceMovie.loadMovies(
      this.signDcps.get('moviesList').value,
      'asc', 1, 50);
  }

}// Fin
