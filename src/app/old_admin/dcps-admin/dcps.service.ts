/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Subject } from 'rxjs/Subject';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Dcp } from './dcps';
import { MessageService } from '../../message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type' : 'application/json'})
};

@Injectable({providedIn: 'root'})

export class DcpService {
  private DcpsUrl = 'http://localhost:5000/ingestapi/dcps';
  constructor(private http: HttpClient,
              private message_Service: MessageService) { }

  findDcps(
    filter = '', sortOrder = 'asc',
    pageNumber = 1, pageSize = 50):  Observable<Dcp> {
      return this.http.get(this.DcpsUrl, {
        params: new HttpParams()
        .set('filter', filter)
        .set('sortOrder', sortOrder)
        .set('pageNumber', pageNumber.toString())
        .set('pageSize', pageSize.toString())
      }).pipe(
        map( (res: Dcp) => res)
      );
    }// Fin

   getDcps(): Observable<Dcp[]> {
    return this.http
    .get<Dcp[]>(this.DcpsUrl)
    .pipe(tap(dcps => this.log('fetched dcps')),
    catchError(this.handleError('getDcps', [])));
  }// Fin getDcps

   getDcp(id: number): Observable<Dcp> {
    const url = `${this.DcpsUrl}/${id}`;
    return this.http.get<Dcp>(url)
    .pipe(tap(_ => this.log(`fetched dcps id=${id}`)),
    catchError(this.handleError<Dcp>(`getDcps id=${id}`)));
  }// Fin getDcp

   getShortCheck(): Observable<Dcp[]> {
    return this.http
    .get<Dcp[]>(this.DcpsUrl)
    .pipe(tap(check => this.log('fetched check')),
    catchError(this.handleError('getShortCheckStatus', [])));
  }// Fin getShortCheck

    getShortCheckStatus(id: number): Observable<Dcp> {
      const url = `${this.DcpsUrl}/${id}/short_check_status`;
      return this.http.get<Dcp>(url)
      .pipe(tap(_ => this.log(`fetched check id=${id}`)),
      catchError(this.handleError<Dcp>(`getShortCheckStatus id=${id}`)));

  }// Fin getShortCheckStatus

    getLongCheck(): Observable<Dcp[]> {
      return this.http
      .get<Dcp[]>(this.DcpsUrl)
      .pipe(tap(check_long => this.log('fetched check_long')),
      catchError(this.handleError('getLongCheckStatus', [])));
     }// Fin getLongCheck

   /* getLongCheckStatus(id : number):Observable<Dcp>{
       const url = `${this.DcpsUrl}/${id}/hash_verification_state_name`;
       return this.http.get<Dcp>(url)
       .pipe(tap(_=>this.log(`fetched check_long id=${id}`)),
       catchError(this.handleError<Dcp>(`getLongCheckStatus id=${id}`)));
  }//Fin getLongCheckStatus
*/
    getDateCheck(): Observable<Dcp[]> {
      return this.http
      .get<Dcp[]>(this.DcpsUrl)
      .pipe(tap(check_date => this.log('fetched check_date')),
      catchError(this.handleError('getDateCheckStatus', [])));
     }// Fin getLongCheck

     getDateCheckStatus(id: number): Observable<Dcp> {
       const url = `${this.DcpsUrl}/${id}/hash_verification_date`;
       return this.http.get<Dcp>(url)
       .pipe(tap(_ => this.log(`fetched check_date id=${id}`)),
       catchError(this.handleError<Dcp>(`getDateCheckStatus id=${id}`)));
    }// Fin getDateCheckStatus




  getLongCheckStatus(id: number): Observable<Dcp> {
    const url = `${this.DcpsUrl}/${id}/do_check_long`;
    return this.http.post<Dcp>(url, '' , httpOptions)
    .pipe(tap(_ => this.log(`fetched check_long id=${id}`)),
      catchError(this.handleError<Dcp>(`getLongCheckStatus id=${id}`)));
  }


      getTorrent(): Observable<Dcp[]> {
      return this.http
      .get<Dcp[]>(this.DcpsUrl)
      .pipe(tap(torrent => this.log('fetched torrent')),
      catchError(this.handleError('getTheTorrent', [])));
     }// Fin getTorrent

     getTheTorrent(id: number): Observable<Dcp> {
       const url = `${this.DcpsUrl}/${id}/torrent_creation_state_name`;
       return this.http.get<Dcp>(url)
       .pipe(tap(_ => this.log(`fetched torrent id=${id}`)),
       catchError(this.handleError<Dcp>(`getTheTorrent id=${id}`)));
    }// Fin getTheTorrent



   addDcp(dcps_s: Dcp): Observable<Dcp> {
    return this.http.post<Dcp>(this.DcpsUrl, dcps_s, httpOptions)
    .pipe(tap((dcps_s: Dcp) => this.log(`added dcps_s w/ id=${dcps_s.id}`)),
    catchError(this.handleError<Dcp>('addDcp')));
  }// Fin addDcps

  deleteDcps(dcps_s: Dcp | number): Observable<Dcp> {
    const id = typeof dcps_s === 'number' ? dcps_s : dcps_s.id;

    const url = `${this.DcpsUrl}/${id}`;

    return this.http.delete<Dcp>(url, httpOptions)
    .pipe(tap(_ => this.log(`delete dcps_s id=${id}`)),
    catchError(this.handleError<Dcp>('deleteDcps')));
  }// Fin deleteDcps

  updateDcps(dcps_s: Dcp): Observable<any> {
    return this.http.put(this.DcpsUrl, dcps_s, httpOptions)
    .pipe(tap(_ => this.log(`updated dcps_s id=${dcps_s.id}`)),
    catchError(this.handleError<any>('updateDcps')));
  }// Fin updateDcps

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
    console.error(error);
    this.log(`${operation} failed: ${error.message}`);
    return of (result as T); };
  }// fin handleError

  private log(message: string) {
    this.message_Service.add(`DcpService: ${message}`);
  }// Fin log

 }// Fin
