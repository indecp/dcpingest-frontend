/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';

import {Dcp} from './dcps';
import {DcpService} from './dcps.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {catchError, finalize} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';



export class DcpsDataSource implements DataSource<Dcp> {

  private dcpsSubject = new BehaviorSubject <Dcp[]>([]);
  private dcpsTotalSubject = new BehaviorSubject<Number>(0);
  dcpsTotal = this.dcpsTotalSubject.asObservable();

  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private DcpService: DcpService) {

  }

  loadDcps(filter: string,
    sortDirection: string,
    pageIndex: number,
    pageSize: number) {

    this.loadingSubject.next(true);

    this.DcpService.findDcps(filter, sortDirection,
      pageIndex, pageSize).pipe(
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe(dcps => {
        this.dcpsSubject.next(dcps['payload']);
        this.dcpsTotalSubject.next(dcps['total']);
      });

  }

  connect(collectionViewer: CollectionViewer): Observable<Dcp[]> {
    console.log('Connecting data source dcps');
    return this.dcpsSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.dcpsSubject.complete();
    this.dcpsTotalSubject.complete();
    this.loadingSubject.complete();
  }

}

