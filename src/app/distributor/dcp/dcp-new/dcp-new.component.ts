/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  FormGroup, FormBuilder} from '@angular/forms';

import { Observable }           from 'rxjs';
import { map }                  from 'rxjs/operators';

import { DcpService, MovieService, ElectronService } from 'app/_services';
import { Dcp, Movie }  from 'app/_models';
import { MessageService }  from 'app/message.service';

@Component({
  selector: 'app-dcp-new',
  templateUrl: './dcp-new.component.html',
  styleUrls: ['./dcp-new.component.css']
})
export class DcpNewComponent implements OnInit {
  dcpForm: FormGroup;
  dcp: Dcp;
  movieId: number;
  movie: Movie;
  clickMessage: any;



  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private dcpService: DcpService,
    private movieService: MovieService,
    public dialogService: MessageService,
    private electronService: ElectronService
  ) {}

  ngOnInit() {
    this.movieId = +this.route.snapshot.queryParams['movieid'];
    this.movieService
      .getMovie(this.movieId)
      .subscribe(movie => this.movie = movie);

    this.createFromGroup();
  }

  createFromGroup() {
    this.dcpForm = this.formBuilder.group({
      // 'id': this.formBuilder.control(),
      'dcp_path': this.formBuilder.control('/home/nicolas/data/DCP_in/CaptainMarvel_TLR-B-2D_S_EN-fr_FR_51_2K_DI_20180923_DTB_IOP'),
      'contentkind': this.formBuilder.control('FTR'),
      'movieid': this.formBuilder.control(this.movieId),
      'ingest': this.formBuilder.control(true),

    });
  }

  handleSubmit() {
  console.log(this.dcpForm.value);
}

  cancel() {
    this.gotoDcps();
  }

  save() {
    const new_dcp: Dcp = this.dcpForm.value as Dcp;
    new_dcp.dcp_path =  this.clickMessage[0];
    console.log(this.dcpForm.value);
    console.log(new_dcp);



    this.dcpService
    .addDcp(new_dcp)
    .subscribe(
      dcp => {
        console.log(dcp);
        this.router.navigate(['../', dcp.id ],
          { relativeTo: this.route });
        });

}


  gotoDcps() {
    // Pass along the dcp id if available
    // so that the CrisisListComponent can select that dcp.
    // Add a totally useless `foo` parameter for kicks.
    // Relative navigation back to the  dcps
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  onClickMe() {
    this.getFiles().then((val) => this.clickMessage = val);
  }

  async getFiles() {
              return new Promise<string>((resolve, reject) => {
                this.electronService.ipcRenderer.once('selected-directory', (event, arg) => {
                  resolve(arg);
                });

                this.electronService.ipcRenderer.send('open-file-dialog');
              });



    }

}
