/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from 'app/auth/auth.guard';

import { DistributorHomeComponent} from './distributor-home/distributor-home.component';
import { DistributorComponent }     from './distributor/distributor.component';
import {
  MovieDetailComponent,
  MovieListComponent,
  MovieNewComponent
}  from './movie/';
import {
  DcpDetailComponent,
  DcpListComponent,
  DcpNewComponent
} from './dcp/';


import { MovieDetailResolverService }    from './movie-detail-resolver.service';
import { DcpDetailResolverService }    from './dcp/dcp-detail-resolver.service';

const distributorRoutes: Routes = [
  {
    path: '',
    component: DistributorComponent,
    children: [
      {
        path: 'dcp',
        component: DcpListComponent,
        data: { animation: 'movies' },
      },
      { path: 'dcp/new',
          component:  DcpNewComponent,
          data: { animation: 'movie' }
      },
      { path: 'dcp/:id',
          component:  DcpDetailComponent,
          resolve: {
            dcp: DcpDetailResolverService
          },
          data: { animation: 'movie' }
      },
      {
        path: 'film',
        component: MovieListComponent,
        data: { animation: 'movies' }
      },
      { path: 'film/new',
          component:  MovieNewComponent,
          data: { animation: 'movie' },


      },
      { path: 'film/:id',
          component:  MovieDetailComponent,
          resolve: {
            movie: MovieDetailResolverService
          },
          data: { animation: 'movie' }
      },
      {
        path: '',
        component: DistributorHomeComponent
      }
    ]
  }
];

/*
const distributorRoutes: Routes = [
  {
    path: '',
    component: DistributorComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        children: [
          { path: 'film',  component: DistributorMovieListComponent, data: { animation: 'movies' } },
          { path: 'film/:id',
            component:  MovieDetailComponent,
            resolve: {
              movie: MovieDetailResolverService
            },
            data: { animation: 'movie' }
          },
        ]
      }
    ]
  },
];
*/

 @NgModule({
   imports: [
     RouterModule.forChild(
       distributorRoutes     )
   ],
   exports: [
     RouterModule
   ]
 })
export class DistributorRoutingModule { }
