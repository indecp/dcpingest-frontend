/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input } from '@angular/core';

import { Distributor, Movie }  from 'app/_models';
import { DistributorsService } from 'app/_services';

import { User } from 'app/_models';
import { AuthenticationService }      from 'app/auth/auth.service';


@Component({
  selector: 'movie-recent',
  templateUrl: './movie-recent.component.html',
  styleUrls: ['./movie-recent.component.css']
})


export class MovieRecentComponent implements OnInit {

  @Input() distributor: Distributor;
  movies: Movie[];
  currentUser: User;

  constructor(
            private distributorsService: DistributorsService,
            private authService: AuthenticationService,
  ) {

  }


  ngOnInit() {

       this.authService.currentUser.subscribe(user => this.currentUser = user);
    this.distributor = this.currentUser.distributor;
  this.getDisributorNewest();

  }

  private getDisributorNewest() {
    this.distributorsService.getNewest(this.distributor.id)
    .subscribe(movies => this.movies = movies);
  }

}
