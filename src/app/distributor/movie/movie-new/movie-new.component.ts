/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  FormGroup, FormBuilder} from '@angular/forms';

import { MovieService } from 'app/_services';
import { Movie }  from 'app/_models';
import { MessageService }  from 'app/message.service';

@Component({
  selector: 'app-movie-new',
  templateUrl: './movie-new.component.html',
  styleUrls: ['./movie-new.component.css']
})
export class MovieNewComponent implements OnInit {
  movieForm: FormGroup;
  movie: Movie;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private movieService: MovieService,
    public dialogServicse: MessageService
  ) {}

  ngOnInit() {


    this.createFromGroup();
  }

  createFromGroup() {
    this.movieForm = this.formBuilder.group({
      // 'id': this.formBuilder.control(),
      'title': this.formBuilder.control(''),
      'releasedate': this.formBuilder.control(''),
      'director' : this.formBuilder.control(''),
      'scenario': this.formBuilder.control(''),
      'actor' : this.formBuilder.control(''),
      'country' : this.formBuilder.control(''),
      'synopsis' : this.formBuilder.control('')
    });
  }

  handleSubmit() {
  console.log(this.movieForm.value);
}

  cancel() {
    this.gotoMovies();
  }

  save() {
    this.movieService
    .addMovie(this.movieForm.value as Movie)
    .subscribe(() => this.gotoMovies());
  }



  gotoMovies() {
    // Pass along the movie id if available
    // so that the CrisisListComponent can select that movie.
    // Add a totally useless `foo` parameter for kicks.
    // Relative navigation back to the  movies
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
