/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, ViewChild, Input}  from '@angular/core';
import { ActivatedRoute }     from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import { Observable }     from 'rxjs';
import { switchMap }      from 'rxjs/operators';



import { DistributorsService } from 'app/_services';
import { Movie, Distributor}  from 'app/_models';
import { User } from 'app/_models';
import { AuthenticationService }      from 'app/auth/auth.service';

@Component({
  selector: 'movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {
  currentUser: User;
  movieMap$: Observable<Movie[]>;
  selectedId: number;
  @Input() distributor: Distributor;

  public displayedColumns = ['affiche', 'id', 'title', 'releasedate', 'FTR', 'TLR', 'action'];
  public dataSource = new MatTableDataSource<Movie>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


 constructor(
    private distributorsService: DistributorsService,
    private route: ActivatedRoute,
    private authService: AuthenticationService,
  ) {}

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }


  ngOnInit() {

    this.authService.currentUser.subscribe(user => this.currentUser = user);
    this.distributor = this.currentUser.distributor;

    this.movieMap$ = this.route.paramMap.pipe(
      switchMap(params => {
        this.selectedId = +params.get('id');
        return this.distributorsService.getMovies(this.currentUser.distributor.id);
      })
    );

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.getMoviesDistributors();

  }

  getMoviesDistributors() {
    this.distributorsService.getMovies(this.distributor.id)
      .subscribe(movie => (this.dataSource.data = movie));
  }

}
