/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input }    from '@angular/core';
import { ActivatedRoute }       from '@angular/router';
import { Observable }           from 'rxjs';
import { map }                  from 'rxjs/operators';

import { User, Distributor }  from 'app/_models';
import { AuthenticationService }      from 'app/auth/auth.service';
import { ElectronService} from 'app/_services';


@Component({
  selector: 'app-distributor-home',
  templateUrl: './distributor-home.component.html',
  styleUrls: ['./distributor-home.component.css']
})

export class DistributorHomeComponent implements OnInit  {

  sessionId: Observable<string>;
  token: Observable<string>;
  clickMessage: any;

  @Input() currentUser: User;
  @Input() distributorid: number;
  distributor: Distributor;



  constructor(
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private electronService: ElectronService
  ) {}




  ngOnInit() {
    // Capture the session ID if available
    this.authService.currentUser.subscribe(user => this.currentUser = user);


    this.sessionId = this.route
      .queryParamMap
      .pipe(map(params => params.get('session_id') || 'None'));

    // Capture the fragment if available
    this.token = this.route
      .fragment
      .pipe(map(fragment => fragment || 'None'));
  }

  onClickMe() {
    this.getFiles().then((val) => this.clickMessage = val);
  }

  async getFiles() {
              return new Promise<string>((resolve, reject) => {
                this.electronService.ipcRenderer.once('selected-directory', (event, arg) => {
                  resolve(arg);
                });

                this.electronService.ipcRenderer.send('open-file-dialog');
              });



    }
}
