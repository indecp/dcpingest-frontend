/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { NgModule }       from '@angular/core';
import { FormsModule, ReactiveFormsModule }    from '@angular/forms';
import { CommonModule }   from '@angular/common';

import {
  MatToolbarModule,
  MatButtonModule,
  MatInputModule,
  MatFormFieldModule,
  MatTableModule,
  MatPaginatorModule,
  MatCardModule,
  MatSortModule,
  MatGridListModule,
} from '@angular/material';


import {FileSizeModule} from 'ngx-filesize';

import { DistributorHomeComponent} from './distributor-home/distributor-home.component';
import { DistributorComponent }     from './distributor/distributor.component';

import {
  MovieDetailComponent,
  MovieListComponent,
  MovieNewComponent,
  MovieRecentComponent
} from './movie';

import {
  DcpDetailComponent,
  DcpListComponent,
  DcpNewComponent
}
from './dcp/';

import { DistributorRoutingModule } from './distributor-routing.module';

import { ElectronService} from 'app/_services';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    DistributorRoutingModule,
    MatSortModule,
    MatCardModule,
    FileSizeModule,
    MatGridListModule
  ],
  declarations: [
    DistributorHomeComponent,
    MovieListComponent,
    MovieDetailComponent,
    MovieNewComponent,
    MovieRecentComponent,
    DcpListComponent,
    DcpDetailComponent,
    DcpNewComponent,
    DistributorComponent,
  ],
  providers: [ElectronService]

})
export class DistributorModule {}
