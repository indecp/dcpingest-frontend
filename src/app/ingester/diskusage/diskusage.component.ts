/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, OnDestroy } from '@angular/core';
import { IngestersService } from 'app/_services';
import { DiskUsage } from 'app/_models';

@Component({
  selector: 'app-diskusage',
  templateUrl: './diskusage.component.html',
  styleUrls: ['./diskusage.component.css']
})
export class DiskusageComponent implements OnInit, OnDestroy {
  interval: any;
  diskusage: DiskUsage;

  constructor(
    private ingestersService: IngestersService,
  ) { }

  ngOnInit() {
    this.getDiskUsage();
    this.interval = setInterval(() => {
      this.getDiskUsage();
    }, 5000);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  getDiskUsage() {
    this.ingestersService.getDiskUsage()
      .subscribe(diskusage => (this.diskusage = diskusage));
  }

}
