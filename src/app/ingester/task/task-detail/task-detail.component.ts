/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

import { Observable, of, from } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';


import { TaskService } from 'app/_services';
import { Task } from 'app/_models';
import { MessageService } from 'app/message.service';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.css']
})
export class TaskDetailComponent implements OnInit, OnDestroy {
  task: Task;
  editName: string;
  interval: any;
  isTaskActive: boolean;
  activeTask: Observable<Task>;
  error: any;
  extend_task: any;



  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private taskService: TaskService,
    public dialogServicse: MessageService
  ) { }

  ngOnInit() {
    // TODO: verify in angulatr routing doc
    // why the this.route.data is needed.

    this.route.data
      .subscribe((data: { task: Task }) => {
        this.editName = data.task.name;
        this.task = data.task;
      });


    this.interval = setInterval(() => {
      this.getTasksContent();
    }, 3000);
  }
  ngOnDestroy() {
    clearInterval(this.interval);
  }


  getTasksContent() {
    this.taskService.getTask(this.task.id)
      .subscribe(
        task => {this.task = task; },
        error => (this.dialogServicse.add(error))
      );


  }// Fin getTasksContent


  goBack() {
    this.gotoTasks();
  }



  canDeactivate(): Observable<boolean> | boolean {
    // Allow synchronous navigation (`true`) if no task or the task is unchanged
    if (!this.task || this.task.name === this.editName) {
      return true;
    }
    // Otherwise ask the user with the dialog service and return its
    // observable which resolves to true or false when the user decides
    // return this.dialogService.add('Discard changes?');
    return false;
  }

  gotoTasks(doDelete: boolean = false) {
    const taskId = this.task ? this.task.id : null;
    // Pass along the task id if available
    // so that the CrisisListComponent can select that task.
    // Add a totally useless `foo` parameter for kicks.
    // Relative navigation back to the  tasks
    this.router.navigate(['/ingester/', { id: taskId , delete: doDelete }]);
  }


  OnDelete(): void {
    if (confirm(
      'Etes vous sur de vouloir suppprimer le DCP ' + this.task.name)) {
      // this.taskService.deleteTasks(this.task).subscribe();
      this.gotoTasks(true);
    }
  }



}
