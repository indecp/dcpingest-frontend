/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';



import { TaskService } from 'app/_services';
import { Task } from 'app/_models';

@Component({
  selector: 'task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {
  taskMap$: Observable<Task[]>;
  selectedId: string;
  interval: any;
  error: any;
  color = 'accent';
  checked = false;
  disabled = false;
  durationInSeconds = 5;

  public displayedColumns = ['id', 'name', 'complete', 'start_date', 'progress', 'action'];
  public dataSource = new MatTableDataSource<Task>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(
    private taskService: TaskService,
    private route: ActivatedRoute,
      private router: Router,
      // private _snackBar: MatSnackBar
  ) { }

  public doFilter = (value: string) => {

    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }


  ngOnInit() {


    this.taskMap$ = this.route.paramMap.pipe(
      switchMap(params => {
        this.selectedId = params.get('id');
        return this.taskService.getTasks();

      })
    );

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.getTasksIngesters();

    this.interval = setInterval(() => {
      this.getTasksIngesters();
    }, 3000);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }
  getTasksIngesters() {
    this.taskService.getTasks()
      .subscribe(tasks => (this.dataSource.data = tasks));
  }


  delete(task: Task): void {
    console.log(task);
    if (confirm('Etes vous sur de vouloir suppprimer le DCP ' + task.name)) {
      console.log('Implement delete functionality here');
      this.dataSource.data = this.dataSource.data.filter(h => h !== task);
      // this.taskService.deleteTask(task).subscribe();

    }
  }

  OnClearComplete(): void {
    this.taskService.ClearTasks()
      .subscribe(
        tasks => (this.dataSource.data = tasks),
        err => (this.error = err)
      );
  }

}
