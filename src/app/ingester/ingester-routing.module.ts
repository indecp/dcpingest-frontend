/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from 'app/auth/auth.guard';

import { IngesterHomeComponent} from './ingester-home/ingester-home.component';
import { IngesterComponent }     from './ingester/ingester.component';
import {
  MovieDetailComponent,
  MovieListComponent,
  MovieNewComponent
}  from './movie/';

import {
  DcpDetailComponent,
  DcpListComponent,
  DcpNewComponent
} from './dcp/';
import {
  TaskDetailComponent,
  TaskListComponent,
  } from './task/';


import { ParametersComponent } from './parameters/parameters.component';

import { DcpDetailResolverService }    from './dcp/dcp-detail-resolver.service';

import { TaskDetailResolverService }    from './task/task-detail-resolver.service';


const ingesterRoutes: Routes = [
  {
    path: '',
    component: IngesterComponent,
    children: [
      {
        path: 'dcp',
        component: DcpListComponent,
        data: { animation: 'movies' },
      },
      { path: 'dcp/new',
          component:  DcpNewComponent,
          data: { animation: 'movie' }
      },
      { path: 'dcp/:id',
          component:  DcpDetailComponent,
          resolve: {
            dcp: DcpDetailResolverService
          },
          data: { animation: 'movie' }
      },
      {
        path: 'task',
        component: TaskListComponent,
        data: { animation: 'movies' },
      },
      {
        path: 'parameters',
        component: ParametersComponent,
      },
      { path: 'task/:id',
          component:  TaskDetailComponent,
          resolve: {
            task: TaskDetailResolverService
          },
          data: { animation: 'movie' }
      },


      {
        path: '',
        component: DcpListComponent
      }
    ]
  }
];

/*
const ingesterRoutes: Routes = [
  {
    path: '',
    component: IngesterComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        children: [
          { path: 'film',  component: IngesterMovieListComponent, data: { animation: 'movies' } },
          { path: 'film/:id',
            component:  MovieDetailComponent,
            resolve: {
              movie: MovieDetailResolverService
            },
            data: { animation: 'movie' }
          },
        ]
      }
    ]
  },
];
*/

 @NgModule({
   imports: [
     RouterModule.forChild(
       ingesterRoutes     )
   ],
   exports: [
     RouterModule
   ]
 })
export class IngesterRoutingModule { }
