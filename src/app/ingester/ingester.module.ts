/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { NgModule }       from '@angular/core';
import { FormsModule, ReactiveFormsModule }    from '@angular/forms';
import { CommonModule }   from '@angular/common';

import {
  MatToolbarModule,
  MatButtonModule,
  MatInputModule,
  MatFormFieldModule,
  MatTableModule,
  MatPaginatorModule,
  MatCardModule,
  MatSortModule,
  MatGridListModule,
  MatDividerModule,
  MatListModule,
  MatProgressSpinnerModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatIconModule,
  MatExpansionModule,
} from '@angular/material';


import {FileSizeModule} from 'ngx-filesize';

import { IngesterHomeComponent} from './ingester-home/ingester-home.component';
import { IngesterComponent }     from './ingester/ingester.component';

import {
  MovieDetailComponent,
  MovieListComponent,
  MovieNewComponent,
  MovieRecentComponent
} from './movie';

import {
  DcpDetailComponent,
  DcpListComponent,
  DcpNewComponent
}
from './dcp/';

import {
  TaskDetailComponent,
  TaskListComponent,
}
from './task/';


import { IngesterRoutingModule } from './ingester-routing.module';

import { ElectronService} from 'app/_services';
import { DiskusageComponent } from './diskusage/diskusage.component';
import { ParametersComponent } from './parameters/parameters.component';

import {
  ShowStateComponent,
  IconCifferComponent
} from 'app/ui';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    IngesterRoutingModule,
    MatSortModule,
    MatCardModule,
    FileSizeModule,
    MatGridListModule,
    MatDividerModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatIconModule,
    MatExpansionModule
  ],
  declarations: [
    IngesterHomeComponent,
    MovieListComponent,
    MovieDetailComponent,
    MovieNewComponent,
    MovieRecentComponent,
    DcpListComponent,
    DcpDetailComponent,
    DcpNewComponent,
    TaskListComponent,
    TaskDetailComponent,
    IngesterComponent,
    DiskusageComponent,
    ParametersComponent,
    ShowStateComponent,
    IconCifferComponent
  ],
  providers: [ElectronService]

})
export class IngesterModule {}
