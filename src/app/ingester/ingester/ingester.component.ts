/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouterOutlet } from '@angular/router';


import { User } from 'app/_models';
import { AuthenticationService }      from 'app/auth/auth.service';
import { slideInAnimation } from 'app/animations';


@Component({
  selector: 'app-ingester',
  templateUrl: './ingester.component.html',
  styleUrls: ['./ingester.component.css'],
  animations: [ slideInAnimation ]
})

export class IngesterComponent implements OnInit {
  currentUser: User;

  constructor(
    private authService: AuthenticationService,
    private router: Router,

  ) {}
  ngOnInit() {
    this.authService.currentUser.subscribe(user => this.currentUser = user);
  }

  logout() {
      this.authService.logout();
      this.router.navigate(['/login']);
  }
  getAnimationData(outlet: RouterOutlet) {
        return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
      }
}
