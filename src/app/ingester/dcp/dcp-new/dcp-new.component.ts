/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  FormGroup, FormBuilder} from '@angular/forms';
import { Validators } from '@angular/forms';


import { Observable }           from 'rxjs';
import { map }                  from 'rxjs/operators';

import { DcpService, ElectronService } from 'app/_services';
import { Dcp, Movie }  from 'app/_models';
import { MessageService }  from 'app/message.service';

@Component({
  selector: 'app-dcp-new',
  templateUrl: './dcp-new.component.html',
  styleUrls: ['./dcp-new.component.css']
})
export class DcpNewComponent implements OnInit {
  dcpForm: FormGroup;
  dcp: Dcp;
  movieId: number;
  movie: Movie;
  clickMessage: any = [''];



  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private dcpService: DcpService,
    public dialogService: MessageService,
    private electronService: ElectronService
  ) {}

  ngOnInit() {
    this.createFromGroup();
  }

  createFromGroup() {
    this.dcpForm = this.formBuilder.group({
      // 'id': this.formBuilder.control(),
      // 'dcp_path': this.formBuilder.control('/home/nicolas/data/DCP_IN/TheThirdMurder_TLR-Date_F_JA-fr_51_2K_PACTE_20180309_TST_IOP_OV'),
      'dcp_path': ['', Validators.required],
      'ingest': this.formBuilder.control(true),

    });
  }

  handleSubmit() {
  console.log(this.dcpForm.value);
}

  cancel() {
    this.gotoDcps();
  }

  save() {
    if (this.dcpForm.value.dcp_path === '') {
      alert('empty path are not valid');
      return;
    }

    console.log(this.dcpForm.value);
    const new_dcp: Dcp = this.dcpForm.value as Dcp;
    console.log(new_dcp);



    this.dcpService
    .addDcp(new_dcp)
    .subscribe(
      dcp => {
        console.log(dcp);
        this.router.navigate(['/ingester/']);
        });

}


  gotoDcps() {
    // Pass along the dcp id if available
    // so that the CrisisListComponent can select that dcp.
    // Add a totally useless `foo` parameter for kicks.
    // Relative navigation back to the  dcp
    this.router.navigate(['/ingester']);
  }

 UpdateDcpPath(val) {
  console.log(val);
  this.clickMessage = val;
  this.dcpForm.setValue({dcp_path: val, ingest: this.dcpForm.value.ingest });
  console.log(this.dcpForm.value);
 }


  onClickMe() {
    this.getFiles().then((val) => { console.log(val); this.UpdateDcpPath(val[0]);
  } );

  }

  async getFiles() {
              return new Promise<string>((resolve, reject) => {
                this.electronService.ipcRenderer.once('selected-directory', (event, arg) => {
                  resolve(arg);
                });

                this.electronService.ipcRenderer.send('open-file-dialog');
              });



    }

}
