/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';



import { IngestersService } from 'app/_services';
import { Dcp } from 'app/_models';

@Component({
  selector: 'dcp-list',
  templateUrl: './dcp-list.component.html',
  styleUrls: ['./dcp-list.component.css']
})
export class DcpListComponent implements OnInit {
  dcpMap$: Observable<Dcp[]>;
  selectedId: number;
  interval: any;
  error: any;
  color = 'accent';
  checked = false;
  disabled = false;
  durationInSeconds = 5;

  public displayedColumns = ['id', 'name', 'valid', 'contentkind', 'size', 'ingest_dcp_state_name', 'ingest_dcp_date',  'action', 'on_seed_active'];
  public dataSource = new MatTableDataSource<Dcp>();
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(
    private ingestersService: IngestersService,
    private route: ActivatedRoute,
      private router: Router,
      private _snackBar: MatSnackBar
  ) { }

  public doFilter = (value: string) => {

    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }


  ngOnInit() {


    this.dcpMap$ = this.route.paramMap.pipe(
      switchMap(params => {
        this.selectedId = +params.get('id');
        if ( params.get('delete') ) {
          console.log ('delete: ' + this.selectedId);
        }
        return this.ingestersService.getDcps();

      })
    );

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.getDcpsIngesters();
    this.interval = setInterval(() => {
      this.getDcpsIngesters();
    }, 3000);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }
  getDcpsIngesters() {
    this.ingestersService.getDcps()
      .subscribe(dcps => (this.dataSource.data = dcps));
  }


  delete(dcp: Dcp): void {
    console.log(dcp);
    if (confirm('Etes vous sur de vouloir suppprimer le DCP ' + dcp.name)) {
      console.log('Implement delete functionality here');
      this.dataSource.data = this.dataSource.data.filter(h => h !== dcp);
      this.ingestersService.deleteDcp(dcp).subscribe();

    }
  }
  onSeed(dcp: Dcp): void {
    console.log(dcp);
    this.ingestersService.seed(dcp).subscribe();
  }

  onIngest(dcp: Dcp): void {
    const message = `ingest démarré pour ${dcp.name}`;
    const action = null;
    this._snackBar.open(message, action, {
      duration: this.durationInSeconds * 1000,
    });
    this.ingestersService.doIngestDcp(dcp, false)
      .subscribe();
    this.getDcpsIngesters();

  }
  OnScan(): void {
    this.ingestersService.scanDcps()
      .subscribe(
        x => console.log(x),
        err => (this.error = err)
      );
  }
}
