/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'app/_models';
import { Subject } from 'rxjs/Subject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { environment } from '../../environments/environment';


@Injectable()
export class UserService {

  	user: User;
  	user$: ReplaySubject<User> = new ReplaySubject(1);
  	isLogged$: ReplaySubject<boolean> = new ReplaySubject(1);
	 constructor(
	    private http: HttpClient
	  ) { }

  register(user: User) {
    return this.http.post('http://127.0.0.1:500/users/register', user);
        }

  delete(id: number) {
    return this.http.delete( `http://127.0.0.1:500/users/${id}`);
        }


	 setAccount(user, store = true) {
    	if (user && !(user instanceof User)) {
     	 user = new User(user);
   	 }
   	 this.user = user;
   	 this.user$.next(this.user);
  }

  getAccount() {
    return this.user;
  }


  get(userId) {
    const url = `http://127.0.0.1:5000/ingestapi/login/${userId}`;
    return this.http.get(url, {})
      .toPromise()
      .then(data => {
          if (!data) {
              return null;
          }
          return data;
      });
  }



}
