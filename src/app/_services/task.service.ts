/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of} from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';


import { MessageService } from 'app/message.service';
import { Task } from 'app/_models';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({ providedIn: 'root' })

export class TaskService {
  private TasksUrl = 'http://localhost:5000/ingestapi/tasks';
  constructor(private http: HttpClient,
    private message_Service: MessageService) { }

  getTasks(): Observable<Task[]> {
    return this.http
      .get<Task[]>(this.TasksUrl)
      .pipe(tap( _ => this.log('fetched task')),
        catchError(this.handleError('getDcps', [])));
  }

  getTask(id: string): Observable<Task> {
   const url = `${this.TasksUrl}/${id}`;
   return this.http.get<Task>(url)
   .pipe(tap(_ => this.log(`fetched task id=${id}`)),
   catchError(this.handleError<Task>(`getTasks id=${id}`)));
  }

  ClearTasks(): Observable<Task[]> {
    const url = `${this.TasksUrl}/clear_tasks`;

    return this.http
      .get<Task[]>(url)
      .pipe(tap( _ => this.log('fetched task')),
        catchError(this.handleError('getDcps', [])));
  }



  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
    console.error(error);
    this.log(`${operation} failed: ${error.message}`);
    return of (result as T); };
  }

  private log(message: string) {
    this.message_Service.add(`DcpService: ${message}`);
  }

}
