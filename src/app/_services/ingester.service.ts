/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Subject } from 'rxjs/Subject';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Ingester, Movie, Dcp, DiskUsage, Parameters} from 'app/_models';

import { MessageService } from 'app/message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type' : 'application/json'})
};

@Injectable({providedIn: 'root'})
export class IngestersService {
 private IngestersUrl = 'http://localhost:5000/ingestapi';

 constructor(private http: HttpClient,
             private message_Service: MessageService) { }



  findMovies(
    filter = '', sortOrder = 'asc',
    pageNumber = 1, pageSize = 50):  Observable<Movie> {
      return this.http.get(this.IngestersUrl, {
        params: new HttpParams()
        .set('filter', filter)
        .set('sortOrder', sortOrder)
        .set('pageNumber', pageNumber.toString())
        .set('pageSize', pageSize.toString())
      }).pipe(
        map( (res: Movie) => res)
      );
    }// Fin findMovies


   getIngesters(): Observable<Ingester[]> {
   return this.http
   .get<Ingester[]>(this.IngestersUrl)
    .pipe(tap(ingesters => this.log('fetched ingesters')),
      catchError(this.handleError('getIngesters', [])));
  }// Fin getIngesters

  getIngestersContent(id: number): Observable<Ingester> {
   const url = `${this.IngestersUrl}/${id}`;
    return this.http.get<Ingester>(url)
     .pipe(tap(_ => this.log(`fetched distrib_s id=${id}`)),
       catchError(this.handleError<Ingester>(`getIngestersContent id=${id}`)));
  }// Fin getIngestersContent

  getNewest(id: number): Observable<Movie[]> {
    const url = `${this.IngestersUrl}/${id}/movies_newest`;
    return this.http.get<Movie[]>(url)
     .pipe(tap(_ => this.log(`fetched distrib_s id=${id}`)),
       catchError(this.handleError<Movie[]>(`getNewest id=${id}`)));
  }

  getMovies(id: number): Observable<Movie[]> {
    const url = `${this.IngestersUrl}/${id}/movies`;
    return this.http.get<Movie[]>(url)
    .pipe(
      tap(_ => this.log(`fetched distrib_s id=${id}`)),
      catchError(this.handleError<Movie[]>(`getMovies id=${id}`)));
  }

  getDcps(): Observable<Dcp[]> {
    const url = `${this.IngestersUrl}/dcps`;
    return this.http.get<Dcp[]>(url)
    .pipe(
      tap(_ => this.log(`fetched dcps`)),
      catchError(this.handleError<Dcp[]>(`getDcps`)));
  }

  doIngestDcp(dcp: Dcp, copy: Boolean): Observable<Dcp> {
   return this.http.post<Dcp>(`${this.IngestersUrl}/dcps/${dcp.id}/ingest`, {'copy': copy}, httpOptions)
  .pipe(tap((dcp: Dcp) => this.log(`Launch ingest for  w/ id=${dcp.id}`)),
  catchError(this.handleError<Dcp>('doIngestDcp')));
  }// F

  getDiskUsage(): Observable<DiskUsage> {
    const url = `${this.IngestersUrl}/misc/diskusage`;
    return this.http.get<DiskUsage>(url)
    .pipe(
      tap(_ => this.log(`get disk usage`)),
      catchError(this.handleError<DiskUsage>(`getDiskUsage`)));
  }

  getIngestParameters(): Observable<Parameters> {
    const url = `${this.IngestersUrl}/misc/parameters`;
    return this.http.get<Parameters>(url)
    .pipe(
      tap(_ => this.log(`getIngestParameters`)),
      catchError(this.handleError<Parameters>(`getIngestParameters error`)));
  }


  scanDcps(): Observable<any> {
    const url = `${this.IngestersUrl}/dcps/scan`;
    return this.http.get<any>(url)
    .pipe(
      tap(_ => this.log(`fetched dcps`)),
      catchError(this.handleError<any>(`scanDcps`)));
  }

  seed(dcp: Dcp | number): Observable<Dcp> {
    const id = typeof dcp === 'number' ? dcp : dcp.id;

    const url = `${this.IngestersUrl}/dcps/${id}/toggle_seed`;

    return this.http.post<Dcp>(url, httpOptions)
    .pipe(tap(_ => this.log(`toggle seed dcp id=${id}`)),
    catchError(this.handleError<Dcp>('deleteDcps')));
  }

  deleteDcp(dcp: Dcp | number): Observable<Dcp> {
    const id = typeof dcp === 'number' ? dcp : dcp.id;

    const url = `${this.IngestersUrl}/dcps/${id}`;

    return this.http.delete<Dcp>(url, httpOptions)
    .pipe(tap(_ => this.log(`delete dcp id=${id}`)),
    catchError(this.handleError<Dcp>('deleteDcps')));
  }

  searchIngesters(term: string): Observable<Ingester[]> {
    if (!term.trim()) {
      return of([]);
    }
      return this.http.get<Ingester[]>(`${this.IngestersUrl}/?name=${term}`)
       .pipe(tap(_ => this.log(`found ingesters matching "$(term)"`)),
         catchError(this.handleError<Ingester[]>('searchIngesters', [])));
  }// Fin searchIngesters

 addIngesters(distrib_s: Ingester): Observable<Ingester> {
    return this.http.post<Ingester>(this.IngestersUrl, distrib_s, httpOptions)
    .pipe(tap((distrib_s: Ingester) => this.log(`added distrib_s w/ id=${distrib_s.name}`)),
      catchError(this.handleError<Ingester>('addIngesters')));
  }// Fin addIngesters

  deleteIngesters(distrib_s: Ingester | number): Observable<Ingester> {
   const id = typeof distrib_s === 'number' ? distrib_s : distrib_s.name;
   const url = `${this.IngestersUrl}/${id}`;
    return this.http.delete<Ingester>(url, httpOptions)
    .pipe(tap(_ => this.log(`delete distrib_s id=${id}`)),
      catchError(this.handleError<Ingester>('deleteIngesters')));
  }// Fin deleteIngesters

  updateIngesters(distrib_s: Ingester): Observable<any> {
    return this.http.put(this.IngestersUrl, distrib_s, httpOptions)
     .pipe(tap(_ => this.log(`updated distrib_s: id=${distrib_s.name}`)),
       catchError(this.handleError<any>('updateIngesters')));
  }// Fin updateIngesters

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
    //  console.error(error);
       this.log(`${operation} failed: ${error.message}`);

        return of (result as T);
    }; }// fin handleError

  private log(message: string) {

    this.message_Service.add(`IngestersService: ${message}`);

 }// Fin log

}// Fin
