/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Subject } from 'rxjs/Subject';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Dcp } from 'app/_models';
import { MessageService } from 'app/message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type' : 'application/json'})
};

@Injectable({providedIn: 'root'})

export class DcpService {
  private DcpsUrl = 'http://localhost:5000/ingestapi/dcps';
  constructor(private http: HttpClient,
              private message_Service: MessageService) { }

  findDcps(
       filter = '', sortOrder = 'asc', sortField= 'id',
    pageNumber = 1, pageSize = 10):  Observable<Dcp> {
      return this.http.get(this.DcpsUrl, {
        params: new HttpParams()
        .set('filter', filter)
        .set('sortOrder', sortOrder)
        .set('sortField', sortField)
        .set('pageNumber', pageNumber.toString())
        .set('pageSize', pageSize.toString())
      }).pipe(
        map( (res: Dcp) => res)
      );
    }// Fin

   getDcps(): Observable<Dcp[]> {
    return this.http
    .get<Dcp[]>(this.DcpsUrl)
    .pipe(tap(dcps => this.log('fetched dcps')),
    catchError(this.handleError('getDcps', [])));
  }// Fin getDcps

   getDcp(id: number): Observable<Dcp> {
    const url = `${this.DcpsUrl}/${id}`;
    return this.http.get<Dcp>(url)
    .pipe(tap(_ => this.log(`fetched dcps id=${id}`)),
    catchError(this.handleError<Dcp>(`getDcps id=${id}`)));
  }// Fin getDcp


  OnScan(dcp: Dcp | number): Observable<any> {
   const id = typeof dcp === 'number' ? dcp : dcp.id;
   const url = `${this.DcpsUrl}/${id}/scan`;
   return this.http.get<any>(url)
   .pipe(tap(_ => this.log('scan dcp')),
   catchError(this.handleError<Dcp>(`getDcps id=${id}`)));
  }// Fi

  ToggleValid(dcp: Dcp | number): Observable<any> {
   const id = typeof dcp === 'number' ? dcp : dcp.id;
   const url = `${this.DcpsUrl}/${id}/toggle_validity`;
   return this.http.get<any>(url)
   .pipe(tap(_ => this.log('scan dcp')),
   catchError(this.handleError<Dcp>(`getDcps id=${id}`)));
  }// Fi
  doIngestDcp(dcp: Dcp, copy: Boolean): Observable<Dcp> {
   return this.http.post<Dcp>(this.DcpsUrl + `/${dcp.id}/ingest`, {'copy': copy}, httpOptions)
  .pipe(tap((dcp: Dcp) => this.log(`Launch ingest for  w/ id=${dcp.id}`)),
  catchError(this.handleError<Dcp>('doIngestDcp')));
 }// Fi

 doCheckLong(dcp: Dcp): Observable<Dcp> {
  return this.http.post<Dcp>(this.DcpsUrl + `/${dcp.id}/do_check_long`, httpOptions)
 .pipe(tap((dcp: Dcp) => this.log(`Launch verification for  w/ id=${dcp.id}`)),
 catchError(this.handleError<Dcp>('doCheckLong')));
}// Fi

 SearchDcp(dcp: Dcp | number): Observable<any> {
 const id = typeof dcp === 'number' ? dcp : dcp.id;
 const url = `${this.DcpsUrl}/${id}/search_dcp`;
 return this.http.get<any>(url)
 .pipe(tap(_ => this.log('Search DCP ')),
 catchError(this.handleError<Dcp>(`search DCP id=${id}`)));
}

SearchDcpNotification(dcp: Dcp | number): Observable<any> {
const id = typeof dcp === 'number' ? dcp : dcp.id;
const url = `${this.DcpsUrl}/${id}/search_dcp/notifications`;
return this.http.get<any>(url)
.pipe(tap(_ => this.log('Notifications for search DCP')),
catchError(this.handleError<Dcp>(`Nofifications for search DCP id=${id}`)));
}


   getShortCheck(): Observable<Dcp[]> {
    return this.http
    .get<Dcp[]>(this.DcpsUrl)
    .pipe(tap(check => this.log('fetched check')),
    catchError(this.handleError('getShortCheckStatus', [])));
  }// Fin getShortCheck

    getShortCheckStatus(id: number): Observable<Dcp> {
      const url = `${this.DcpsUrl}/${id}/short_check_status`;
      return this.http.get<Dcp>(url)
      .pipe(tap(_ => this.log(`fetched check id=${id}`)),
      catchError(this.handleError<Dcp>(`getShortCheckStatus id=${id}`)));

  }// Fin getShortCheckStatus

    getLongCheck(): Observable<Dcp[]> {
      return this.http
      .get<Dcp[]>(this.DcpsUrl)
      .pipe(tap(check_long => this.log('fetched check_long')),
      catchError(this.handleError('getLongCheckStatus', [])));
     }// Fin getLongCheck

   /* getLongCheckStatus(id : number):Observable<Dcp>{
       const url = `${this.DcpsUrl}/${id}/hash_verification_state_name`;
       return this.http.get<Dcp>(url)
       .pipe(tap(_=>this.log(`fetched check_long id=${id}`)),
       catchError(this.handleError<Dcp>(`getLongCheckStatus id=${id}`)));
  }//Fin getLongCheckStatus
*/
    getDateCheck(): Observable<Dcp[]> {
      return this.http
      .get<Dcp[]>(this.DcpsUrl)
      .pipe(tap(check_date => this.log('fetched check_date')),
      catchError(this.handleError('getDateCheckStatus', [])));
     }// Fin getLongCheck

     getDateCheckStatus(id: number): Observable<Dcp> {
       const url = `${this.DcpsUrl}/${id}/hash_verification_date`;
       return this.http.get<Dcp>(url)
       .pipe(tap(_ => this.log(`fetched check_date id=${id}`)),
       catchError(this.handleError<Dcp>(`getDateCheckStatus id=${id}`)));
    }// Fin getDateCheckStatus




  getLongCheckStatus(id: number): Observable<Dcp> {
    const url = `${this.DcpsUrl}/${id}/do_check_long`;
    return this.http.post<Dcp>(url, '' , httpOptions)
    .pipe(tap(_ => this.log(`fetched check_long id=${id}`)),
      catchError(this.handleError<Dcp>(`getLongCheckStatus id=${id}`)));
  }


      getTorrent(): Observable<Dcp[]> {
      return this.http
      .get<Dcp[]>(this.DcpsUrl)
      .pipe(tap(torrent => this.log('fetched torrent')),
      catchError(this.handleError('getTheTorrent', [])));
     }// Fin getTorrent

     getTheTorrent(id: number): Observable<Dcp> {
       const url = `${this.DcpsUrl}/${id}/torrent_creation_state_name`;
       return this.http.get<Dcp>(url)
       .pipe(tap(_ => this.log(`fetched torrent id=${id}`)),
       catchError(this.handleError<Dcp>(`getTheTorrent id=${id}`)));
    }// Fin getTheTorrent



   addDcp(dcp: Dcp): Observable<Dcp> {
    return this.http.post<Dcp>(this.DcpsUrl, dcp, httpOptions)
    .pipe(tap((dcp: Dcp) => this.log(`added dcp w/ id=${dcp.id}`)),
    catchError(this.handleError<Dcp>('addDcp')));
  }// Fin addDcps


  deleteDcps(dcp: Dcp | number): Observable<Dcp> {
    const id = typeof dcp === 'number' ? dcp : dcp.id;

    const url = `${this.DcpsUrl}/${id}`;

    return this.http.delete<Dcp>(url, httpOptions)
    .pipe(tap(_ => this.log(`delete dcp id=${id}`)),
    catchError(this.handleError<Dcp>('deleteDcps')));
  }// Fin deleteDcps

  updateDcp(dcp: Dcp): Observable<any> {
    return this.http.put(this.DcpsUrl + `/${dcp.id}`, dcp, httpOptions)
    .pipe(tap(_ => this.log(`updated dcp id=${dcp.id}`)),
    catchError(this.handleError<any>('updateDcp')));
  }// Fin updateDcps

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
    console.error(error);
    this.log(`${operation} failed: ${error.message}`);
    return of (result as T); };
  }// fin handleError

  private log(message: string) {
    this.message_Service.add(`DcpService: ${message}`);
  }// Fin log

 }// Fin
