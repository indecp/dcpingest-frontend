/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Subject } from 'rxjs/Subject';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Distributor, Movie, Dcp} from 'app/_models';

import { MessageService } from 'app/message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type' : 'application/json'})
};

@Injectable({providedIn: 'root'})
export class DistributorsService {
 private DistributorsUrl = 'http://localhost:5000/ingestapi/distributors';

 constructor(private http: HttpClient,
             private message_Service: MessageService) { }



  findMovies(
    filter = '', sortOrder = 'asc',
    pageNumber = 1, pageSize = 50):  Observable<Movie> {
      return this.http.get(this.DistributorsUrl, {
        params: new HttpParams()
        .set('filter', filter)
        .set('sortOrder', sortOrder)
        .set('pageNumber', pageNumber.toString())
        .set('pageSize', pageSize.toString())
      }).pipe(
        map( (res: Movie) => res)
      );
    }// Fin findMovies


   getDistributors(): Observable<Distributor[]> {
   return this.http
   .get<Distributor[]>(this.DistributorsUrl)
    .pipe(tap(distributors => this.log('fetched distributors')),
      catchError(this.handleError('getDistributors', [])));
  }// Fin getDistributors

  getDistributorsContent(id: number): Observable<Distributor> {
   const url = `${this.DistributorsUrl}/${id}`;
    return this.http.get<Distributor>(url)
     .pipe(tap(_ => this.log(`fetched distrib_s id=${id}`)),
       catchError(this.handleError<Distributor>(`getDistributorsContent id=${id}`)));
  }// Fin getDistributorsContent

  getNewest(id: number): Observable<Movie[]> {
    const url = `${this.DistributorsUrl}/${id}/movies_newest`;
    return this.http.get<Movie[]>(url)
     .pipe(tap(_ => this.log(`fetched distrib_s id=${id}`)),
       catchError(this.handleError<Movie[]>(`getNewest id=${id}`)));
  }

  getMovies(id: number): Observable<Movie[]> {
    const url = `${this.DistributorsUrl}/${id}/movies`;
    return this.http.get<Movie[]>(url)
    .pipe(
      tap(_ => this.log(`fetched distrib_s id=${id}`)),
      catchError(this.handleError<Movie[]>(`getMovies id=${id}`)));
  }

  getDcps(id: number): Observable<Dcp[]> {
    const url = `${this.DistributorsUrl}/${id}/dcps`;
    return this.http.get<Dcp[]>(url)
    .pipe(
      tap(_ => this.log(`fetched distrib id=${id}`)),
      catchError(this.handleError<Dcp[]>(`getDcps id=${id}`)));
  }

  searchDistributors(term: string): Observable<Distributor[]> {
    if (!term.trim()) {
      return of([]);
    }
      return this.http.get<Distributor[]>(`${this.DistributorsUrl}/?name=${term}`)
       .pipe(tap(_ => this.log(`found distributors matching "$(term)"`)),
         catchError(this.handleError<Distributor[]>('searchDistributors', [])));
  }// Fin searchDistributors

 addDistributors(distrib_s: Distributor): Observable<Distributor> {
    return this.http.post<Distributor>(this.DistributorsUrl, distrib_s, httpOptions)
    .pipe(tap((distrib_s: Distributor) => this.log(`added distrib_s w/ id=${distrib_s.name}`)),
      catchError(this.handleError<Distributor>('addDistributors')));
  }// Fin addDistributors

  deleteDistributors(distrib_s: Distributor | number): Observable<Distributor> {
   const id = typeof distrib_s === 'number' ? distrib_s : distrib_s.name;
   const url = `${this.DistributorsUrl}/${id}`;
    return this.http.delete<Distributor>(url, httpOptions)
    .pipe(tap(_ => this.log(`delete distrib_s id=${id}`)),
      catchError(this.handleError<Distributor>('deleteDistributors')));
  }// Fin deleteDistributors

  updateDistributors(distrib_s: Distributor): Observable<any> {
    return this.http.put(this.DistributorsUrl, distrib_s, httpOptions)
     .pipe(tap(_ => this.log(`updated distrib_s: id=${distrib_s.name}`)),
       catchError(this.handleError<any>('updateDistributors')));
  }// Fin updateDistributors

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
    //  console.error(error);
       this.log(`${operation} failed: ${error.message}`);

        return of (result as T);
    }; }// fin handleError

  private log(message: string) {

    this.message_Service.add(`DistributorsService: ${message}`);

 }// Fin log

}// Fin
