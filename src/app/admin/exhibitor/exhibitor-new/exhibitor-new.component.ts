/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  FormGroup, FormBuilder} from '@angular/forms';

import { ExhibitorsService } from '../../../_services';
import { Exhibitor } from '../../../_models';
import { MessageService }  from 'app/message.service';

@Component({
  selector: 'app-exhibitor-new',
  templateUrl: './exhibitor-new.component.html',
  styleUrls: ['./exhibitor-new.component.css']
})
export class ExhibitorNewComponent implements OnInit {

  exhibitor: Exhibitor;
  exhibitorForm: FormGroup;

  constructor(private route: ActivatedRoute,
	          private exhibitorService: ExhibitorsService,
	          private formBuilder: FormBuilder,
	          private router: Router,
	          public dialogServicse: MessageService
           ) { }


  ngOnInit() {
  this.createFromGroup();
  }

    createFromGroup() {
    this.exhibitorForm = this.formBuilder.group({
      'name': this.formBuilder.control(''),
      'contact': this.formBuilder.control(''),
      'cncid' : this.formBuilder.control(''),
      'address': this.formBuilder.control(''),
      'city' : this.formBuilder.control(''),
      'dept' : this.formBuilder.control(''),
      'iptinc' : this.formBuilder.control('')
    });
  }

  handleSubmit() {
  console.log(this.exhibitorForm.value);
}

  cancel() {
    this.gotoExhibitor();
  }

  save() {
    this.exhibitorService
    .addExhibitors(this.exhibitorForm.value as Exhibitor)
    .subscribe(() => this.gotoExhibitor());
  }



  gotoExhibitor() {
	// Pass along the movie id if available
    // so that the CrisisListComponent can select that movie.
    // Add a totally useless `foo` parameter for kicks.
    // Relative navigation back to the  movies
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}

