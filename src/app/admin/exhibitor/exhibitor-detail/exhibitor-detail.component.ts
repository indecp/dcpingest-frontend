/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  FormGroup, FormBuilder} from '@angular/forms';

import { Observable } from 'rxjs';

import { ExhibitorsService } from '../../../_services';
import { Exhibitor } from '../../../_models';
import { MessageService }  from 'app/message.service';

@Component({
  selector: 'app-exhibitor-detail',
  templateUrl: './exhibitor-detail.component.html',
  styleUrls: ['./exhibitor-detail.component.css']
})
export class ExhibitorDetailComponent implements OnInit {

  exhibitor: Exhibitor;
  editName: string;
  exhibitorForm: FormGroup;

  constructor(private route: ActivatedRoute,
	          private exhibitorService: ExhibitorsService,
	          private formBuilder: FormBuilder,
	          private router: Router,
	          public dialogServicse: MessageService
           ) { }

  ngOnInit() {
    this.route.data
      .subscribe((data: { exhibitor: Exhibitor}) => {
        this.editName = data.exhibitor.name;
        this.exhibitor = data.exhibitor;

      });
    this.createFromGroup();
  }

    createFromGroup() {
    this.exhibitorForm = this.formBuilder.group({
      'name': this.formBuilder.control(this.exhibitor.name),
      'contact': this.formBuilder.control(this.exhibitor.contact),
      'cncid' : this.formBuilder.control(this.exhibitor.cncid),
      'address': this.formBuilder.control(this.exhibitor.address),
      'city' : this.formBuilder.control(this.exhibitor.city),
      'dept' : this.formBuilder.control(this.exhibitor.dept),
      'iptinc' : this.formBuilder.control(this.exhibitor.iptinc)
    });
  }


  handleSubmit() {
  console.log(this.exhibitorForm.value);
}

  cancel() {
    this.gotoExhibitor();
  }

  save() {
    this.exhibitorService
    .updateExhibitors(this.exhibitorForm.value as Exhibitor)
    .subscribe(() => this.gotoExhibitor());
  }

  canDeactivate(): Observable<boolean> | boolean {
    // Allow synchronous navigation (`true`) if no movie or the movie is unchanged
    if (!this.exhibitor || this.exhibitor.name === this.editName) {
      return true;
    }
    // Otherwise ask the user with the dialog service and return its
    // observable which resolves to true or false when the user decides
    // return this.dialogService.add('Discard changes?');
    return false;
  }

  gotoExhibitor() {
    const exhibitorId = this.exhibitor ? this.exhibitor.id : null;
    // Pass along the movie id if available
    // so that the CrisisListComponent can select that movie.
    // Add a totally useless `foo` parameter for kicks.
    // Relative navigation back to the  movies
    this.router.navigate(['../', { id: exhibitorId}], { relativeTo: this.route });
  }
}
