/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import {MatTabsModule, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import {fromEvent} from 'rxjs/observable/fromEvent';
import {debounceTime, distinctUntilChanged, startWith, tap, delay} from 'rxjs/operators';
import {merge} from 'rxjs/observable/merge';
import { ActivatedRoute, Router } from '@angular/router';

import { ExhibitorsService } from 'app/_services';
import { Exhibitor } from 'app/_models';

import { AuthenticationService }      from 'app/auth/auth.service';

import { User } from 'app/_models';

@Component({
  selector: 'app-exhibitor-list',
  templateUrl: './exhibitor-list.component.html',
  styleUrls: ['./exhibitor-list.component.css']
})
export class ExhibitorListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'department', 'CNC_Code_autorised', 'ip_tinc', 'Nb_screen', 'free_space', 'delete'];
  dataSource = new MatTableDataSource<Exhibitor>();
  exhibitor: Exhibitor[];
  error: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;


  constructor(
  	private exhibitorService: ExhibitorsService,
    private route: ActivatedRoute,
    private authService: AuthenticationService,
  ) {}

 public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

ngOnInit() {
    this.exhibitorService
      .getExhibitors()
      .subscribe(
        exhibitors => (this.dataSource.data = exhibitors),
        error => (this.error = error)
      );
  this.dataSource.paginator = this.paginator;
  this.dataSource.sort = this.sort;

  }// Fin ngOnInit

}
