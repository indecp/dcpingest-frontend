/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Injectable }             from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
}                                 from '@angular/router';
import { Observable, of, EMPTY }  from 'rxjs';
import { mergeMap, take }         from 'rxjs/operators';


import { ExhibitorsService } from 'app/_services';
import { Exhibitor }   from 'app/_models';

@Injectable({
  providedIn: 'root',
})
export class ExhibitorDetailResolverService implements Resolve<Exhibitor> {
  constructor(
    private exhibitorService: ExhibitorsService,
    private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Exhibitor> | Observable<never> {
    const id = +route.paramMap.get('id');
    return this.exhibitorService.getExhibitorsContent(id).pipe(
      take(1),
       mergeMap( exhibitor => {
        if (exhibitor) {
          return of(exhibitor);
        } else { // id not found
          this.router.navigate(['/admin/exhibitor']);
          return EMPTY;
        }
      })
    );


  }
}
