/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  FormGroup, FormBuilder} from '@angular/forms';

import { DistributorsService } from '../../../_services';
import { Distributor } from '../../../_models';
import { MessageService }  from 'app/message.service';

@Component({
  selector: 'app-distributor-new',
  templateUrl: './distributor-new.component.html',
  styleUrls: ['./distributor-new.component.css']
})
export class DistributorNewComponent implements OnInit {

  distributor: Distributor;
  distributorForm: FormGroup;

  constructor(private route: ActivatedRoute,
	          private distributorsService: DistributorsService,
	          private formBuilder: FormBuilder,
	          private router: Router,
	          public dialogServicse: MessageService
           ) { }


  ngOnInit() {


    this.createFromGroup();
  }

    createFromGroup() {
    this.distributorForm = this.formBuilder.group({
      'name': this.formBuilder.control(''),
      'contact': this.formBuilder.control(''),
      'cnc_code' : this.formBuilder.control(''),
    });
  }



  handleSubmit() {
  console.log(this.distributorForm.value);
}

  cancel() {
    this.gotoDistributors();
  }

  save() {
    this.distributorsService
    .addDistributors(this.distributorForm.value as Distributor)
    .subscribe(() => this.gotoDistributors());
  }



  gotoDistributors() {
	// Pass along the movie id if available
    // so that the CrisisListComponent can select that movie.
    // Add a totally useless `foo` parameter for kicks.
    // Relative navigation back to the  movies
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}


