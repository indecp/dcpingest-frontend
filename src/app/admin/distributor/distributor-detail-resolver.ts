/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Injectable }             from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
}                                 from '@angular/router';
import { Observable, of, EMPTY }  from 'rxjs';
import { mergeMap, take }         from 'rxjs/operators';


import { DistributorsService } from 'app/_services';
import { Distributor}  from 'app/_models';

@Injectable({
  providedIn: 'root',
})
export class DistributorDetailResolverService implements Resolve<Distributor> {
  constructor(
    private distributorService: DistributorsService,
    private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Distributor> | Observable<never> {
    const id = +route.paramMap.get('id');
    return this.distributorService.getDistributorsContent(id).pipe(
      take(1),
       mergeMap( distributor => {
        if (distributor) {
          return of(distributor);
        } else { // id not found
          this.router.navigate(['/admin/distributor']);
          return EMPTY;
        }
      })
    );


  }
}
