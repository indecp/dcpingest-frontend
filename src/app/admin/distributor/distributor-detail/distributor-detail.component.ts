/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  FormGroup, FormBuilder} from '@angular/forms';

import { Observable } from 'rxjs';

import { DistributorsService } from '../../../_services';
import { Distributor } from '../../../_models';
import { MessageService }  from 'app/message.service';


@Component({
  selector: 'app-distributor-detail',
  templateUrl: './distributor-detail.component.html',
  styleUrls: ['./distributor-detail.component.css']
})
export class DistributorDetailComponent implements OnInit {


  distributor: Distributor;
  editName: string;
  distributorForm: FormGroup;

  constructor(private route: ActivatedRoute,
	          private distributorsService: DistributorsService,
	          private formBuilder: FormBuilder,
	          private router: Router,
	          public dialogServicse: MessageService
           ) { }

  ngOnInit() {
    this.route.data
      .subscribe((data: { distributor: Distributor}) => {
        this.editName = data.distributor.name;
        this.distributor = data.distributor;

      });
    this.createFromGroup();
  }

    createFromGroup() {
    this.distributorForm = this.formBuilder.group({
      'name': this.formBuilder.control(this.distributor.name),
      'contact': this.formBuilder.control(this.distributor.contact),
      'cnc_code' : this.formBuilder.control(this.distributor.cnc_code),
      'user': this.formBuilder.control(this.distributor.user),
      'isdcpbay' : this.formBuilder.control(this.distributor.isdcpbay),
    });
  }


  handleSubmit() {
  console.log(this.distributorForm.value);
}

  cancel() {
    this.gotoDistributors();
  }

  save() {
    this.distributorsService
    .updateDistributors(this.distributorForm.value as Distributor)
    .subscribe(() => this.gotoDistributors());
  }

  canDeactivate(): Observable<boolean> | boolean {
    // Allow synchronous navigation (`true`) if no movie or the movie is unchanged
    if (!this.distributor || this.distributor.name === this.editName) {
      return true;
    }
    // Otherwise ask the user with the dialog service and return its
    // observable which resolves to true or false when the user decides
    // return this.dialogService.add('Discard changes?');
    return false;
  }

  gotoDistributors() {
    const distributorId = this.distributor ? this.distributor.id : null;
    // Pass along the movie id if available
    // so that the CrisisListComponent can select that movie.
    // Add a totally useless `foo` parameter for kicks.
    // Relative navigation back to the  movies
    this.router.navigate(['../', { id: distributorId }], { relativeTo: this.route });
  }
}

