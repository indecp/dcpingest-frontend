/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from 'app/auth/auth.guard';

import { AdminComponent } from './admin/admin.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';

import { MovieListComponent,
    		 MovieDetailComponent,
    		 MovieNewComponent
	} from './movie';

import { DcpListComponent,
	       DcpDetailComponent,
         DcpNewComponent
	} from './dcp/';

import { DistributorListComponent,
         DistributorDetailComponent,
         DistributorNewComponent
  } from './distributor/';

import { ExhibitorListComponent,
         ExhibitorDetailComponent,
         ExhibitorNewComponent
  } from './exhibitor/';


import { MovieDetailResolverService }    from './movie-detail-resolver.service';
import { DcpDetailResolverService }    from './dcp/dcp-detail-resolver.service';
import { DistributorDetailResolverService }    from './distributor/distributor-detail-resolver';
import { ExhibitorDetailResolverService }    from './exhibitor/exhibitor-detail-resolver';

const routes: Routes = [

 {
   path : '',
   component: AdminComponent,
   children: [
   		{
    	  path: 'film',
   		  component: MovieListComponent,
   		  data: { animation: 'movies' },
   		},

   		{
    	  path: 'film/new',
   		  component: MovieNewComponent,
   		  data: { animation: 'movie' },
   		},

   		{
   		  path: 'film/:id',
          component:  MovieDetailComponent,
          data: { animation: 'movie' },
          resolve: {
            movie: MovieDetailResolverService
          			  },
         },

         {
	        path: 'dcp',
	        component: DcpListComponent,
	        data: { animation: 'movies' },
         },

         {
          path: 'dcp/new',
          component:  DcpNewComponent,
          data: { animation: 'movie' }
         },

         {
          path: 'dcp/:id',
          component: DcpDetailComponent,
          data: { animation: 'movie' },
          resolve: {
            dcp: DcpDetailResolverService
              },
         },

         {
          path: 'distributor',
          component: DistributorListComponent,
          data: { animation: 'movies' },
         },

         {
          path: 'distributor/new',
          component: DistributorNewComponent,
          data: { animation: 'movie' },
         },

         {
          path: 'distributor/:id',
          component: DistributorDetailComponent,
          data: { animation: 'movie' },
          resolve: {
            distributor: DistributorDetailResolverService
                },
         },

         {
          path: 'exhibitor',
          component: ExhibitorListComponent,
          data: { animation: 'movies' },
         },

         {
          path: 'exhibitor/new',
          component: ExhibitorNewComponent,
          data: { animation: 'movie' },
         },

         {
          path: 'exhibitor/:id',
          component: ExhibitorDetailComponent,
          data: { animation: 'movie' },
          resolve: {
            exhibitor: ExhibitorDetailResolverService
                },
         },

   		 {
   		 	path: '',
   		 	component: AdminHomeComponent,
   		 },


   ]
 }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
