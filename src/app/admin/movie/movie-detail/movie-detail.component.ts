/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  FormGroup, FormBuilder} from '@angular/forms';

import { Observable } from 'rxjs';

import { MovieService } from 'app/_services';
import { Movie }  from 'app/_models';
import { MessageService }  from 'app/message.service';


@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})

export class MovieDetailComponent implements OnInit {

  movie: Movie;
  editName: string;
  movieForm: FormGroup;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private movieService: MovieService,
    public dialogServicse: MessageService
  ) {}

  ngOnInit() {
    // TODO: verify in angulatr routing doc
    // why the this.route.data is needed.

    this.route.data
      .subscribe((data: { movie: Movie }) => {
        this.editName = data.movie.title;
        this.movie = data.movie;

      });
    this.createFromGroup();
  }

  createFromGroup() {
    this.movieForm = this.formBuilder.group({
      'title': this.formBuilder.control(this.movie.title),
      'releasedate': this.formBuilder.control(this.movie.releasedate),
      'director' : this.formBuilder.control(this.movie.director),
      'scenario': this.formBuilder.control(this.movie.scenario),
      'actor' : this.formBuilder.control(this.movie.actor),
      'country' : this.formBuilder.control(this.movie.country),
      'synopsis' : this.formBuilder.control(this.movie.synopsis)
    });
  }

  handleSubmit() {
  console.log(this.movieForm.value);
}

  cancel() {
    this.gotoMovies();
  }

  save() {
    this.movieService
    .updateMovie(this.movieForm.value as Movie)
    .subscribe(() => this.gotoMovies());
  }

  canDeactivate(): Observable<boolean> | boolean {
    // Allow synchronous navigation (`true`) if no movie or the movie is unchanged
    if (!this.movie || this.movie.title === this.editName) {
      return true;
    }
    // Otherwise ask the user with the dialog service and return its
    // observable which resolves to true or false when the user decides
    // return this.dialogService.add('Discard changes?');
    return false;
  }

  gotoMovies() {
    const movieId = this.movie ? this.movie.id : null;
    // Pass along the movie id if available
    // so that the CrisisListComponent can select that movie.
    // Add a totally useless `foo` parameter for kicks.
    // Relative navigation back to the  movies
    this.router.navigate(['../', { id: movieId }], { relativeTo: this.route });
  }
}
