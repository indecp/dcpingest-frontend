/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input , ElementRef} from '@angular/core';
import { FormControl, FormGroup, FormBuilder, FormArray, Validators} from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { Movie, Distributor } from 'app/_models';
import { MovieService, DistributorsService } from 'app/_services';


import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'movie-new',
  templateUrl: './movie-new.component.html',
  styleUrls: ['./movie-new.component.css']
})

export class MovieNewComponent implements OnInit {

 movie: Movie;
  signMovie: FormGroup;
  dataSourceDistributors: Distributor[];

constructor(private formBuilder: FormBuilder,
            private movieService: MovieService,
            private distributorsService: DistributorsService,
            private route: ActivatedRoute,
            private router: Router) {}

  ngOnInit() {
    this.createFromGroup();

    this.distributorsService
      .getDistributors()
       .subscribe(
   distributor => (this.dataSourceDistributors = distributor)
        );
  }// Fin ngOnInit


  createFromGroup() {
  this.signMovie = this.formBuilder.group({
      'title': this.formBuilder.control(''),
      'synopsis': this.formBuilder.control(''),
      'director': this.formBuilder.control(''),
      'scenario': this.formBuilder.control(''),
      'actor': this.formBuilder.control(''),
      'country': this.formBuilder.control(''),
      'distributorsList' : this.formBuilder.control('')
    });
  }// Fin createFromGroup

  handleSubmit() {
  console.log(this.signMovie.value);
}


  onSubmit() {
    this.movieService
    .addMovie(this.signMovie.value as Movie)
      .subscribe(() => this.gotoMovies());
  }// FIN onSubmit


  gotoMovies() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }


}// Fin
