/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  FormGroup, FormBuilder} from '@angular/forms';

import { DcpService } from 'app/_services';
import { Dcp }  from 'app/_models';
import { MessageService }  from 'app/message.service';

@Component({
  selector: 'app-dcp-new',
  templateUrl: './dcp-new.component.html',
  styleUrls: ['./dcp-new.component.css']
})
export class DcpNewComponent implements OnInit {

  dcpForm: FormGroup;
  dcp: Dcp;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private dcpService: DcpService,
    public dialogServicse: MessageService
  ) {}

  ngOnInit() {


    this.createFromGroup();
  }

  createFromGroup() {
    this.dcpForm = this.formBuilder.group({
      // 'id': this.formBuilder.control(),
      'name': this.formBuilder.control(''),
      'contentkind': this.formBuilder.control(''),
      'size' : this.formBuilder.control(''),
    });
  }

  handleSubmit() {
  console.log(this.dcpForm.value);
}

  cancel() {
    this.gotoDcps();
  }

  save() {
    this.dcpService
    .addDcp(this.dcpForm.value as Dcp)
    .subscribe(() => this.gotoDcps());
  }



  gotoDcps() {
    // Pass along the dcp id if available
    // so that the CrisisListComponent can select that dcp.
    // Add a totally useless `foo` parameter for kicks.
    // Relative navigation back to the  dcps
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
