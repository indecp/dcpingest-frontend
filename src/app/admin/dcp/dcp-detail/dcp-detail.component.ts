/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {  FormGroup, FormBuilder} from '@angular/forms';

import { Observable } from 'rxjs';

import { DcpService } from 'app/_services';
import { Dcp }  from 'app/_models/dcp';
import { MessageService }  from 'app/message.service';

@Component({
  selector: 'app-dcp-detail',
  templateUrl: './dcp-detail.component.html',
  styleUrls: ['./dcp-detail.component.css']
})
export class DcpDetailComponent implements OnInit {

  dcp: Dcp;
  editName: string;
  dcpForm: FormGroup;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private dcpService: DcpService,
    public dialogServicse: MessageService
  ) {}

  ngOnInit() {
    // TODO: verify in angulatr routing doc
    // why the this.route.data is needed.

   this.route.data
   	.subscribe((data: { dcp: Dcp }) => {
   		this.editName = data.dcp.name;
   		this.dcp = data.dcp;
   		this.createFromGroup();
   	});
  }

  createFromGroup() {
    this.dcpForm = this.formBuilder.group({
      'id': this.formBuilder.control(this.dcp.id),
      'name': this.formBuilder.control(this.dcp.name),
      'contentkind': this.formBuilder.control(this.dcp.contentkind),
      'size' : this.formBuilder.control(this.dcp.size),
    });
  }

  handleSubmit() {
  console.log(this.dcpForm.value);
}

  cancel() {
    this.gotoDcps();
  }

  save() {
    this.dcpService
    .updateDcp(this.dcpForm.value as Dcp)
    .subscribe(() => this.gotoDcps());
  }

  canDeactivate(): Observable<boolean> | boolean {
    // Allow synchronous navigation (`true`) if no dcp or the dcp is unchanged
    if (!this.dcp || this.dcp.name === this.editName) {
      return true;
    }
    // Otherwise ask the user with the dialog service and return its
    // observable which resolves to true or false when the user decides
    // return this.dialogService.add('Discard changes?');
    return false;
  }

  gotoDcps() {
    const dcpId = this.dcp ? this.dcp.id : null;
    // Pass along the dcp id if available
    // so that the CrisisListComponent can select that dcp.
    // Add a totally useless `foo` parameter for kicks.
    // Relative navigation back to the  dcps
    this.router.navigate(['../', { id: dcpId }], { relativeTo: this.route });
  }
}
