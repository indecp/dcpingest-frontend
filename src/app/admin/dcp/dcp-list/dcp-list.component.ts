/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import {MatTabsModule, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import {fromEvent} from 'rxjs/observable/fromEvent';
import {debounceTime, distinctUntilChanged, startWith, tap, delay} from 'rxjs/operators';
import {merge} from 'rxjs/observable/merge';
import { ActivatedRoute, Router } from '@angular/router';

import { DcpService } from '../../../_services/dcp.service';

import { DcpsDataSource }  from '../dcps.datasource' ;

@Component({
  selector: 'app-dcp-list',
  templateUrl: './dcp-list.component.html',
  styleUrls: ['./dcp-list.component.css']
})

export class DcpListComponent implements OnInit {

  nbMovies: Number = 80;
  dataSource: DcpsDataSource;

  public displayedColumns = ['id', 'name', 'contentkind', 'size', 'action'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;

 constructor(private dcpService: DcpService,
             private router: Router,
             private route: ActivatedRoute) {}



  ngOnInit() {
      this.dataSource = new DcpsDataSource(this.dcpService);
      this.dataSource.loadDcps('', 'desc', 'name', 1, 50);
      this.dataSource.dcpsTotal.subscribe
    ( total => { this.nbMovies = total; } );
  }

 ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 1);

    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;

          this.loadDcpPage();
        })
      )
      .subscribe();

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadDcpPage())
      )
      .subscribe();
  }

  loadDcpPage() {
    this.dataSource.loadDcps(
      this.input.nativeElement.value,
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }



}
