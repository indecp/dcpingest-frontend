/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, Input }    from '@angular/core';
import { ActivatedRoute }       from '@angular/router';
import { Observable }           from 'rxjs';
import { map }                  from 'rxjs/operators';

import { User, Exhibitor } from 'app/_models';
import { AuthenticationService }      from 'app/auth/auth.service';


@Component({
  selector: 'app-exhibitor-home',
  templateUrl: './exhibitor-home.component.html',
  styleUrls: ['./exhibitor-home.component.css']
})
export class ExhibitorHomeComponent implements OnInit {

 sessionId: Observable<string>;
  token: Observable<string>;


  @Input() currentUser: User;
  @Input() exhibitorid: number;
  exhibitor: Exhibitor;


  constructor(
    private route: ActivatedRoute,
    private authService: AuthenticationService
  ) {}


  ngOnInit() {
    // Capture the session ID if available
    this.authService.currentUser.subscribe(user => this.currentUser = user);


    this.sessionId = this.route
      .queryParamMap
      .pipe(map(params => params.get('session_id') || 'None'));

    // Capture the fragment if available
    this.token = this.route
      .fragment
      .pipe(map(fragment => fragment || 'None'));
  }

}
