/*****************************************************************************
 * This program is a web interface for dcp ingest.
 *
 * Copyright (C) 2019 Nicolas Bertrand <nicolas@indecp.org>
 *
 * This file is part of dcpingest-frontend
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

import { Component, OnInit, ViewChild, ElementRef, AfterViewInit , Input} from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { ActivatedRoute , Router} from '@angular/router';
import { Location } from '@angular/common';
import {fromEvent} from 'rxjs/observable/fromEvent';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {merge} from 'rxjs/observable/merge';

import { MoviesDataSource } from '../movies.datasource';
import { MovieService} from 'app/_services';
import { Movie, Distributor, Dcp }  from 'app/_models';
import { User } from 'app/_models';
import { AuthenticationService }      from 'app/auth/auth.service';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  @Input() movie: Movie;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;

  constructor(private route: ActivatedRoute,
              private movieService: MovieService,
              private location: Location,
              private router: Router) { }

  ngOnInit() {
    this.getFilm();

}
  getFilm(): void {
    const id = +this.route.snapshot.paramMap.get('id');

    this.movieService.getMovie(id)
      .subscribe(movie => this.movie = movie);


  }// Fin getFilm



  goBack(): void {
    this.location.back();
  }// Fin goBack

}// END
