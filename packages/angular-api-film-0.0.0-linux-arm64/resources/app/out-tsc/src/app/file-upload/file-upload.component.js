var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component } from '@angular/core';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
var FileUploadComponent = /** @class */ (function () {
    function FileUploadComponent() {
        this.URL = 'http://localhost:4200/api/upload';
        this.uploader = new FileUploader({
            url: this.URL,
        });
    }
    FileUploadComponent.prototype.ngOnInit = function () {
        this.uploader.onAfterAddingFile = function (file) {
            console.log('***** onAfterAddingFile ******');
            console.log('file ', file);
        };
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            console.log('ImageUpload:uploaded:', item, status, response);
            alert('Fichier envoyé avec succès');
        };
        this.uploader.onCompleteAll = function () {
            console.log('******* onCompleteAll *********');
        };
        this.uploader.onWhenAddingFileFailed = function (item, filter, options) {
            console.log('***** onWhenAddingFileFailed ********');
            alert('Erreur veuillez recommencer');
        };
    };
    FileUploadComponent = __decorate([
        Component({
            selector: 'app-file-upload',
            templateUrl: './file-upload.component.html',
            styleUrls: ['./file-upload.component.css']
        })
    ], FileUploadComponent);
    return FileUploadComponent;
}());
export { FileUploadComponent };
//# sourceMappingURL=file-upload.component.js.map