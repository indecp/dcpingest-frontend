import { async, TestBed } from '@angular/core/testing';
import { ExhibitorsListForDistributorsComponent } from './exhibitors-list-for-distributors.component';
describe('ExhibitorsListForDistributorsComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [ExhibitorsListForDistributorsComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(ExhibitorsListForDistributorsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=exhibitors-list-for-distributors.component.spec.js.map