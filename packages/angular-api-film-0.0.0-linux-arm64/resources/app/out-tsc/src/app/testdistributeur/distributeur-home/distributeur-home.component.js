var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ROLE } from '../../user/user.model';
import { UserService } from '../../user/user.service';
var DistributeurHomeComponent = /** @class */ (function () {
    function DistributeurHomeComponent(userService) {
        var _this = this;
        this.userService = userService;
        this.cats = [];
        this.isLogged = false;
        this.userSubscription = this.userService.user$.subscribe(function (user) {
            _this.isLogged = user ? true : false;
            _this.user = user;
            // this.generateCats(user);
            // this.getHome(user);
        });
    }
    DistributeurHomeComponent.prototype.ngOnInit = function () {
    };
    DistributeurHomeComponent.prototype.getRole = function (user) {
        if (!user || !user.ID) {
            console.debug('No user');
            return;
        }
        return ROLE[user.role];
    };
    DistributeurHomeComponent = __decorate([
        Component({
            selector: 'app-distributeur-home',
            templateUrl: './distributeur-home.component.html',
            styleUrls: ['./distributeur-home.component.css']
        }),
        __metadata("design:paramtypes", [UserService])
    ], DistributeurHomeComponent);
    return DistributeurHomeComponent;
}());
export { DistributeurHomeComponent };
//# sourceMappingURL=distributeur-home.component.js.map