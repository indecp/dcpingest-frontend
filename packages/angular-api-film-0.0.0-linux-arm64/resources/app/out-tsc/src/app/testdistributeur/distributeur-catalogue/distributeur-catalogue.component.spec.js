import { async, TestBed } from '@angular/core/testing';
import { DistributeurCatalogueComponent } from './distributeur-catalogue.component';
describe('DistributeurCatalogueComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [DistributeurCatalogueComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(DistributeurCatalogueComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=distributeur-catalogue.component.spec.js.map