import { async, TestBed } from '@angular/core/testing';
import { DistributeurSalleConnectesComponent } from './distributeur-salle-connectes.component';
describe('DistributeurSalleConnectesComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [DistributeurSalleConnectesComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(DistributeurSalleConnectesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=distributeur-salle-connectes.component.spec.js.map