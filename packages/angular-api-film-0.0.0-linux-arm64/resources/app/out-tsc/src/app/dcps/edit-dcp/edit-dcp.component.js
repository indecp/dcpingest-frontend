var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DcpsService } from '../dcps.service';
var EditDcpComponent = /** @class */ (function () {
    function EditDcpComponent(formBuilder, dcpService, http, location, route) {
        this.formBuilder = formBuilder;
        this.dcpService = dcpService;
        this.http = http;
        this.location = location;
        this.route = route;
        this.TypeDCP = ['FTR', 'TLR'];
        this.dcpForm = this.createFromGroup();
    }
    EditDcpComponent.prototype.createFromGroup = function () {
        return new FormGroup({
            id: new FormControl(),
            dcpname: new FormControl(),
            contentkind: new FormControl(),
            size: new FormControl(),
        });
    };
    ; //Fin createFromGroup
    EditDcpComponent.prototype.goBack = function () {
        this.location.back();
    }; //Fin goBack
    EditDcpComponent.prototype.onSubmit = function () {
        var _this = this;
        this.dcpService
            .updateDcps(this.dcpForm.value)
            .subscribe(function () { return _this.goBack(); });
    }; //Fin onSubmit
    EditDcpComponent.prototype.ngOnInit = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.dcpService.getDcp(id)
            .subscribe(function (dcp) {
            _this.dcpForm.patchValue(dcp);
        });
    }; //Fin ngOnInit
    EditDcpComponent = __decorate([
        Component({
            selector: 'app-edit-dcp',
            templateUrl: './edit-dcp.component.html',
            styleUrls: ['./edit-dcp.component.css']
        }),
        __metadata("design:paramtypes", [FormBuilder,
            DcpsService,
            HttpClient,
            Location,
            ActivatedRoute])
    ], EditDcpComponent);
    return EditDcpComponent;
}()); //Fin
export { EditDcpComponent };
//# sourceMappingURL=edit-dcp.component.js.map