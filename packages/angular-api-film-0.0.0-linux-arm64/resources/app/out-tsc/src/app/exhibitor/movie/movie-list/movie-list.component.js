var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { merge } from "rxjs/observable/merge";
import { ActivatedRoute, Router } from '@angular/router';
import { MoviesDataSource } from '../movies.datasource';
import { MovieService } from '../../../_services/movie.service';
var MovieListComponent = /** @class */ (function () {
    function MovieListComponent(router, route, movieService) {
        this.router = router;
        this.route = route;
        this.movieService = movieService;
        this.nbAll = 80;
        this.displayedColumns = ['affiche', 'demander', 'name', 'date', 'FTR', 'TLR'];
    }
    /*
       TODO: implement movie search or not needed ?
       public doFilter = (value: string) => {
       this.dataSource.filter = value.trim().toLocaleLowerCase();
     }*/
    MovieListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataSource = new MoviesDataSource(this.movieService);
        this.dataSource.loadMovies('', 'desc', 1, 50),
            this.dataSource.moviesTotal.subscribe(function (total) { _this.nbAll = total; });
    };
    MovieListComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 1; });
        fromEvent(this.input.nativeElement, 'keyup')
            .pipe(debounceTime(150), distinctUntilChanged(), tap(function () {
            _this.paginator.pageIndex = 0;
            _this.loadMoviesAll();
        })).subscribe();
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(tap(function () { return _this.loadMoviesAll(); }))
            .subscribe();
    };
    MovieListComponent.prototype.loadMoviesAll = function () {
        this.dataSource.loadMovies(this.input.nativeElement.value, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
    };
    __decorate([
        ViewChild(MatPaginator),
        __metadata("design:type", MatPaginator)
    ], MovieListComponent.prototype, "paginator", void 0);
    __decorate([
        ViewChild(MatSort),
        __metadata("design:type", MatSort)
    ], MovieListComponent.prototype, "sort", void 0);
    __decorate([
        ViewChild('input'),
        __metadata("design:type", ElementRef)
    ], MovieListComponent.prototype, "input", void 0);
    MovieListComponent = __decorate([
        Component({
            selector: 'app-movie-list',
            templateUrl: './movie-list.component.html',
            styleUrls: ['./movie-list.component.css']
        }),
        __metadata("design:paramtypes", [Router,
            ActivatedRoute,
            MovieService])
    ], MovieListComponent);
    return MovieListComponent;
}());
export { MovieListComponent };
//# sourceMappingURL=movie-list.component.js.map