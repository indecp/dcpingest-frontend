var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from '../movies.service';
import { Location } from '@angular/common';
var EditMovieComponent = /** @class */ (function () {
    function EditMovieComponent(formBuilder, movieService, http, location, route) {
        this.formBuilder = formBuilder;
        this.movieService = movieService;
        this.http = http;
        this.location = location;
        this.route = route;
        this.movieForm = this.createFromGroup();
    }
    EditMovieComponent.prototype.createFromGroup = function () {
        return new FormGroup({
            id: new FormControl(),
            title: new FormControl(),
            synopsis: new FormControl(),
            director: new FormControl(),
            scenario: new FormControl(),
            actor: new FormControl(),
            country: new FormControl(),
        });
    };
    ; //Fin createFromGroup
    EditMovieComponent.prototype.goBack = function () {
        this.location.back();
    }; //Fin goBack
    EditMovieComponent.prototype.onSubmit = function () {
        var _this = this;
        this.movieService
            .updateMovies(this.movieForm.value)
            .subscribe(function () { return _this.goBack(); });
    }; //Fin onSubmit
    EditMovieComponent.prototype.ngOnInit = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.movieService.getMovie(id)
            .subscribe(function (movie) {
            _this.movieForm.patchValue(movie);
        });
    }; //Fin ngOnInit
    EditMovieComponent = __decorate([
        Component({
            selector: 'app-edit-movie',
            templateUrl: './edit-movie.component.html',
            styleUrls: ['./edit-movie.component.css']
        }),
        __metadata("design:paramtypes", [FormBuilder,
            MoviesService,
            HttpClient,
            Location,
            ActivatedRoute])
    ], EditMovieComponent);
    return EditMovieComponent;
}()); //Fin
export { EditMovieComponent };
//# sourceMappingURL=edit-movie.component.js.map