var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { MoviesService } from '../movies.service';
var MovieSearchComponent = /** @class */ (function () {
    function MovieSearchComponent(movies_Service) {
        this.movies_Service = movies_Service;
        this.searchTerms = new Subject();
    }
    MovieSearchComponent.prototype.ngOnInit = function () {
        /* this.movies$ = this.searchTerms.pipe(
           debounceTime(300),
           distinctUntilChanged(),
           switchMap((term: string) => this.movies_Service.searchMovies(term)));
       */ 
    }; //Fin ngOnInit
    MovieSearchComponent = __decorate([
        Component({
            selector: 'app-movie-search',
            templateUrl: './movie-search.component.html',
            styleUrls: ['./movie-search.component.css']
        }),
        __metadata("design:paramtypes", [MoviesService])
    ], MovieSearchComponent);
    return MovieSearchComponent;
}()); //Fin
export { MovieSearchComponent };
//# sourceMappingURL=movie-search.component.js.map