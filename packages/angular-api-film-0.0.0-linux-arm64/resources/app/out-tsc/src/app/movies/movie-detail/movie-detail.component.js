var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Movie } from '../movies';
import { MoviesService } from '../movies.service';
var MovieDetailComponent = /** @class */ (function () {
    function MovieDetailComponent(route, movieService, location) {
        this.route = route;
        this.movieService = movieService;
        this.location = location;
        this.displayedColumns = ['dcpid', 'dcpname', 'contentkind', 'size'];
    }
    MovieDetailComponent.prototype.ngOnInit = function () {
        this.getFilm();
    }; //Fin ngOnInit
    MovieDetailComponent.prototype.getFilm = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.movieService.getMovie(id)
            .subscribe(function (movie) { return _this.movie = movie; });
    }; //Fin getFilm
    MovieDetailComponent.prototype.goBack = function () {
        this.location.back();
    }; //Fin goBack
    __decorate([
        Input(),
        __metadata("design:type", Movie)
    ], MovieDetailComponent.prototype, "movie", void 0);
    MovieDetailComponent = __decorate([
        Component({
            selector: 'app-movie-detail',
            templateUrl: './movie-detail.component.html',
            styleUrls: ['./movie-detail.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute,
            MoviesService,
            Location])
    ], MovieDetailComponent);
    return MovieDetailComponent;
}()); //Fin
export { MovieDetailComponent };
//# sourceMappingURL=movie-detail.component.js.map