var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { MoviesService } from '../../movies/movies.service';
var MovieShortComponent = /** @class */ (function () {
    function MovieShortComponent(movieService) {
        this.movieService = movieService;
    }
    MovieShortComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.movieService.getMovie(this.movieid)
            .subscribe(function (movie) { return _this.movie = movie; });
    }; //Fin ngOnInit
    __decorate([
        Input(),
        __metadata("design:type", Number)
    ], MovieShortComponent.prototype, "movieid", void 0);
    MovieShortComponent = __decorate([
        Component({
            selector: 'app-movie-short',
            templateUrl: './movie-short.component.html',
            styleUrls: ['./movie-short.component.css']
        }),
        __metadata("design:paramtypes", [MoviesService])
    ], MovieShortComponent);
    return MovieShortComponent;
}()); //Fin
export { MovieShortComponent };
//# sourceMappingURL=movie-short.component.js.map