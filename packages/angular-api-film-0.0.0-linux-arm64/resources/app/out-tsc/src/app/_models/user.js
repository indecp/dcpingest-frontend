export var ROLE = {
    1: 'admin',
    2: 'distributor',
    3: 'exhibitor'
};
var User = /** @class */ (function () {
    function User(data) {
        Object.assign(this, data);
    }
    User.prototype.isAdministrator = function () {
        return this.role === 1;
    };
    User.prototype.isDistributor = function () {
        return this.role === 2;
    };
    User.prototype.isExhibitor = function () {
        return this.role === 3;
    };
    User.prototype.getRoleTxt = function () {
        return [this.role];
    };
    return User;
}());
export { User };
;
//# sourceMappingURL=user.js.map