var Movie = /** @class */ (function () {
    function Movie() {
        this.title = '';
        this.original_tilte = '';
        this.synopsis = '';
        this.director = '';
        this.scenario = '';
        this.actor = '';
        this.country = '';
    }
    return Movie;
}());
export { Movie };
//# sourceMappingURL=movie.js.map