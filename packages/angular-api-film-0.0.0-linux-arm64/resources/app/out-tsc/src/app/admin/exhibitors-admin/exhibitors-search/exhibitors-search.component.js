var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ExhibitorsService } from '../exhibitors.service';
var ExhibitorsSearchComponent = /** @class */ (function () {
    function ExhibitorsSearchComponent(exhibitors_Service) {
        this.exhibitors_Service = exhibitors_Service;
        this.searchTerms = new Subject();
    }
    ExhibitorsSearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.exhib$ = this.searchTerms.pipe(debounceTime(300), distinctUntilChanged(), switchMap(function (term) { return _this.exhibitors_Service.searchExhibitors(term); }));
    }; //Fin ngOnInit
    ExhibitorsSearchComponent.prototype.search = function (term) {
        this.searchTerms.next(term);
    }; //Fin search
    ExhibitorsSearchComponent = __decorate([
        Component({
            selector: 'app-exhibitors-search',
            templateUrl: './exhibitors-search.component.html',
            styleUrls: ['./exhibitors-search.component.css']
        }),
        __metadata("design:paramtypes", [ExhibitorsService])
    ], ExhibitorsSearchComponent);
    return ExhibitorsSearchComponent;
}()); //Fin
export { ExhibitorsSearchComponent };
//# sourceMappingURL=exhibitors-search.component.js.map