var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Dcp } from '../dcps';
import { DcpsService } from '../dcps.service';
import { registerLocaleData } from '@angular/common';
import { ROLE } from '../../../user/user.model';
import { UserService } from '../../../user/user.service';
import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';
registerLocaleData(localeFr, 'fr-FR', localeFrExtra);
var DcpsDetailComponent = /** @class */ (function () {
    function DcpsDetailComponent(route, location, userService, dcpService) {
        var _this = this;
        this.route = route;
        this.location = location;
        this.userService = userService;
        this.dcpService = dcpService;
        this.isLogged = false;
        this.displayedColumns = ['dcpid', 'dcpname', 'contentkind', 'size'];
        this.dataSource = new MatTableDataSource();
        this.userSubscription = this.userService.user$.subscribe(function (user) {
            _this.isLogged = user ? true : false;
            _this.user = user;
        });
    }
    DcpsDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getDcpsContent();
        this.onCheckLong();
        //this.OnTorrentCreate();
        //this.OnDateCheck();
        this.interval = setInterval(function () {
            _this.getDcpsContent();
        }, 5000);
    }; //Fin ngOnInit
    DcpsDetailComponent.prototype.getDcpsContent = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.dcpService.getDcp(id)
            .subscribe(function (dcp) { return (_this.dcp = dcp); }, function (error) { return (_this.error = error); });
    }; //Fin getDcpsContent
    DcpsDetailComponent.prototype.goBack = function () {
        this.location.back();
    }; //Fin goBack
    DcpsDetailComponent.prototype.onCheckShort = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.dcpService.getShortCheckStatus(id)
            .subscribe(function (dcp) { return (_this.dcp = dcp); }, function (error) { return (_this.error = error); });
    };
    DcpsDetailComponent.prototype.onCheckLong = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.dcpService.getLongCheckStatus(id)
            .subscribe(function (dcp) { return (_this.dcp = dcp); }, function (error) { return (_this.error = error); });
    };
    DcpsDetailComponent.prototype.OnTorrentCreate = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.dcpService.getTheTorrent(id)
            .subscribe(function (dcp) { return (_this.dcp = dcp); }, function (error) { return (_this.error = error); });
    };
    DcpsDetailComponent.prototype.OnDcpIngest = function () {
        //Not implemented
    };
    DcpsDetailComponent.prototype.getRole = function (user) {
        if (!user || !user.ID) {
            console.debug('No user');
            return;
        }
        return ROLE[user.role];
    };
    __decorate([
        Input(),
        __metadata("design:type", Dcp)
    ], DcpsDetailComponent.prototype, "dcp", void 0);
    __decorate([
        ViewChild(MatPaginator),
        __metadata("design:type", MatPaginator)
    ], DcpsDetailComponent.prototype, "paginator", void 0);
    DcpsDetailComponent = __decorate([
        Component({
            selector: 'app-dcps-detail',
            templateUrl: './dcps-detail.component.html',
            styleUrls: ['./dcps-detail.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute,
            Location,
            UserService,
            DcpsService])
    ], DcpsDetailComponent);
    return DcpsDetailComponent;
}()); //Fin
export { DcpsDetailComponent };
//# sourceMappingURL=dcps-detail.component.js.map