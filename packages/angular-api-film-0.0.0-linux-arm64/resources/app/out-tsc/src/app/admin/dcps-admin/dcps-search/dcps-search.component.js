var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { DcpsService } from '../dcps.service';
var DcpsSearchComponent = /** @class */ (function () {
    function DcpsSearchComponent(dcps_Service) {
        this.dcps_Service = dcps_Service;
        this.searchTerms = new Subject();
    }
    DcpsSearchComponent.prototype.ngOnInit = function () {
        /*  this.dcps$ = this.searchTerms.pipe(
          debounceTime(300),
          distinctUntilChanged(),
          switchMap((term: string) =>this.dcps_Service.searchDcps(term)));
        }//fin ngOnInit
      
        search(term : string): void{
          this.searchTerms.next(term);
        }//Fin search
      */
    }; //Fin
    DcpsSearchComponent = __decorate([
        Component({
            selector: 'app-dcps-search',
            templateUrl: './dcps-search.component.html',
            styleUrls: ['./dcps-search.component.css']
        }),
        __metadata("design:paramtypes", [DcpsService])
    ], DcpsSearchComponent);
    return DcpsSearchComponent;
}());
export { DcpsSearchComponent };
//# sourceMappingURL=dcps-search.component.js.map