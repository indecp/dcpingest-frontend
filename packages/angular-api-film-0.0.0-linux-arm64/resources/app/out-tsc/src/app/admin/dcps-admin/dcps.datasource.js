import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { catchError, finalize } from "rxjs/operators";
import { of } from "rxjs/observable/of";
var DcpsDataSource = /** @class */ (function () {
    function DcpsDataSource(dcpsService) {
        this.dcpsService = dcpsService;
        this.dcpsSubject = new BehaviorSubject([]);
        this.dcpsTotalSubject = new BehaviorSubject(0);
        this.dcpsTotal = this.dcpsTotalSubject.asObservable();
        this.loadingSubject = new BehaviorSubject(false);
        this.loading$ = this.loadingSubject.asObservable();
    }
    DcpsDataSource.prototype.loadDcps = function (filter, sortDirection, pageIndex, pageSize) {
        var _this = this;
        this.loadingSubject.next(true);
        this.dcpsService.findDcps(filter, sortDirection, pageIndex, pageSize).pipe(catchError(function () { return of([]); }), finalize(function () { return _this.loadingSubject.next(false); }))
            .subscribe(function (dcps) {
            _this.dcpsSubject.next(dcps['payload']);
            _this.dcpsTotalSubject.next(dcps['total']);
        });
    };
    DcpsDataSource.prototype.connect = function (collectionViewer) {
        console.log("Connecting data source dcps");
        return this.dcpsSubject.asObservable();
    };
    DcpsDataSource.prototype.disconnect = function (collectionViewer) {
        this.dcpsSubject.complete();
        this.dcpsTotalSubject.complete();
        this.loadingSubject.complete();
    };
    return DcpsDataSource;
}());
export { DcpsDataSource };
//# sourceMappingURL=dcps.datasource.js.map