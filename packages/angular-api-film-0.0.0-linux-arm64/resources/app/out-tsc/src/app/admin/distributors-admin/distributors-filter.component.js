var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Pipe } from '@angular/core';
var DistributorsFilterPipe = /** @class */ (function () {
    function DistributorsFilterPipe() {
    }
    DistributorsFilterPipe.prototype.transform = function (items, searchTerm) {
        if (!items || !searchTerm) {
            return items;
        }
        // filter items array, items which match and return true will be
        // kept, false will be filtered out
        return items.filter(function (distrib) {
            return distrib.name.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1;
        });
    };
    DistributorsFilterPipe = __decorate([
        Pipe({
            name: 'DistribFilter'
        })
    ], DistributorsFilterPipe);
    return DistributorsFilterPipe;
}());
export { DistributorsFilterPipe };
//# sourceMappingURL=distributors-filter.component.js.map