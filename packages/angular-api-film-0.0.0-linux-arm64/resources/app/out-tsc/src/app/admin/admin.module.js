var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin/admin.component';
import { MoviesComponent } from './movies-admin/movie_list/movie_list.component';
import { MatCardModule, MatTabsModule, MatPaginatorModule, MatSortModule, MatTableModule, MatMenuModule, MatFormFieldModule, MatToolbarModule, MatButtonModule, MatInputModule, } from '@angular/material';
import { AdminHomeComponent } from './admin-home/admin-home.component';
var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }
    AdminModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                AdminRoutingModule,
                MatCardModule,
                MatTabsModule,
                MatPaginatorModule,
                MatSortModule,
                MatTableModule,
                MatMenuModule,
                MatFormFieldModule,
                MatToolbarModule,
                MatButtonModule,
                MatInputModule
            ],
            declarations: [
                AdminComponent, MoviesComponent, AdminHomeComponent
            ]
        })
    ], AdminModule);
    return AdminModule;
}());
export { AdminModule };
//# sourceMappingURL=admin.module.js.map