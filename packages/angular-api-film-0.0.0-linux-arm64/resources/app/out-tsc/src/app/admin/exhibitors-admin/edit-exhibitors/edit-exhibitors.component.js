var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ExhibitorsService } from '../exhibitors.service';
import { Location } from '@angular/common';
import { ROLE } from '../../../user/user.model';
import { UserService } from '../../../user/user.service';
var EditExhibitorsComponent = /** @class */ (function () {
    function EditExhibitorsComponent(formBuilder, exhibitorsService, userService, http, router, location, route) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.exhibitorsService = exhibitorsService;
        this.userService = userService;
        this.http = http;
        this.router = router;
        this.location = location;
        this.route = route;
        this.isLogged = false;
        this.exhibitorsForm = this.createFromGroup();
        this.userSubscription = this.userService.user$.subscribe(function (user) {
            _this.isLogged = user ? true : false;
            _this.user = user;
        });
    }
    EditExhibitorsComponent.prototype.createFromGroup = function () {
        return new FormGroup({
            id: new FormControl(),
            name: new FormControl(),
            CNC_Code_autorised: new FormControl(),
            log: new FormControl(),
            pass: new FormControl(),
            mail: new FormControl(),
            name_contact: new FormControl(),
            number_contact: new FormControl(),
            address_contact: new FormControl(),
            ZIP_Code: new FormControl(),
            town: new FormControl(),
            department: new FormControl(),
            Nb_screen: new FormControl(),
            ip_tinc: new FormControl(),
            free_space: new FormControl()
        });
    };
    ; //Fin createFromGroup
    EditExhibitorsComponent.prototype.goBack = function () {
        this.location.back();
    };
    EditExhibitorsComponent.prototype.onSubmit = function () {
        var _this = this;
        this.exhibitorsService
            .updateExhibitors(this.exhibitorsForm.value)
            .subscribe(function () { return _this.goBack(); });
    };
    EditExhibitorsComponent.prototype.ngOnInit = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.exhibitorsService.getExhibitorsContent(id)
            .subscribe(function (exhib) {
            _this.exhibitorsForm.patchValue(exhib);
        });
    };
    EditExhibitorsComponent.prototype.getRole = function (user) {
        if (!user || !user.ID) {
            console.debug('No user');
            return;
        }
        return ROLE[user.role];
    };
    EditExhibitorsComponent = __decorate([
        Component({
            selector: 'app-edit-exhibitors',
            templateUrl: './edit-exhibitors.component.html',
            styleUrls: ['./edit-exhibitors.component.css']
        }),
        __metadata("design:paramtypes", [FormBuilder,
            ExhibitorsService,
            UserService,
            HttpClient,
            Router,
            Location,
            ActivatedRoute])
    ], EditExhibitorsComponent);
    return EditExhibitorsComponent;
}());
export { EditExhibitorsComponent };
//# sourceMappingURL=edit-exhibitors.component.js.map