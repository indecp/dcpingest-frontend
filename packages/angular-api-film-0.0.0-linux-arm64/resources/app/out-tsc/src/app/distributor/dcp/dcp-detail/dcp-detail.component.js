var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DcpService } from 'app/_services';
import { MessageService } from 'app/message.service';
var DcpDetailComponent = /** @class */ (function () {
    function DcpDetailComponent(route, router, formBuilder, dcpService, dialogServicse) {
        this.route = route;
        this.router = router;
        this.formBuilder = formBuilder;
        this.dcpService = dcpService;
        this.dialogServicse = dialogServicse;
    }
    DcpDetailComponent.prototype.ngOnInit = function () {
        // TODO: verify in angulatr routing doc
        // why the this.route.data is needed.
        var _this = this;
        this.route.data
            .subscribe(function (data) {
            _this.editName = data.dcp.name;
            _this.dcp = data.dcp;
            _this.createFromGroup();
        });
    };
    DcpDetailComponent.prototype.createFromGroup = function () {
        this.dcpForm = this.formBuilder.group({
            'id': this.formBuilder.control(this.dcp.id),
            'name': this.formBuilder.control(this.dcp.name),
            'contentkind': this.formBuilder.control(this.dcp.contentkind),
            'size': this.formBuilder.control(this.dcp.size),
        });
    };
    DcpDetailComponent.prototype.handleSubmit = function () {
        console.log(this.dcpForm.value);
    };
    DcpDetailComponent.prototype.cancel = function () {
        this.gotoDcps();
    };
    DcpDetailComponent.prototype.save = function () {
        var _this = this;
        this.dcpService
            .updateDcp(this.dcpForm.value)
            .subscribe(function () { return _this.gotoDcps(); });
    };
    DcpDetailComponent.prototype.canDeactivate = function () {
        // Allow synchronous navigation (`true`) if no dcp or the dcp is unchanged
        if (!this.dcp || this.dcp.name === this.editName) {
            return true;
        }
        // Otherwise ask the user with the dialog service and return its
        // observable which resolves to true or false when the user decides
        //return this.dialogService.add('Discard changes?');
        return false;
    };
    DcpDetailComponent.prototype.gotoDcps = function () {
        var dcpId = this.dcp ? this.dcp.id : null;
        // Pass along the dcp id if available
        // so that the CrisisListComponent can select that dcp.
        // Add a totally useless `foo` parameter for kicks.
        // Relative navigation back to the  dcps
        this.router.navigate(['../', { id: dcpId }], { relativeTo: this.route });
    };
    DcpDetailComponent = __decorate([
        Component({
            selector: 'app-dcp-detail',
            templateUrl: './dcp-detail.component.html',
            styleUrls: ['./dcp-detail.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute,
            Router,
            FormBuilder,
            DcpService,
            MessageService])
    ], DcpDetailComponent);
    return DcpDetailComponent;
}());
export { DcpDetailComponent };
//# sourceMappingURL=dcp-detail.component.js.map