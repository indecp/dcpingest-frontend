var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'app/auth/auth.service';
import { slideInAnimation } from 'app/animations';
var DistributorComponent = /** @class */ (function () {
    function DistributorComponent(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    DistributorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.currentUser.subscribe(function (user) { return _this.currentUser = user; });
    };
    DistributorComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['/login']);
    };
    DistributorComponent.prototype.getAnimationData = function (outlet) {
        return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
    };
    DistributorComponent = __decorate([
        Component({
            selector: 'app-distributor',
            templateUrl: './distributor.component.html',
            styleUrls: ['./distributor.component.css'],
            animations: [slideInAnimation]
        }),
        __metadata("design:paramtypes", [AuthenticationService,
            Router])
    ], DistributorComponent);
    return DistributorComponent;
}());
export { DistributorComponent };
//# sourceMappingURL=distributor.component.js.map