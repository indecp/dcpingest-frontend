import { async, TestBed } from '@angular/core/testing';
import { MoviesViewDistributorComponent } from './movies-view-distributor.component';
describe('MoviesViewDistributorComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [MoviesViewDistributorComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(MoviesViewDistributorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=movies-view-distributor.component.spec.js.map