var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { switchMap } from 'rxjs/operators';
import { DistributorsService } from 'app/_services';
import { AuthenticationService } from 'app/auth/auth.service';
var DcpListComponent = /** @class */ (function () {
    function DcpListComponent(distributorsService, route, authService) {
        var _this = this;
        this.distributorsService = distributorsService;
        this.route = route;
        this.authService = authService;
        this.displayedColumns = ['id', 'name', 'contentkind', 'size'];
        this.dataSource = new MatTableDataSource();
        this.doFilter = function (value) {
            _this.dataSource.filter = value.trim().toLocaleLowerCase();
        };
    }
    DcpListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.currentUser.subscribe(function (user) { return _this.currentUser = user; });
        this.distributor = this.currentUser.distributor;
        this.dcpMap$ = this.route.paramMap.pipe(switchMap(function (params) {
            _this.selectedId = +params.get('id');
            return _this.distributorsService.getDcps(_this.currentUser.distributor.id);
        }));
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.getDcpsDistributors();
    };
    DcpListComponent.prototype.getDcpsDistributors = function () {
        var _this = this;
        this.distributorsService.getDcps(this.distributor.id)
            .subscribe(function (dcp) { return (_this.dataSource.data = dcp); });
    };
    __decorate([
        ViewChild(MatPaginator),
        __metadata("design:type", MatPaginator)
    ], DcpListComponent.prototype, "paginator", void 0);
    __decorate([
        ViewChild(MatSort),
        __metadata("design:type", MatSort)
    ], DcpListComponent.prototype, "sort", void 0);
    DcpListComponent = __decorate([
        Component({
            selector: 'dcp-list',
            templateUrl: './dcp-list.component.html',
            styleUrls: ['./dcp-list.component.css']
        }),
        __metadata("design:paramtypes", [DistributorsService,
            ActivatedRoute,
            AuthenticationService])
    ], DcpListComponent);
    return DcpListComponent;
}());
export { DcpListComponent };
//# sourceMappingURL=dcp-list.component.js.map