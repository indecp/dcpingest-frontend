var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { switchMap } from 'rxjs/operators';
import { DistributorsService } from 'app/_services';
import { Distributor } from 'app/_models';
import { AuthenticationService } from 'app/auth/auth.service';
var MovieListComponent = /** @class */ (function () {
    function MovieListComponent(distributorsService, route, authService) {
        var _this = this;
        this.distributorsService = distributorsService;
        this.route = route;
        this.authService = authService;
        this.displayedColumns = ['affiche', 'id', 'title', 'releasedate', 'FTR', 'TLR', 'action'];
        this.dataSource = new MatTableDataSource();
        this.doFilter = function (value) {
            _this.dataSource.filter = value.trim().toLocaleLowerCase();
        };
    }
    MovieListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.currentUser.subscribe(function (user) { return _this.currentUser = user; });
        this.distributor = this.currentUser.distributor;
        this.movieMap$ = this.route.paramMap.pipe(switchMap(function (params) {
            _this.selectedId = +params.get('id');
            return _this.distributorsService.getMovies(_this.currentUser.distributor.id);
        }));
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.getMoviesDistributors();
    };
    MovieListComponent.prototype.getMoviesDistributors = function () {
        var _this = this;
        this.distributorsService.getMovies(this.distributor.id)
            .subscribe(function (movie) { return (_this.dataSource.data = movie); });
    };
    __decorate([
        Input(),
        __metadata("design:type", Distributor)
    ], MovieListComponent.prototype, "distributor", void 0);
    __decorate([
        ViewChild(MatPaginator),
        __metadata("design:type", MatPaginator)
    ], MovieListComponent.prototype, "paginator", void 0);
    __decorate([
        ViewChild(MatSort),
        __metadata("design:type", MatSort)
    ], MovieListComponent.prototype, "sort", void 0);
    MovieListComponent = __decorate([
        Component({
            selector: 'movie-list',
            templateUrl: './movie-list.component.html',
            styleUrls: ['./movie-list.component.css']
        }),
        __metadata("design:paramtypes", [DistributorsService,
            ActivatedRoute,
            AuthenticationService])
    ], MovieListComponent);
    return MovieListComponent;
}());
export { MovieListComponent };
//# sourceMappingURL=movie-list.component.js.map