var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { MovieService } from 'app/_services';
import { MessageService } from 'app/message.service';
var MovieDetailComponent = /** @class */ (function () {
    function MovieDetailComponent(route, router, formBuilder, movieService, dialogServicse) {
        this.route = route;
        this.router = router;
        this.formBuilder = formBuilder;
        this.movieService = movieService;
        this.dialogServicse = dialogServicse;
    }
    MovieDetailComponent.prototype.ngOnInit = function () {
        // TODO: verify in angulatr routing doc
        // why the this.route.data is needed.
        var _this = this;
        this.route.data
            .subscribe(function (data) {
            _this.editName = data.movie.title;
            _this.movie = data.movie;
        });
        this.createFromGroup();
    };
    MovieDetailComponent.prototype.createFromGroup = function () {
        this.movieForm = this.formBuilder.group({
            'id': this.formBuilder.control(this.movie.id),
            'title': this.formBuilder.control(this.movie.title),
            'releasedate': this.formBuilder.control(this.movie.releasedate),
            'director': this.formBuilder.control(this.movie.director),
            'scenario': this.formBuilder.control(this.movie.scenario),
            'actor': this.formBuilder.control(this.movie.actor),
            'country': this.formBuilder.control(this.movie.country),
            'synopsis': this.formBuilder.control(this.movie.synopsis)
        });
    };
    MovieDetailComponent.prototype.handleSubmit = function () {
        console.log(this.movieForm.value);
    };
    MovieDetailComponent.prototype.cancel = function () {
        this.gotoMovies();
    };
    MovieDetailComponent.prototype.save = function () {
        var _this = this;
        this.movieService
            .updateMovie(this.movieForm.value)
            .subscribe(function () { return _this.gotoMovies(); });
    };
    MovieDetailComponent.prototype.canDeactivate = function () {
        // Allow synchronous navigation (`true`) if no movie or the movie is unchanged
        if (!this.movie || this.movie.title === this.editName) {
            return true;
        }
        // Otherwise ask the user with the dialog service and return its
        // observable which resolves to true or false when the user decides
        //return this.dialogService.add('Discard changes?');
        return false;
    };
    MovieDetailComponent.prototype.gotoMovies = function () {
        var movieId = this.movie ? this.movie.id : null;
        // Pass along the movie id if available
        // so that the CrisisListComponent can select that movie.
        // Add a totally useless `foo` parameter for kicks.
        // Relative navigation back to the  movies
        this.router.navigate(['../', { id: movieId }], { relativeTo: this.route });
    };
    MovieDetailComponent = __decorate([
        Component({
            selector: 'app-movie-detail',
            templateUrl: './movie-detail.component.html',
            styleUrls: ['./movie-detail.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute,
            Router,
            FormBuilder,
            MovieService,
            MessageService])
    ], MovieDetailComponent);
    return MovieDetailComponent;
}());
export { MovieDetailComponent };
//# sourceMappingURL=movie-detail.component.js.map