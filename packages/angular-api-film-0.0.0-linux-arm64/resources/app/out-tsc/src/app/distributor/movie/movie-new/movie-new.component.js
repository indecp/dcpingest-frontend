var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { MovieService } from 'app/_services';
import { MessageService } from 'app/message.service';
var MovieNewComponent = /** @class */ (function () {
    function MovieNewComponent(route, router, formBuilder, movieService, dialogServicse) {
        this.route = route;
        this.router = router;
        this.formBuilder = formBuilder;
        this.movieService = movieService;
        this.dialogServicse = dialogServicse;
    }
    MovieNewComponent.prototype.ngOnInit = function () {
        this.createFromGroup();
    };
    MovieNewComponent.prototype.createFromGroup = function () {
        this.movieForm = this.formBuilder.group({
            //'id': this.formBuilder.control(),
            'title': this.formBuilder.control(''),
            'releasedate': this.formBuilder.control(''),
            'director': this.formBuilder.control(''),
            'scenario': this.formBuilder.control(''),
            'actor': this.formBuilder.control(''),
            'country': this.formBuilder.control(''),
            'synopsis': this.formBuilder.control('')
        });
    };
    MovieNewComponent.prototype.handleSubmit = function () {
        console.log(this.movieForm.value);
    };
    MovieNewComponent.prototype.cancel = function () {
        this.gotoMovies();
    };
    MovieNewComponent.prototype.save = function () {
        var _this = this;
        this.movieService
            .addMovie(this.movieForm.value)
            .subscribe(function () { return _this.gotoMovies(); });
    };
    MovieNewComponent.prototype.gotoMovies = function () {
        // Pass along the movie id if available
        // so that the CrisisListComponent can select that movie.
        // Add a totally useless `foo` parameter for kicks.
        // Relative navigation back to the  movies
        this.router.navigate(['../'], { relativeTo: this.route });
    };
    MovieNewComponent = __decorate([
        Component({
            selector: 'app-movie-new',
            templateUrl: './movie-new.component.html',
            styleUrls: ['./movie-new.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute,
            Router,
            FormBuilder,
            MovieService,
            MessageService])
    ], MovieNewComponent);
    return MovieNewComponent;
}());
export { MovieNewComponent };
//# sourceMappingURL=movie-new.component.js.map