var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { of, EMPTY } from 'rxjs';
import { mergeMap, take } from 'rxjs/operators';
import { DcpService } from 'app/_services';
var DcpDetailResolverService = /** @class */ (function () {
    function DcpDetailResolverService(dcpService, router) {
        this.dcpService = dcpService;
        this.router = router;
    }
    DcpDetailResolverService.prototype.resolve = function (route, state) {
        var _this = this;
        var id = +route.paramMap.get('id');
        return this.dcpService.getDcp(id).pipe(take(1), mergeMap(function (dcp) {
            if (dcp) {
                return of(dcp);
            }
            else { // id not found
                _this.router.navigate(['/distributor/dcp']);
                return EMPTY;
            }
        }));
    };
    DcpDetailResolverService = __decorate([
        Injectable({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [DcpService,
            Router])
    ], DcpDetailResolverService);
    return DcpDetailResolverService;
}());
export { DcpDetailResolverService };
//# sourceMappingURL=dcp-detail-resolver.service.js.map