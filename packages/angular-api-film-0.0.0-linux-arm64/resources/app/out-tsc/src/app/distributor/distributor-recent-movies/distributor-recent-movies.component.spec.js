import { async, TestBed } from '@angular/core/testing';
import { DistributorRecentMoviesComponent } from './distributor-recent-movies.component';
describe('DistributorRecentMoviesComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [DistributorRecentMoviesComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(DistributorRecentMoviesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=distributor-recent-movies.component.spec.js.map