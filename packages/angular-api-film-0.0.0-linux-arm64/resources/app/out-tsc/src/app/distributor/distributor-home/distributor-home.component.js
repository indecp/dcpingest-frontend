var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { User } from 'app/_models';
import { AuthenticationService } from 'app/auth/auth.service';
var DistributorHomeComponent = /** @class */ (function () {
    function DistributorHomeComponent(route, authService) {
        this.route = route;
        this.authService = authService;
    }
    DistributorHomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Capture the session ID if available
        this.authService.currentUser.subscribe(function (user) { return _this.currentUser = user; });
        this.sessionId = this.route
            .queryParamMap
            .pipe(map(function (params) { return params.get('session_id') || 'None'; }));
        // Capture the fragment if available
        this.token = this.route
            .fragment
            .pipe(map(function (fragment) { return fragment || 'None'; }));
    };
    __decorate([
        Input(),
        __metadata("design:type", User)
    ], DistributorHomeComponent.prototype, "currentUser", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Number)
    ], DistributorHomeComponent.prototype, "distributorid", void 0);
    DistributorHomeComponent = __decorate([
        Component({
            selector: 'app-distributor-home',
            templateUrl: './distributor-home.component.html',
            styleUrls: ['./distributor-home.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute,
            AuthenticationService])
    ], DistributorHomeComponent);
    return DistributorHomeComponent;
}());
export { DistributorHomeComponent };
//# sourceMappingURL=distributor-home.component.js.map