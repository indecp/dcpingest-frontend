var DISTRIBUTORS = /** @class */ (function () {
    function DISTRIBUTORS() {
        this.distributorname = '';
        this.partners = '';
        this.mail = '';
        this.user_contact = '';
        this.number_contact = '';
        this.user_Id = '';
        this.mail_as_user_id = '';
    }
    return DISTRIBUTORS;
}());
export { DISTRIBUTORS };
//# sourceMappingURL=distributors.js.map