var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { DistributorsService } from '../distributors.service';
var DistributorsSearchComponent = /** @class */ (function () {
    function DistributorsSearchComponent(distrib_Service) {
        this.distrib_Service = distrib_Service;
        this.searchTerms = new Subject();
    }
    DistributorsSearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.distrib$ = this.searchTerms.pipe(debounceTime(300), distinctUntilChanged(), switchMap(function (term) { return _this.distrib_Service.searchDistributors(term); }));
    }; //fin ngOnInit
    DistributorsSearchComponent.prototype.search = function (term) {
        this.searchTerms.next(term);
    }; //Fin search
    DistributorsSearchComponent = __decorate([
        Component({
            selector: 'app-distributors-search',
            templateUrl: './distributors-search.component.html',
            styleUrls: ['./distributors-search.component.css']
        }),
        __metadata("design:paramtypes", [DistributorsService])
    ], DistributorsSearchComponent);
    return DistributorsSearchComponent;
}());
export { DistributorsSearchComponent };
//# sourceMappingURL=distributors-search.component.js.map