var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DistributorsService } from '../distributors.service';
import { DISTRIBUTORS } from '../distributors';
var DistributorsNewComponent = /** @class */ (function () {
    function DistributorsNewComponent(formBuilder, distrib_Service, http) {
        this.formBuilder = formBuilder;
        this.distrib_Service = distrib_Service;
        this.http = http;
        this.Partenaire = ['OUI', 'NON'];
        this.dataSource = new FormData();
        this.signDistrib = this.createFromGroup();
    }
    DistributorsNewComponent.prototype.createFromGroup = function () {
        return new FormGroup({
            distributorname: new FormControl(),
            user_contact: new FormControl(),
            partners: new FormControl(),
            number_contact: new FormControl(),
            mail: new FormControl(),
            CNC_Code: new FormControl()
        });
    };
    ; //Fin createFromGroup
    DistributorsNewComponent.prototype.createFormGroupWithBuilderAndModel = function (formBuilder) {
        return formBuilder.group({
            DataDistrib: formBuilder.group(new DISTRIBUTORS()),
        });
    }; //FIN   createFormGroupWithBuilderAndModel
    DistributorsNewComponent.prototype.ngOnInit = function () { };
    DistributorsNewComponent.prototype.onSubmit = function () {
        var _this = this;
        this.distrib_Service.addDistributors(this.signDistrib.value)
            .subscribe({ complete: function () { return _this.dataSource; } });
    };
    DistributorsNewComponent = __decorate([
        Component({
            selector: 'app-distributors-new',
            templateUrl: './distributors-new.component.html',
            styleUrls: ['./distributors-new.component.css']
        }),
        __metadata("design:paramtypes", [FormBuilder,
            DistributorsService,
            HttpClient])
    ], DistributorsNewComponent);
    return DistributorsNewComponent;
}()); //Fin
export { DistributorsNewComponent };
//# sourceMappingURL=distributors-new.component.js.map