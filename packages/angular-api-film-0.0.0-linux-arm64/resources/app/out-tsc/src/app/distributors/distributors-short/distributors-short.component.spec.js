import { async, TestBed } from '@angular/core/testing';
import { DistributorsShortComponent } from './distributors-short.component';
describe('DistributorsShortComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [DistributorsShortComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(DistributorsShortComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=distributors-short.component.spec.js.map