var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
var httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
var MoviesService = /** @class */ (function () {
    function MoviesService(http, message_Service) {
        this.http = http;
        this.message_Service = message_Service;
        this.MoviesUrl = 'api/movies';
    }
    MoviesService.prototype.getMovies = function () {
        var _this = this;
        return this.http.get(this.MoviesUrl)
            .pipe(tap(function (movies) { return _this.log('fetched movies'); }), catchError(this.handleError('getMovies', [])));
    };
    MoviesService.prototype.getFilm = function (id) {
        var _this = this;
        var url = this.MoviesUrl + "/" + id;
        return this.http.get(url).pipe(tap(function (_) { return _this.log("fetched movie_s id=" + id); }), catchError(this.handleError("getMovies id=" + id)));
    }; //Fin getFilm
    MoviesService.prototype.searchMovies = function (term) {
        var _this = this;
        if (!term.trim()) {
            return of([]);
        }
        return this.http.get(this.MoviesUrl + "/?name=" + term)
            .pipe(tap(function (_) { return _this.log("found movies matching \"$(term)\""); }), catchError(this.handleError('searchMovies', [])));
    };
    MoviesService.prototype.addMovie = function (movie_s) {
        var _this = this;
        return this.http.post(this.MoviesUrl, movie_s, httpOptions)
            .pipe(tap(function (movie_s) { return _this.log("added movie_s w/ id=" + movie_s.id); }), catchError(this.handleError('addMovie')));
    };
    MoviesService.prototype.deleteMovie = function (movie_s) {
        var _this = this;
        var id = typeof movie_s === 'number' ? movie_s : movie_s.id;
        var url = this.MoviesUrl + "/" + id;
        return this.http.delete(url, httpOptions)
            .pipe(tap(function (_) { return _this.log("delete movie_s id=" + id); }), catchError(this.handleError('deleteMovie')));
    };
    MoviesService.prototype.updateMovies = function (movie_s) {
        var _this = this;
        return this.http.put(this.MoviesUrl, movie_s, httpOptions)
            .pipe(tap(function (_) { return _this.log("updated movie_s id=" + movie_s.id); }), catchError(this.handleError('updateMovies')));
    };
    MoviesService.prototype.handleError = function (operation, result) {
        var _this = this;
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            console.error(error);
            _this.log(operation + " failed: " + error.message);
            return of(result);
        }; //fin handleError
    };
    MoviesService.prototype.log = function (message) {
        this.message_Service.add('MoviesService: ${message}');
    };
    MoviesService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [HttpClient,
            MessageService])
    ], MoviesService);
    return MoviesService;
}());
export { MoviesService };
//# sourceMappingURL=movies.service.js.map