import { Subject } from 'rxjs/Subject';
var AddMovieComponent = /** @class */ (function () {
    function AddMovieComponent() {
        this.MovieSubject = new Subject();
    }
    AddMovieComponent.prototype.emitMovie = function () {
        this.MovieSubject.next(this.films.slice());
    };
    AddMovieComponent.prototype.addFilm = function (film) {
        this.films.push(film);
        this.emitMovie();
    };
    return AddMovieComponent;
}());
export { AddMovieComponent };
/*  get diagnostic() { return JSON.stringify(this.new_movie); }
}
*/
/*
export class AddMovieComponent implements OnInit {

    movie : Movie[];
    synop : Movie[];

  constructor(private movies_Service: MoviesService) { }

  ngOnInit() {
    this.getMovies();
  }

  getMovies():void {

    this.movies_Service.getMovies()
    .subscribe(movie => this.movie = movie);
  }

  add(name:string):void{

    name = name.trim();
    if(!name) { return; }
    this.movies_Service.addMovie({name} as Movie)
    .subscribe(movies => {this.movie.push(movies);
    });
  }

  
  
 }*/
//# sourceMappingURL=add-movie.component.js.map