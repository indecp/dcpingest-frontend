import { async, TestBed } from '@angular/core/testing';
import { ExhibitorsSearchComponent } from './exhibitors-search.component';
describe('ExhibitorsSearchComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [ExhibitorsSearchComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(ExhibitorsSearchComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=exhibitors-search.component.spec.js.map