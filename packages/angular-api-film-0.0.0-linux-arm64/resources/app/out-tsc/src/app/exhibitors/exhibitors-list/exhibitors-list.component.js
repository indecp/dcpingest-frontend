var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ExhibitorsService } from '../exhibitors.service';
var ExhibitorsListComponent = /** @class */ (function () {
    function ExhibitorsListComponent(exhibitors_service) {
        this.exhibitors_service = exhibitors_service;
        this.displayedColumns = ['id', 'name', 'department', 'CNC_Code_autorised', 'ip_tinc', 'Nb_screen', 'free_space', 'delete'];
        this.dataSource = new MatTableDataSource();
    }
    ExhibitorsListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.exhibitors_service
            .getExhibitors()
            .subscribe(function (movies) { return (_this.dataSource.data = movies); }, function (error) { return (_this.error = error); });
        this.dataSource.paginator = this.paginator;
    }; //Fin ngOnInit
    __decorate([
        ViewChild(MatPaginator),
        __metadata("design:type", MatPaginator)
    ], ExhibitorsListComponent.prototype, "paginator", void 0);
    __decorate([
        ViewChild(MatSort),
        __metadata("design:type", MatSort)
    ], ExhibitorsListComponent.prototype, "sort", void 0);
    ExhibitorsListComponent = __decorate([
        Component({
            selector: 'app-exhibitors-list',
            templateUrl: './exhibitors-list.component.html',
            styleUrls: ['./exhibitors-list.component.css']
        }),
        __metadata("design:paramtypes", [ExhibitorsService])
    ], ExhibitorsListComponent);
    return ExhibitorsListComponent;
}()); //Fin 
export { ExhibitorsListComponent };
//# sourceMappingURL=exhibitors-list.component.js.map