var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Exhibitors } from '../exhibitors';
import { ExhibitorsService } from '../exhibitors.service';
var ExhibitorsNewComponent = /** @class */ (function () {
    function ExhibitorsNewComponent(formBuilder, exhibitors_Service, http) {
        this.formBuilder = formBuilder;
        this.exhibitors_Service = exhibitors_Service;
        this.http = http;
        this.dataSource = new FormData();
        this.signExhibitors = this.createFromGroup();
    }
    ExhibitorsNewComponent.prototype.createFromGroup = function () {
        return new FormGroup({
            name: new FormControl(),
            CNC_Code_autorised: new FormControl(),
            log: new FormControl(),
            pass: new FormControl(),
            mail: new FormControl(),
            name_contact: new FormControl(),
            number_contact: new FormControl(),
            address_contact: new FormControl(),
            ZIP_Code: new FormControl(),
            town: new FormControl(),
            department: new FormControl(),
            Nb_screen: new FormControl(),
            ip_tinc: new FormControl(),
            free_space: new FormControl()
        });
    };
    ; //Fin createFromGroup
    ExhibitorsNewComponent.prototype.createFormGroupWithBuilder = function (formBuilder) {
        return formBuilder.group({
            name: '',
            CNC_Code_autorised: '',
            log: '',
            pass: '',
            mail: '',
            name_contact: '',
            number_contact: '',
            address_contact: '',
            ZIP_Code: '',
            town: '',
            department: '',
            Nb_screen: '',
            ip_tinc: '',
            free_space: ''
        });
    };
    ; //FIN  createFormGroupWithBuilder
    ExhibitorsNewComponent.prototype.createFormGroupWithBuilderAndModel = function (formBuilder) {
        return formBuilder.group({
            DataExhibitors: formBuilder.group(new Exhibitors()),
        });
    }; //FIN  createFormGroupWithBuilderAndModel
    ExhibitorsNewComponent.prototype.onSubmit = function () {
        var _this = this;
        this.exhibitors_Service.addExhibitors(this.signExhibitors.value)
            .subscribe({ complete: function () { return _this.dataSource; } });
    };
    ExhibitorsNewComponent.prototype.ngOnInit = function () { };
    ExhibitorsNewComponent = __decorate([
        Component({
            selector: 'app-exhibitors-new',
            templateUrl: './exhibitors-new.component.html',
            styleUrls: ['./exhibitors-new.component.css']
        }),
        __metadata("design:paramtypes", [FormBuilder,
            ExhibitorsService,
            HttpClient])
    ], ExhibitorsNewComponent);
    return ExhibitorsNewComponent;
}());
export { ExhibitorsNewComponent };
//# sourceMappingURL=exhibitors-new.component.js.map