var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { ROLE } from 'app/_models';
import { UserService } from 'app/_services';
import { DISTRIBUTORS } from '../distributors';
import { DistributorsService } from '../distributors.service';
import { ActivatedRoute, Router } from '@angular/router';
var DistributorsDetailComponent = /** @class */ (function () {
    function DistributorsDetailComponent(route, Distrib_Service, location, userService, router) {
        var _this = this;
        this.route = route;
        this.Distrib_Service = Distrib_Service;
        this.location = location;
        this.userService = userService;
        this.router = router;
        this.displayedColumns = ['distribid', 'name', 'partners', 'mail', 'user_contact', 'number_contact', 'CNC_Code', 'user_Id', 'mail_as_user_id'];
        this.dataSource = new MatTableDataSource(this.distributors);
        this.isLogged = false;
        this.userSubscription = this.userService.user$.subscribe(function (user) {
            _this.isLogged = user ? true : false;
            _this.user = user;
        });
    }
    DistributorsDetailComponent.prototype.ngOnInit = function () {
        this.getDistributorsContent();
        this.dataSource.paginator = this.paginator;
    }; //Fin ngOnInit
    DistributorsDetailComponent.prototype.getDistributorsContent = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.Distrib_Service.getDistributorsContent(id)
            .subscribe(function (distributor) { return _this.distributor = distributor; });
    }; //Fin getDistributorsContent
    DistributorsDetailComponent.prototype.goBack = function () {
        this.location.back();
    }; //Fin goBack
    DistributorsDetailComponent.prototype.save = function () {
        var _this = this;
        this.Distrib_Service.updateDistributors(this.distributor)
            .subscribe(function () { return _this.goBack(); });
    }; //Fin save
    DistributorsDetailComponent.prototype.getRole = function (user) {
        if (!user || !user.ID) {
            console.debug('No user');
            return;
        }
        return ROLE[user.role];
    };
    __decorate([
        Input(),
        __metadata("design:type", DISTRIBUTORS)
    ], DistributorsDetailComponent.prototype, "distributor", void 0);
    __decorate([
        ViewChild(MatPaginator),
        __metadata("design:type", MatPaginator)
    ], DistributorsDetailComponent.prototype, "paginator", void 0);
    DistributorsDetailComponent = __decorate([
        Component({
            selector: 'app-distributors-detail',
            templateUrl: './distributors-detail.component.html',
            styleUrls: ['./distributors-detail.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute,
            DistributorsService,
            Location,
            UserService,
            Router])
    ], DistributorsDetailComponent);
    return DistributorsDetailComponent;
}()); //Fin
export { DistributorsDetailComponent };
//# sourceMappingURL=distributors-detail.component.js.map