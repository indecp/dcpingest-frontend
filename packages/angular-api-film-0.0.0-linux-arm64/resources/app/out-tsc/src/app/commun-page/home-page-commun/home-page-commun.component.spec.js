import { async, TestBed } from '@angular/core/testing';
import { HomePageCommunComponent } from './home-page-commun.component';
describe('HomePageCommunComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [HomePageCommunComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(HomePageCommunComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=home-page-commun.component.spec.js.map