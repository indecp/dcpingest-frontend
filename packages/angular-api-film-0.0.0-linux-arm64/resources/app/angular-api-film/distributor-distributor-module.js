(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["distributor-distributor-module"],{

/***/ "./node_modules/filesize/lib/filesize.js":
/*!***********************************************!*\
  !*** ./node_modules/filesize/lib/filesize.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * filesize
 *
 * @copyright 2018 Jason Mulligan <jason.mulligan@avoidwork.com>
 * @license BSD-3-Clause
 * @version 3.6.1
 */
(function (global) {
	var b = /^(b|B)$/,
	    symbol = {
		iec: {
			bits: ["b", "Kib", "Mib", "Gib", "Tib", "Pib", "Eib", "Zib", "Yib"],
			bytes: ["B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"]
		},
		jedec: {
			bits: ["b", "Kb", "Mb", "Gb", "Tb", "Pb", "Eb", "Zb", "Yb"],
			bytes: ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
		}
	},
	    fullform = {
		iec: ["", "kibi", "mebi", "gibi", "tebi", "pebi", "exbi", "zebi", "yobi"],
		jedec: ["", "kilo", "mega", "giga", "tera", "peta", "exa", "zetta", "yotta"]
	};

	/**
  * filesize
  *
  * @method filesize
  * @param  {Mixed}   arg        String, Int or Float to transform
  * @param  {Object}  descriptor [Optional] Flags
  * @return {String}             Readable file size String
  */
	function filesize(arg) {
		var descriptor = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

		var result = [],
		    val = 0,
		    e = void 0,
		    base = void 0,
		    bits = void 0,
		    ceil = void 0,
		    full = void 0,
		    fullforms = void 0,
		    neg = void 0,
		    num = void 0,
		    output = void 0,
		    round = void 0,
		    unix = void 0,
		    separator = void 0,
		    spacer = void 0,
		    standard = void 0,
		    symbols = void 0;

		if (isNaN(arg)) {
			throw new Error("Invalid arguments");
		}

		bits = descriptor.bits === true;
		unix = descriptor.unix === true;
		base = descriptor.base || 2;
		round = descriptor.round !== void 0 ? descriptor.round : unix ? 1 : 2;
		separator = descriptor.separator !== void 0 ? descriptor.separator || "" : "";
		spacer = descriptor.spacer !== void 0 ? descriptor.spacer : unix ? "" : " ";
		symbols = descriptor.symbols || descriptor.suffixes || {};
		standard = base === 2 ? descriptor.standard || "jedec" : "jedec";
		output = descriptor.output || "string";
		full = descriptor.fullform === true;
		fullforms = descriptor.fullforms instanceof Array ? descriptor.fullforms : [];
		e = descriptor.exponent !== void 0 ? descriptor.exponent : -1;
		num = Number(arg);
		neg = num < 0;
		ceil = base > 2 ? 1000 : 1024;

		// Flipping a negative number to determine the size
		if (neg) {
			num = -num;
		}

		// Determining the exponent
		if (e === -1 || isNaN(e)) {
			e = Math.floor(Math.log(num) / Math.log(ceil));

			if (e < 0) {
				e = 0;
			}
		}

		// Exceeding supported length, time to reduce & multiply
		if (e > 8) {
			e = 8;
		}

		// Zero is now a special case because bytes divide by 1
		if (num === 0) {
			result[0] = 0;
			result[1] = unix ? "" : symbol[standard][bits ? "bits" : "bytes"][e];
		} else {
			val = num / (base === 2 ? Math.pow(2, e * 10) : Math.pow(1000, e));

			if (bits) {
				val = val * 8;

				if (val >= ceil && e < 8) {
					val = val / ceil;
					e++;
				}
			}

			result[0] = Number(val.toFixed(e > 0 ? round : 0));
			result[1] = base === 10 && e === 1 ? bits ? "kb" : "kB" : symbol[standard][bits ? "bits" : "bytes"][e];

			if (unix) {
				result[1] = standard === "jedec" ? result[1].charAt(0) : e > 0 ? result[1].replace(/B$/, "") : result[1];

				if (b.test(result[1])) {
					result[0] = Math.floor(result[0]);
					result[1] = "";
				}
			}
		}

		// Decorating a 'diff'
		if (neg) {
			result[0] = -result[0];
		}

		// Applying custom symbol
		result[1] = symbols[result[1]] || result[1];

		// Returning Array, Object, or String (default)
		if (output === "array") {
			return result;
		}

		if (output === "exponent") {
			return e;
		}

		if (output === "object") {
			return { value: result[0], suffix: result[1], symbol: result[1] };
		}

		if (full) {
			result[1] = fullforms[e] ? fullforms[e] : fullform[standard][e] + (bits ? "bit" : "byte") + (result[0] === 1 ? "" : "s");
		}

		if (separator.length > 0) {
			result[0] = result[0].toString().replace(".", separator);
		}

		return result.join(spacer);
	}

	// Partial application for functional programming
	filesize.partial = function (opt) {
		return function (arg) {
			return filesize(arg, opt);
		};
	};

	// CommonJS, AMD, script tag
	if (true) {
		module.exports = filesize;
	} else {}
})(typeof window !== "undefined" ? window : global);


/***/ }),

/***/ "./node_modules/ngx-filesize/dist/filesize.module.js":
/*!***********************************************************!*\
  !*** ./node_modules/ngx-filesize/dist/filesize.module.js ***!
  \***********************************************************/
/*! exports provided: FileSizeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileSizeModule", function() { return FileSizeModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _filesize_pipe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./filesize.pipe */ "./node_modules/ngx-filesize/dist/filesize.pipe.js");


var FileSizeModule = /** @class */ (function () {
    function FileSizeModule() {
    }
    FileSizeModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    declarations: [
                        _filesize_pipe__WEBPACK_IMPORTED_MODULE_1__["FileSizePipe"]
                    ],
                    exports: [
                        _filesize_pipe__WEBPACK_IMPORTED_MODULE_1__["FileSizePipe"]
                    ]
                },] },
    ];
    /** @nocollapse */
    FileSizeModule.ctorParameters = function () { return []; };
    return FileSizeModule;
}());

//# sourceMappingURL=filesize.module.js.map

/***/ }),

/***/ "./node_modules/ngx-filesize/dist/filesize.pipe.js":
/*!*********************************************************!*\
  !*** ./node_modules/ngx-filesize/dist/filesize.pipe.js ***!
  \*********************************************************/
/*! exports provided: FileSizePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileSizePipe", function() { return FileSizePipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var filesize__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! filesize */ "./node_modules/filesize/lib/filesize.js");
/* harmony import */ var filesize__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(filesize__WEBPACK_IMPORTED_MODULE_1__);


var FileSizePipe = /** @class */ (function () {
    function FileSizePipe() {
    }
    FileSizePipe.prototype.transform = function (value, options) {
        if (Array.isArray(value)) {
            return value.map(function (val) { return FileSizePipe.transformOne(val, options); });
        }
        return FileSizePipe.transformOne(value, options);
    };
    FileSizePipe.transformOne = function (value, options) {
        return filesize__WEBPACK_IMPORTED_MODULE_1__(value, options);
    };
    FileSizePipe.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"], args: [{
                    name: 'filesize'
                },] },
    ];
    /** @nocollapse */
    FileSizePipe.ctorParameters = function () { return []; };
    return FileSizePipe;
}());

//# sourceMappingURL=filesize.pipe.js.map

/***/ }),

/***/ "./node_modules/ngx-filesize/dist/index.js":
/*!*************************************************!*\
  !*** ./node_modules/ngx-filesize/dist/index.js ***!
  \*************************************************/
/*! exports provided: FileSizeModule, FileSizePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _filesize_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./filesize.module */ "./node_modules/ngx-filesize/dist/filesize.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FileSizeModule", function() { return _filesize_module__WEBPACK_IMPORTED_MODULE_0__["FileSizeModule"]; });

/* harmony import */ var _filesize_pipe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./filesize.pipe */ "./node_modules/ngx-filesize/dist/filesize.pipe.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FileSizePipe", function() { return _filesize_pipe__WEBPACK_IMPORTED_MODULE_1__["FileSizePipe"]; });



//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/distributor/dcp/dcp-detail-resolver.service.ts":
/*!****************************************************************!*\
  !*** ./src/app/distributor/dcp/dcp-detail-resolver.service.ts ***!
  \****************************************************************/
/*! exports provided: DcpDetailResolverService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DcpDetailResolverService", function() { return DcpDetailResolverService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var app_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/_services */ "./src/app/_services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DcpDetailResolverService = /** @class */ (function () {
    function DcpDetailResolverService(dcpService, router) {
        this.dcpService = dcpService;
        this.router = router;
    }
    DcpDetailResolverService.prototype.resolve = function (route, state) {
        var _this = this;
        var id = +route.paramMap.get('id');
        return this.dcpService.getDcp(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["mergeMap"])(function (dcp) {
            if (dcp) {
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(dcp);
            }
            else { // id not found
                _this.router.navigate(['/distributor/dcp']);
                return rxjs__WEBPACK_IMPORTED_MODULE_2__["EMPTY"];
            }
        }));
    };
    DcpDetailResolverService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [app_services__WEBPACK_IMPORTED_MODULE_4__["DcpService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], DcpDetailResolverService);
    return DcpDetailResolverService;
}());



/***/ }),

/***/ "./src/app/distributor/dcp/dcp-detail/dcp-detail.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/distributor/dcp/dcp-detail/dcp-detail.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "label {\n  display: block;\n  width: 6em;\n  margin: .5em 0;\n\n}\n\ninput .inputText {\n  flex-basis: 66.6667%;\n  font-size: 1em;\n  padding-left: .4em;\n  margin-left: 10px;\n\n}\n\nmat-form-field {    \n    top: 10px;\n    width: 280px;\n    margin-bottom:  5px;\n}\n\nbutton {\n  font-family: Arial;\n  background-color: #eee;\n  border: none;\n  padding: 5px 10px;\n  border-radius: 4px;\n  cursor: pointer;\n}\n\ndiv .button {margin-left: 520px;}\n\nbutton:hover {\n  background-color: #cfd8dc;\n}\n\nbutton:disabled {\n  background-color: #eee;\n  color: #ccc;\n  cursor: auto;\n}\n\nlabel.Field{\n    text-align: right;\n    clear: both;\n    float: left;\n    margin-left: 500px;\n    margin-right: 30px;\n    padding-top: 22px;\n}\n"

/***/ }),

/***/ "./src/app/distributor/dcp/dcp-detail/dcp-detail.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/distributor/dcp/dcp-detail/dcp-detail.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"dcp\">\n  <form [formGroup]=\"dcpForm\" (ngSubmit)=\"handleSubmit()\">\n\n    <div>\n      <label class=\"Field\">Titre:</label>\n      <mat-form-field>\n      <input matInput formControlName=\"name\"\n      required=\"required\">\n      </mat-form-field>\n    </div>\n\n    <div>\n      <label class=\"Field\">Type:</label>\n      <mat-form-field>\n      <input matInput type=\"text\" formControlName=\"contentkind\" required=\"required\">\n    </mat-form-field>\n    </div>\n\n    <div>\n      <label class=\"Field\">Taille : </label>\n      <mat-form-field>\n      <input matInput type=\"text\" formControlName=\"size\" required=\"required\">\n    </mat-form-field>\n    </div>\n\n\n\n\n   \n       <div class=\"button\" style=\"margin-left: 520px;\">\n      <button mat-raised-button color=\"primary\" type=\"submit\" [disabled]=\"dcpForm.pristine\" (click)=\"save()\">Save</button>\n      <button mat-raised-button color=\"warn\" (click)=\"cancel()\" >Cancel</button>\n    </div>\n  </form>\n  <p>\n    Form Value: {{ dcpForm.value | json }}\n  </p>\n\n\n  <p>\n    Form Status: {{ dcpForm.status }}\n  </p>\n\n</div>\n"

/***/ }),

/***/ "./src/app/distributor/dcp/dcp-detail/dcp-detail.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/distributor/dcp/dcp-detail/dcp-detail.component.ts ***!
  \********************************************************************/
/*! exports provided: DcpDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DcpDetailComponent", function() { return DcpDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var app_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/_services */ "./src/app/_services/index.ts");
/* harmony import */ var app_message_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/message.service */ "./src/app/message.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DcpDetailComponent = /** @class */ (function () {
    function DcpDetailComponent(route, router, formBuilder, dcpService, dialogServicse) {
        this.route = route;
        this.router = router;
        this.formBuilder = formBuilder;
        this.dcpService = dcpService;
        this.dialogServicse = dialogServicse;
    }
    DcpDetailComponent.prototype.ngOnInit = function () {
        // TODO: verify in angulatr routing doc
        // why the this.route.data is needed.
        var _this = this;
        this.route.data
            .subscribe(function (data) {
            _this.editName = data.dcp.name;
            _this.dcp = data.dcp;
            _this.createFromGroup();
        });
    };
    DcpDetailComponent.prototype.createFromGroup = function () {
        this.dcpForm = this.formBuilder.group({
            'id': this.formBuilder.control(this.dcp.id),
            'name': this.formBuilder.control(this.dcp.name),
            'contentkind': this.formBuilder.control(this.dcp.contentkind),
            'size': this.formBuilder.control(this.dcp.size),
        });
    };
    DcpDetailComponent.prototype.handleSubmit = function () {
        console.log(this.dcpForm.value);
    };
    DcpDetailComponent.prototype.cancel = function () {
        this.gotoDcps();
    };
    DcpDetailComponent.prototype.save = function () {
        var _this = this;
        this.dcpService
            .updateDcp(this.dcpForm.value)
            .subscribe(function () { return _this.gotoDcps(); });
    };
    DcpDetailComponent.prototype.canDeactivate = function () {
        // Allow synchronous navigation (`true`) if no dcp or the dcp is unchanged
        if (!this.dcp || this.dcp.name === this.editName) {
            return true;
        }
        // Otherwise ask the user with the dialog service and return its
        // observable which resolves to true or false when the user decides
        //return this.dialogService.add('Discard changes?');
        return false;
    };
    DcpDetailComponent.prototype.gotoDcps = function () {
        var dcpId = this.dcp ? this.dcp.id : null;
        // Pass along the dcp id if available
        // so that the CrisisListComponent can select that dcp.
        // Add a totally useless `foo` parameter for kicks.
        // Relative navigation back to the  dcps
        this.router.navigate(['../', { id: dcpId }], { relativeTo: this.route });
    };
    DcpDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dcp-detail',
            template: __webpack_require__(/*! ./dcp-detail.component.html */ "./src/app/distributor/dcp/dcp-detail/dcp-detail.component.html"),
            styles: [__webpack_require__(/*! ./dcp-detail.component.css */ "./src/app/distributor/dcp/dcp-detail/dcp-detail.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            app_services__WEBPACK_IMPORTED_MODULE_3__["DcpService"],
            app_message_service__WEBPACK_IMPORTED_MODULE_4__["MessageService"]])
    ], DcpDetailComponent);
    return DcpDetailComponent;
}());



/***/ }),

/***/ "./src/app/distributor/dcp/dcp-list/dcp-list.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/distributor/dcp/dcp-list/dcp-list.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* CrisisListComponent's private CSS styles */\n.dcp {\n  margin: 0 0 2em 0;\n  list-style-type: none;\n  padding: 0;\n  width: 100%;\n}\n.dcp li {\n  position: relative;\n  cursor: pointer;\n  margin: .5em;\n  padding: .3em 0;\n  height: 1.6em;\n  border-radius: 4px;\n}\n.dcp tr {\n  position: relative;\n  cursor: pointer;\n  margin: .5em;\n  padding: .3em 0;\n  height: 1.6em;\n  border-radius: 4px;\n}\n.onglet   {\n  background-color: #EEE;\n  font : caption;\n  padding-right: 150px;\n  padding-bottom: 5px;\n  padding-top: 5px;\n\n}\n.dcp li:hover {\n  color: #607D8B;\n\n  left: .1em;\n}\n.dcp tr:hover {\n  color: #607D8B;\n\n  left: .1em;\n}\n.dcp a {\n  color: #888;\n  text-decoration: none;\n  display: block;\n}\n.dcp a:hover {\n  color:#015d86;\n}\n.dcp .badge {\n  display: inline-block;\n  font-size: small;\n  color: white;\n  padding: 0.8em 0.7em 0 0.7em;\n  background-color: #0173ab;\n  line-height: 1em;\n  position: relative;\n  left: -1px;\n  top: -4px;\n  height: 1.8em;\n  min-width: 16px;\n  text-align: right;\n  margin-right: .8em;\n  border-radius: 4px 0 0 4px;\n}\nbutton {\n  background-color: #eee;\n  border: none;\n  padding: 5px 10px;\n  border-radius: 4px;\n  cursor: pointer;\n  cursor: hand;\n  font-family: Arial;\n}\nbutton:hover {\n  background-color: #cfd8dc;\n}\nbutton.delete {\n  position: relative;\n  left: 194px;\n  top: -32px;\n  background-color: gray !important;\n  color: white;\n}\n.dcp li.selected {\n  background-color:;\n  color: white;\n}\n.dcp li.selected:hover {\n  background-color: #BBD8DC;\n}\n.negative {color: red;}\n.positive {color: green;}\n"

/***/ }),

/***/ "./src/app/distributor/dcp/dcp-list/dcp-list.component.html":
/*!******************************************************************!*\
  !*** ./src/app/distributor/dcp/dcp-list/dcp-list.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ul class=\"dcp\">\n\t<ul>\n\t\t<button mat-raised-button color=\"primary\" routerLink=\"./new\"\n\t\trouterLinkActive=\"active\">\n\t\tAjouter un DCP\n\t</button>\n\t</ul>\n\n\t<mat-form-field>\n\n\t\t<input matInput type=\"text\" (keyup)=\"doFilter($event.target.value)\" placeholder=\"Search dcps\" #input>\n\t</mat-form-field>\n\n\t<table mat-table [dataSource]=\"dataSource\" matSort>\n\t\t<ng-container matColumnDef=\"id\">\n\t\t\t<th mat-header-cell class=\"onglet\" *matHeaderCellDef mat-sort-header>Id</th>\n\t\t\t<td mat-cell *matCellDef=\"let dcp\">{{dcp.id}}</td>\n\t\t</ng-container>\n\n\t\t<ng-container matColumnDef=\"name\">\n\t\t\t<th mat-header-cell *matHeaderCellDef class=\"onglet\" mat-sort-header>DCP</th>\n\t\t\t<td mat-cell *matCellDef=\"let dcp\">\n\t\t\t\t<a [routerLink]=\"[dcp.id]\">{{dcp.name}}</a></td>\n\t\t</ng-container>\n\n\t\t<ng-container matColumnDef=\"contentkind\">\n\t\t\t<th mat-header-cell *matHeaderCellDef class=\"onglet\" mat-sort-header>Type</th>\n\t\t\t<td mat-cell *matCellDef=\"let dcp\">\n\t\t\t\t{{dcp.contentkind }}\n\t\t\t</td>\n\t\t</ng-container>\n\n\t\t<ng-container matColumnDef=\"size\">\n\t\t\t<th mat-header-cell *matHeaderCellDef class=\"onglet\">Taille</th>\n\t\t\t<td mat-cell *matCellDef=\"let dcp\">\n\t\t\t\t{{ dcp.size | filesize }}\n\t\t\t</td>\n\t\t</ng-container>\n\n\t\t<ng-container matColumnDef=\"action\">\n\t\t\t<th mat-header-cell *matHeaderCellDef class=\"onglet\">Action</th>\n\t\t\t<td mat-cell *matCellDef=\"let dcp\">\n\t\t\t\t<button mat-raised-button color=\"primary\">supprimer</button>\n\t\t\t</td>\n\t\t</ng-container>\n\t\t<tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n\t\t<tr mat-row *matRowDef=\"let dcp; columns: displayedColumns;\"></tr>\n\t</table>\n\n\t<mat-paginator [pageIndex]=\"0\" [pageSize]=\"50\" [pageSizeOptions]=\"[10, 25, 50, 100]\">\n\t</mat-paginator>\n\n</ul>\n"

/***/ }),

/***/ "./src/app/distributor/dcp/dcp-list/dcp-list.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/distributor/dcp/dcp-list/dcp-list.component.ts ***!
  \****************************************************************/
/*! exports provided: DcpListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DcpListComponent", function() { return DcpListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var app_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/_services */ "./src/app/_services/index.ts");
/* harmony import */ var app_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/auth/auth.service */ "./src/app/auth/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DcpListComponent = /** @class */ (function () {
    function DcpListComponent(distributorsService, route, authService) {
        var _this = this;
        this.distributorsService = distributorsService;
        this.route = route;
        this.authService = authService;
        this.displayedColumns = ['id', 'name', 'contentkind', 'size'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.doFilter = function (value) {
            _this.dataSource.filter = value.trim().toLocaleLowerCase();
        };
    }
    DcpListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.currentUser.subscribe(function (user) { return _this.currentUser = user; });
        this.distributor = this.currentUser.distributor;
        this.dcpMap$ = this.route.paramMap.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["switchMap"])(function (params) {
            _this.selectedId = +params.get('id');
            return _this.distributorsService.getDcps(_this.currentUser.distributor.id);
        }));
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.getDcpsDistributors();
    };
    DcpListComponent.prototype.getDcpsDistributors = function () {
        var _this = this;
        this.distributorsService.getDcps(this.distributor.id)
            .subscribe(function (dcp) { return (_this.dataSource.data = dcp); });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], DcpListComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], DcpListComponent.prototype, "sort", void 0);
    DcpListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'dcp-list',
            template: __webpack_require__(/*! ./dcp-list.component.html */ "./src/app/distributor/dcp/dcp-list/dcp-list.component.html"),
            styles: [__webpack_require__(/*! ./dcp-list.component.css */ "./src/app/distributor/dcp/dcp-list/dcp-list.component.css")]
        }),
        __metadata("design:paramtypes", [app_services__WEBPACK_IMPORTED_MODULE_4__["DistributorsService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            app_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"]])
    ], DcpListComponent);
    return DcpListComponent;
}());



/***/ }),

/***/ "./src/app/distributor/dcp/dcp-new/dcp-new.component.css":
/*!***************************************************************!*\
  !*** ./src/app/distributor/dcp/dcp-new/dcp-new.component.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "label {\n  display: block;\n  width: 6em;\n  margin: .5em 0;\n\n}\n\ninput .inputText {\n  flex-basis: 66.6667%;\n  font-size: 1em;\n  padding-left: .4em;\n  margin-left: 10px;\n\n}\n\nmat-form-field {    \n    top: 10px;\n    width: 280px;\n    margin-bottom:  5px;\n}\n\nbutton {\n  font-family: Arial;\n  background-color: #eee;\n  border: none;\n  padding: 5px 10px;\n  border-radius: 4px;\n  cursor: pointer;\n}\n\ndiv .button {margin-left: 520px;}\n\nbutton:hover {\n  background-color: #cfd8dc;\n}\n\nbutton:disabled {\n  background-color: #eee;\n  color: #ccc;\n  cursor: auto;\n}\n\nlabel.Field{\n    text-align: right;\n    clear: both;\n    float: left;\n    margin-left: 500px;\n    margin-right: 30px;\n    padding-top: 22px;\n}\n"

/***/ }),

/***/ "./src/app/distributor/dcp/dcp-new/dcp-new.component.html":
/*!****************************************************************!*\
  !*** ./src/app/distributor/dcp/dcp-new/dcp-new.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <form [formGroup]=\"dcpForm\" (ngSubmit)=\"handleSubmit()\">\n\n    <div>\n      <label class=\"Field\">Titre:</label>\n      <mat-form-field>\n      <input matInput formControlName=\"name\"\n      required=\"required\">\n      </mat-form-field>\n    </div>\n\n    <div>\n      <label class=\"Field\">Type:</label>\n      <mat-form-field>\n      <input matInput type=\"text\" formControlName=\"contentkind\" required=\"required\">\n    </mat-form-field>\n    </div>\n\n    <div>\n      <label class=\"Field\">Taille : </label>\n      <mat-form-field>\n      <input matInput type=\"text\" formControlName=\"size\" required=\"required\">{{filesize}}\n    </mat-form-field>\n    </div>\n\n\n\n\n   \n       <div class=\"button\" style=\"margin-left: 520px;\">\n      <button mat-raised-button color=\"primary\" type=\"submit\" [disabled]=\"dcpForm.pristine\" (click)=\"save()\">Save</button>\n      <button mat-raised-button color=\"warn\" (click)=\"cancel()\" >Cancel</button>\n    </div>\n\n  </form>\n\n  <p>\n    Form Value: {{ dcpForm.value | json }}\n  </p>\n\n\n  <p>\n    Form Status: {{ dcpForm.status }}\n  </p>\n"

/***/ }),

/***/ "./src/app/distributor/dcp/dcp-new/dcp-new.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/distributor/dcp/dcp-new/dcp-new.component.ts ***!
  \**************************************************************/
/*! exports provided: DcpNewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DcpNewComponent", function() { return DcpNewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var app_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/_services */ "./src/app/_services/index.ts");
/* harmony import */ var app_message_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/message.service */ "./src/app/message.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DcpNewComponent = /** @class */ (function () {
    function DcpNewComponent(route, router, formBuilder, dcpService, dialogServicse) {
        this.route = route;
        this.router = router;
        this.formBuilder = formBuilder;
        this.dcpService = dcpService;
        this.dialogServicse = dialogServicse;
    }
    DcpNewComponent.prototype.ngOnInit = function () {
        this.createFromGroup();
    };
    DcpNewComponent.prototype.createFromGroup = function () {
        this.dcpForm = this.formBuilder.group({
            //'id': this.formBuilder.control(),
            'name': this.formBuilder.control(''),
            'contentkind': this.formBuilder.control(''),
            'size': this.formBuilder.control(''),
        });
    };
    DcpNewComponent.prototype.handleSubmit = function () {
        console.log(this.dcpForm.value);
    };
    DcpNewComponent.prototype.cancel = function () {
        this.gotoDcps();
    };
    DcpNewComponent.prototype.save = function () {
        var _this = this;
        this.dcpService
            .addDcp(this.dcpForm.value)
            .subscribe(function () { return _this.gotoDcps(); });
    };
    DcpNewComponent.prototype.gotoDcps = function () {
        // Pass along the dcp id if available
        // so that the CrisisListComponent can select that dcp.
        // Add a totally useless `foo` parameter for kicks.
        // Relative navigation back to the  dcps
        this.router.navigate(['../'], { relativeTo: this.route });
    };
    DcpNewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dcp-new',
            template: __webpack_require__(/*! ./dcp-new.component.html */ "./src/app/distributor/dcp/dcp-new/dcp-new.component.html"),
            styles: [__webpack_require__(/*! ./dcp-new.component.css */ "./src/app/distributor/dcp/dcp-new/dcp-new.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            app_services__WEBPACK_IMPORTED_MODULE_3__["DcpService"],
            app_message_service__WEBPACK_IMPORTED_MODULE_4__["MessageService"]])
    ], DcpNewComponent);
    return DcpNewComponent;
}());



/***/ }),

/***/ "./src/app/distributor/dcp/index.ts":
/*!******************************************!*\
  !*** ./src/app/distributor/dcp/index.ts ***!
  \******************************************/
/*! exports provided: DcpListComponent, DcpDetailComponent, DcpNewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _dcp_list_dcp_list_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dcp-list/dcp-list.component */ "./src/app/distributor/dcp/dcp-list/dcp-list.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DcpListComponent", function() { return _dcp_list_dcp_list_component__WEBPACK_IMPORTED_MODULE_0__["DcpListComponent"]; });

/* harmony import */ var _dcp_detail_dcp_detail_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dcp-detail/dcp-detail.component */ "./src/app/distributor/dcp/dcp-detail/dcp-detail.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DcpDetailComponent", function() { return _dcp_detail_dcp_detail_component__WEBPACK_IMPORTED_MODULE_1__["DcpDetailComponent"]; });

/* harmony import */ var _dcp_new_dcp_new_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dcp-new/dcp-new.component */ "./src/app/distributor/dcp/dcp-new/dcp-new.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DcpNewComponent", function() { return _dcp_new_dcp_new_component__WEBPACK_IMPORTED_MODULE_2__["DcpNewComponent"]; });






/***/ }),

/***/ "./src/app/distributor/distributor-home/distributor-home.component.css":
/*!*****************************************************************************!*\
  !*** ./src/app/distributor/distributor-home/distributor-home.component.css ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "rm label {\n  display: inline-block;\n  width: 3em;\n  margin: .5em 0;\n  color: #607D8B;\n  font-weight: bold;\n}\n\n\n\n\nmat-card {\n  box-shadow: 10px 10px 5px #656565;\n}\n\n\n"

/***/ }),

/***/ "./src/app/distributor/distributor-home/distributor-home.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/distributor/distributor-home/distributor-home.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div mat-card class=\"AffDistrib\" fxLayout=\"row\" fxLayoutGap=\"20px\" fxLayoutAlign=\"center center\">\n\n  <div class=\"positionOfdesc\" fxLayout=\"column\" fxLayoutAlign=\"left\" fxLayoutGap=\"0.5%\" style=\"width:300px; margin-bottom: 15px;\">\n          \n\n   <p class=\"title-desc\" style=\"margin-bottom:0.5%; border-bottom: 0.5px solid #cecece;\">\n    <strong>Mail:</strong>\n    {{currentUser.email}}</p>\n          \n   <p class=\"title-desc\" style=\"margin-bottom:0.5%; border-bottom: 0.5px solid #cecece;\">\n    <strong>Contact :</strong>\n    {{currentUser.distributor.contact}}</p>\n  \n  </div>\n\n  <div class=\"positionOfdesc2\" fxLayout=\"column\" fxLayout.xs=\"column\" fxLayoutWrap fxLayoutGap=\"0.5%\" fxLayoutAlign=\"left\" style=\"width: 300px; margin-bottom: 51px;\">\n   \n\n   <p class=\"title-desc\" style=\"margin-bottom:0.5%; border-bottom: 0.5px solid #cecece;\">\n    <strong>Code CNC :</strong>\n    {{currentUser.distributor.cnc_code}}</p>   \n\n   <p class=\"title-desc\" style=\"margin-bottom:0.5%; border-bottom: 0.5px solid #cecece;\">\n    <strong>Identifiant utilisateur :</strong>\n    {{currentUser.login}}</p>   \n </div></div>"

/***/ }),

/***/ "./src/app/distributor/distributor-home/distributor-home.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/distributor/distributor-home/distributor-home.component.ts ***!
  \****************************************************************************/
/*! exports provided: DistributorHomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DistributorHomeComponent", function() { return DistributorHomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var app_models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/_models */ "./src/app/_models/index.ts");
/* harmony import */ var app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/auth/auth.service */ "./src/app/auth/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DistributorHomeComponent = /** @class */ (function () {
    function DistributorHomeComponent(route, authService) {
        this.route = route;
        this.authService = authService;
    }
    DistributorHomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Capture the session ID if available
        this.authService.currentUser.subscribe(function (user) { return _this.currentUser = user; });
        this.sessionId = this.route
            .queryParamMap
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (params) { return params.get('session_id') || 'None'; }));
        // Capture the fragment if available
        this.token = this.route
            .fragment
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (fragment) { return fragment || 'None'; }));
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", app_models__WEBPACK_IMPORTED_MODULE_3__["User"])
    ], DistributorHomeComponent.prototype, "currentUser", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], DistributorHomeComponent.prototype, "distributorid", void 0);
    DistributorHomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-distributor-home',
            template: __webpack_require__(/*! ./distributor-home.component.html */ "./src/app/distributor/distributor-home/distributor-home.component.html"),
            styles: [__webpack_require__(/*! ./distributor-home.component.css */ "./src/app/distributor/distributor-home/distributor-home.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            app_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"]])
    ], DistributorHomeComponent);
    return DistributorHomeComponent;
}());



/***/ }),

/***/ "./src/app/distributor/distributor-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/distributor/distributor-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: DistributorRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DistributorRoutingModule", function() { return DistributorRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _distributor_home_distributor_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./distributor-home/distributor-home.component */ "./src/app/distributor/distributor-home/distributor-home.component.ts");
/* harmony import */ var _distributor_distributor_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./distributor/distributor.component */ "./src/app/distributor/distributor/distributor.component.ts");
/* harmony import */ var _movie___WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./movie/ */ "./src/app/distributor/movie/index.ts");
/* harmony import */ var _dcp___WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dcp/ */ "./src/app/distributor/dcp/index.ts");
/* harmony import */ var _movie_detail_resolver_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./movie-detail-resolver.service */ "./src/app/distributor/movie-detail-resolver.service.ts");
/* harmony import */ var _dcp_dcp_detail_resolver_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dcp/dcp-detail-resolver.service */ "./src/app/distributor/dcp/dcp-detail-resolver.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var distributorRoutes = [
    {
        path: '',
        component: _distributor_distributor_component__WEBPACK_IMPORTED_MODULE_3__["DistributorComponent"],
        children: [
            {
                path: 'dcp',
                component: _dcp___WEBPACK_IMPORTED_MODULE_5__["DcpListComponent"],
                data: { animation: 'movies' },
            },
            { path: 'dcp/new',
                component: _dcp___WEBPACK_IMPORTED_MODULE_5__["DcpNewComponent"],
                data: { animation: 'movie' }
            },
            { path: 'dcp/:id',
                component: _dcp___WEBPACK_IMPORTED_MODULE_5__["DcpDetailComponent"],
                resolve: {
                    dcp: _dcp_dcp_detail_resolver_service__WEBPACK_IMPORTED_MODULE_7__["DcpDetailResolverService"]
                },
                data: { animation: 'movie' }
            },
            {
                path: 'film',
                component: _movie___WEBPACK_IMPORTED_MODULE_4__["MovieListComponent"],
                data: { animation: 'movies' }
            },
            { path: 'film/new',
                component: _movie___WEBPACK_IMPORTED_MODULE_4__["MovieNewComponent"],
            },
            { path: 'film/:id',
                component: _movie___WEBPACK_IMPORTED_MODULE_4__["MovieDetailComponent"],
                resolve: {
                    movie: _movie_detail_resolver_service__WEBPACK_IMPORTED_MODULE_6__["MovieDetailResolverService"]
                },
                data: { animation: 'movie' }
            },
            {
                path: '',
                component: _distributor_home_distributor_home_component__WEBPACK_IMPORTED_MODULE_2__["DistributorHomeComponent"]
            }
        ]
    }
];
/*
const distributorRoutes: Routes = [
  {
    path: '',
    component: DistributorComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        children: [
          { path: 'film',  component: DistributorMovieListComponent, data: { animation: 'movies' } },
          { path: 'film/:id',
            component:  MovieDetailComponent,
            resolve: {
              movie: MovieDetailResolverService
            },
            data: { animation: 'movie' }
          },
        ]
      }
    ]
  },
];
*/
var DistributorRoutingModule = /** @class */ (function () {
    function DistributorRoutingModule() {
    }
    DistributorRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(distributorRoutes)
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]
            ]
        })
    ], DistributorRoutingModule);
    return DistributorRoutingModule;
}());



/***/ }),

/***/ "./src/app/distributor/distributor.module.ts":
/*!***************************************************!*\
  !*** ./src/app/distributor/distributor.module.ts ***!
  \***************************************************/
/*! exports provided: DistributorModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DistributorModule", function() { return DistributorModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ngx_filesize__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-filesize */ "./node_modules/ngx-filesize/dist/index.js");
/* harmony import */ var _distributor_home_distributor_home_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./distributor-home/distributor-home.component */ "./src/app/distributor/distributor-home/distributor-home.component.ts");
/* harmony import */ var _distributor_distributor_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./distributor/distributor.component */ "./src/app/distributor/distributor/distributor.component.ts");
/* harmony import */ var _movie___WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./movie/ */ "./src/app/distributor/movie/index.ts");
/* harmony import */ var _dcp___WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./dcp/ */ "./src/app/distributor/dcp/index.ts");
/* harmony import */ var _distributor_routing_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./distributor-routing.module */ "./src/app/distributor/distributor-routing.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var DistributorModule = /** @class */ (function () {
    function DistributorModule() {
    }
    DistributorModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginatorModule"],
                _distributor_routing_module__WEBPACK_IMPORTED_MODULE_9__["DistributorRoutingModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                ngx_filesize__WEBPACK_IMPORTED_MODULE_4__["FileSizeModule"]
            ],
            declarations: [
                _distributor_home_distributor_home_component__WEBPACK_IMPORTED_MODULE_5__["DistributorHomeComponent"],
                _movie___WEBPACK_IMPORTED_MODULE_7__["MovieListComponent"],
                _movie___WEBPACK_IMPORTED_MODULE_7__["MovieDetailComponent"],
                _movie___WEBPACK_IMPORTED_MODULE_7__["MovieNewComponent"],
                _dcp___WEBPACK_IMPORTED_MODULE_8__["DcpListComponent"],
                _dcp___WEBPACK_IMPORTED_MODULE_8__["DcpDetailComponent"],
                _dcp___WEBPACK_IMPORTED_MODULE_8__["DcpNewComponent"],
                _distributor_distributor_component__WEBPACK_IMPORTED_MODULE_6__["DistributorComponent"],
            ]
        })
    ], DistributorModule);
    return DistributorModule;
}());



/***/ }),

/***/ "./src/app/distributor/distributor/distributor.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/distributor/distributor/distributor.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nav button:hover {\n background-color: #2a3b98;\n padding-top: 14px;\n padding-bottom: 14px;\n transition-property: left right;\n transition-duration: 0.5s;\n}\n\nnav button.active {\n background-color: #2a3b98;                                    \n padding-top: 14px;\n padding-bottom: 14px;\n\n}\n\nnav button.disconnect {\n background-color: #ed2503;\n     color: rgba(255, 255, 255, 0.87);\n\n  \n}\n\nnav button.disconnect :hover {\n      color: rgba(255, 255, 255, 0.87);\n background-color: #ed2503;\n padding-top: 14px;\n padding-bottom: 14px;\n transition-property: left right;\n transition-duration: 1.5s;\n}\n\nnav button.disconnect {\n background-color: #ed2503;\n     color: rgba(255, 255, 255, 0.87);\n\n  \n}\n\nnav button.disconnect :hover {\n      color: rgba(255, 255, 255, 0.87);\n background-color: #ed2503;\n padding-top: 14px;\n padding-bottom: 14px;\n transition-property: left right;\n transition-duration: 1.5s;\n}"

/***/ }),

/***/ "./src/app/distributor/distributor/distributor.component.html":
/*!********************************************************************!*\
  !*** ./src/app/distributor/distributor/distributor.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav>\n    <mat-toolbar color=\"primary\" class=\"toolbar\">\n\n        <div> {{currentUser?.login}} </div>\n    <button mat-button  routerLink=\"./\" routerLinkActive=\"active\"\n                       [routerLinkActiveOptions]=\"{ exact: true }\">Dashboard\n    </button>\n\n        <button mat-button routerLink=\"./film\" routerLinkActive=\"active\">Mon catalogue</button>\n        <button mat-button routerLink=\"./dcp\" routerLinkActive=\"active\">Mes DCP</button>\n\n\n        <button mat-button routerLink=\"./demande_de_film\" routerLinkActive=\"active\">Les demandes de films</button>\n\n        <button mat-button routerLink=\"./salle\" routerLinkActive=\"active\">Les salles connectées</button>\n\n\n        <button mat-button  routerLinkActive=\"disconnect\" class=\"disconnect\" (click)=\"logout()\">Déconnexion</button>\n    </mat-toolbar>\n</nav>\n\n\n<h2>DISTRIBUTEUR {{ currentUser.distributor.name }} </h2>\n\n\n<div [@routeAnimation]=\"getAnimationData(routerOutlet)\">\n  <router-outlet #routerOutlet=\"outlet\"></router-outlet>\n</div>\n"

/***/ }),

/***/ "./src/app/distributor/distributor/distributor.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/distributor/distributor/distributor.component.ts ***!
  \******************************************************************/
/*! exports provided: DistributorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DistributorComponent", function() { return DistributorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var app_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/animations */ "./src/app/animations.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DistributorComponent = /** @class */ (function () {
    function DistributorComponent(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    DistributorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.currentUser.subscribe(function (user) { return _this.currentUser = user; });
    };
    DistributorComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['/login']);
    };
    DistributorComponent.prototype.getAnimationData = function (outlet) {
        return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
    };
    DistributorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-distributor',
            template: __webpack_require__(/*! ./distributor.component.html */ "./src/app/distributor/distributor/distributor.component.html"),
            styles: [__webpack_require__(/*! ./distributor.component.css */ "./src/app/distributor/distributor/distributor.component.css")],
            animations: [app_animations__WEBPACK_IMPORTED_MODULE_3__["slideInAnimation"]]
        }),
        __metadata("design:paramtypes", [app_auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], DistributorComponent);
    return DistributorComponent;
}());



/***/ }),

/***/ "./src/app/distributor/movie-detail-resolver.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/distributor/movie-detail-resolver.service.ts ***!
  \**************************************************************/
/*! exports provided: MovieDetailResolverService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MovieDetailResolverService", function() { return MovieDetailResolverService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var app_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/_services */ "./src/app/_services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MovieDetailResolverService = /** @class */ (function () {
    function MovieDetailResolverService(movieService, router) {
        this.movieService = movieService;
        this.router = router;
    }
    MovieDetailResolverService.prototype.resolve = function (route, state) {
        var _this = this;
        var id = route.paramMap.get('id');
        return this.movieService.getMovie(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["mergeMap"])(function (movie) {
            if (movie) {
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(movie);
            }
            else { // id not found
                _this.router.navigate(['/distributor/film']);
                return rxjs__WEBPACK_IMPORTED_MODULE_2__["EMPTY"];
            }
        }));
    };
    MovieDetailResolverService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [app_services__WEBPACK_IMPORTED_MODULE_4__["MovieService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], MovieDetailResolverService);
    return MovieDetailResolverService;
}());



/***/ }),

/***/ "./src/app/distributor/movie/index.ts":
/*!********************************************!*\
  !*** ./src/app/distributor/movie/index.ts ***!
  \********************************************/
/*! exports provided: MovieListComponent, MovieDetailComponent, MovieNewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _movie_list_movie_list_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./movie-list/movie-list.component */ "./src/app/distributor/movie/movie-list/movie-list.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "MovieListComponent", function() { return _movie_list_movie_list_component__WEBPACK_IMPORTED_MODULE_0__["MovieListComponent"]; });

/* harmony import */ var _movie_detail_movie_detail_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./movie-detail/movie-detail.component */ "./src/app/distributor/movie/movie-detail/movie-detail.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "MovieDetailComponent", function() { return _movie_detail_movie_detail_component__WEBPACK_IMPORTED_MODULE_1__["MovieDetailComponent"]; });

/* harmony import */ var _movie_new_movie_new_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./movie-new/movie-new.component */ "./src/app/distributor/movie/movie-new/movie-new.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "MovieNewComponent", function() { return _movie_new_movie_new_component__WEBPACK_IMPORTED_MODULE_2__["MovieNewComponent"]; });






/***/ }),

/***/ "./src/app/distributor/movie/movie-detail/movie-detail.component.css":
/*!***************************************************************************!*\
  !*** ./src/app/distributor/movie/movie-detail/movie-detail.component.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "label {\n  display: block;\n  width: 6em;\n  margin: .5em 0;\n\n}\n\ninput .inputText {\n  flex-basis: 66.6667%;\n  font-size: 1em;\n  padding-left: .4em;\n  margin-left: 10px;\n\n}\n\nmat-form-field {    \n    top: 10px;\n    width: 280px;\n    margin-bottom:  5px;\n}\n\nbutton {\n  font-family: Arial;\n  background-color: #eee;\n  border: none;\n  padding: 5px 10px;\n  border-radius: 4px;\n  cursor: pointer;\n}\n\ndiv .button {margin-left: 520px;}\n\nbutton:hover {\n  background-color: #cfd8dc;\n}\n\nbutton:disabled {\n  background-color: #eee;\n  color: #ccc;\n  cursor: auto;\n}\n\nlabel.Field{\n    text-align: right;\n    clear: both;\n    float: left;\n    margin-left: 500px;\n    margin-right: 30px;\n    padding-top: 22px;\n}\n\ntextarea {\n      top: 10px;\n    width: 250px;\n    margin-bottom:  5px;\n}"

/***/ }),

/***/ "./src/app/distributor/movie/movie-detail/movie-detail.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/distributor/movie/movie-detail/movie-detail.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"movie\">\n  <form [formGroup]=\"movieForm\" (ngSubmit)=\"handleSubmit()\">\n    \n    <div>\n      <label class=\"Field\">Titre:</label>\n      <mat-form-field>\n      <input class=\"inputText\" matInput formControlName=\"title\"\n      required=\"required\">\n      </mat-form-field>\n    </div>\n\n    <div>\n      <label class=\"Field\">Date:</label>\n      <mat-form-field>\n      <input class=\"inputText\" matInput type=\"date\" formControlName=\"releasedate\" required=\"required\">\n    </mat-form-field>\n    </div>\n\n    <div>\n      <label class=\"Field\">Directeur:</label>\n      <mat-form-field>\n      <input class=\"inputText\" matInput type=\"text\" formControlName=\"director\" required=\"required\">\n    </mat-form-field>\n    </div>\n\n   <div>\n     <label class=\"Field\">Scénario:</label>\n      <mat-form-field>\n      <input class=\"inputText\" matInput type=\"text\" formControlName=\"scenario\" required=\"required\">\n    </mat-form-field>\n  </div>\n\n   <div>\n    <label class=\"Field\">Acteur:</label>\n      <mat-form-field>\n      <input class=\"inputText\" matInput type=\"text\" formControlName=\"actor\" required=\"required\">\n    </mat-form-field>\n  </div>\n\n   <div>\n      <label class=\"Field\">Pays:</label>\n      <mat-form-field>\n      <input class=\"inputText\" matInput type=\"text\" formControlName=\"country\" required=\"required\">\n    </mat-form-field>\n  </div>\n\n     <div>\n      <label class=\"Field\">Synopsis:</label>\n      <mat-form-field>\n      <textarea matInput type=\"text\" formControlName=\"synopsis\" required=\"required\"></textarea>\n    </mat-form-field>\n  </div>\n\n\n\n\n    <div class=\"button\">\n      <button mat-raised-button color=\"primary\" type=\"submit\" [disabled]=\"movieForm.pristine\" (click)=\"save()\">Save</button>\n      <button mat-raised-button color=\"warn\" (click)=\"cancel()\" [disabled]=\"movieForm.pristine\">Cancel</button>\n    </div>\n  </form>\n  \n  <p>\n    Form Value: {{ movieForm.value | json }}\n  </p>\n\n\n  <p>\n    Form Status: {{ movieForm.status }}\n  </p>\n\n</div>\n"

/***/ }),

/***/ "./src/app/distributor/movie/movie-detail/movie-detail.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/distributor/movie/movie-detail/movie-detail.component.ts ***!
  \**************************************************************************/
/*! exports provided: MovieDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MovieDetailComponent", function() { return MovieDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var app_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/_services */ "./src/app/_services/index.ts");
/* harmony import */ var app_message_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/message.service */ "./src/app/message.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MovieDetailComponent = /** @class */ (function () {
    function MovieDetailComponent(route, router, formBuilder, movieService, dialogServicse) {
        this.route = route;
        this.router = router;
        this.formBuilder = formBuilder;
        this.movieService = movieService;
        this.dialogServicse = dialogServicse;
    }
    MovieDetailComponent.prototype.ngOnInit = function () {
        // TODO: verify in angulatr routing doc
        // why the this.route.data is needed.
        var _this = this;
        this.route.data
            .subscribe(function (data) {
            _this.editName = data.movie.title;
            _this.movie = data.movie;
        });
        this.createFromGroup();
    };
    MovieDetailComponent.prototype.createFromGroup = function () {
        this.movieForm = this.formBuilder.group({
            'id': this.formBuilder.control(this.movie.id),
            'title': this.formBuilder.control(this.movie.title),
            'releasedate': this.formBuilder.control(this.movie.releasedate),
            'director': this.formBuilder.control(this.movie.director),
            'scenario': this.formBuilder.control(this.movie.scenario),
            'actor': this.formBuilder.control(this.movie.actor),
            'country': this.formBuilder.control(this.movie.country),
            'synopsis': this.formBuilder.control(this.movie.synopsis)
        });
    };
    MovieDetailComponent.prototype.handleSubmit = function () {
        console.log(this.movieForm.value);
    };
    MovieDetailComponent.prototype.cancel = function () {
        this.gotoMovies();
    };
    MovieDetailComponent.prototype.save = function () {
        var _this = this;
        this.movieService
            .updateMovie(this.movieForm.value)
            .subscribe(function () { return _this.gotoMovies(); });
    };
    MovieDetailComponent.prototype.canDeactivate = function () {
        // Allow synchronous navigation (`true`) if no movie or the movie is unchanged
        if (!this.movie || this.movie.title === this.editName) {
            return true;
        }
        // Otherwise ask the user with the dialog service and return its
        // observable which resolves to true or false when the user decides
        //return this.dialogService.add('Discard changes?');
        return false;
    };
    MovieDetailComponent.prototype.gotoMovies = function () {
        var movieId = this.movie ? this.movie.id : null;
        // Pass along the movie id if available
        // so that the CrisisListComponent can select that movie.
        // Add a totally useless `foo` parameter for kicks.
        // Relative navigation back to the  movies
        this.router.navigate(['../', { id: movieId }], { relativeTo: this.route });
    };
    MovieDetailComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-movie-detail',
            template: __webpack_require__(/*! ./movie-detail.component.html */ "./src/app/distributor/movie/movie-detail/movie-detail.component.html"),
            styles: [__webpack_require__(/*! ./movie-detail.component.css */ "./src/app/distributor/movie/movie-detail/movie-detail.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            app_services__WEBPACK_IMPORTED_MODULE_3__["MovieService"],
            app_message_service__WEBPACK_IMPORTED_MODULE_4__["MessageService"]])
    ], MovieDetailComponent);
    return MovieDetailComponent;
}());



/***/ }),

/***/ "./src/app/distributor/movie/movie-list/movie-list.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/distributor/movie/movie-list/movie-list.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* CrisisListComponent's private CSS styles */\n.movie {\n  margin: 0 0 2em 0;\n  list-style-type: none;\n  padding: 0;\n  width: 100%;\n}\n.movie li {\n  position: relative;\n  cursor: pointer;\n  margin: .5em;\n  padding: .3em 0;\n  height: 1.6em;\n  border-radius: 4px;\n}\n.movie tr {\n  position: relative;\n  cursor: pointer;\n  margin: .5em;\n  padding: .3em 0;\n  height: 1.6em;\n  border-radius: 4px;\n}\n.onglet   {\n  background-color: #EEE;\n  font : caption;\n  padding-right: 150px;\n  padding-bottom: 5px;\n  padding-top: 5px;\n\n}\n.movie li:hover {\n  color: #607D8B;\n  \n  left: .1em;\n}\n.movie tr:hover {\n  color: #607D8B;\n\n  left: .1em;\n}\n.movie a {\n  color: #888;\n  text-decoration: none;\n  display: block;\n}\n.movie a:hover {\n  color:#015d86;\n}\n.movie .badge {\n  display: inline-block;\n  font-size: small;\n  color: white;\n  padding: 0.8em 0.7em 0 0.7em;\n  background-color: #0173ab;\n  line-height: 1em;\n  position: relative;\n  left: -1px;\n  top: -4px;\n  height: 1.8em;\n  min-width: 16px;\n  text-align: right;\n  margin-right: .8em;\n  border-radius: 4px 0 0 4px;\n}\nbutton {\n  background-color: #eee;\n  border: none;\n  padding: 5px 10px;\n  border-radius: 4px;\n  cursor: pointer;\n  cursor: hand;\n  font-family: Arial;\n}\nbutton:hover {\n  background-color: #cfd8dc;\n}\nbutton.delete {\n  position: relative;\n  left: 194px;\n  top: -32px;\n  background-color: gray !important;\n  color: white;\n}\n.movie li.selected {\n  background-color:;\n  color: white;\n}\n.movie li.selected:hover {\n  background-color: #BBD8DC;\n}\n.negative {color: red;}\n.positive {color: green;}"

/***/ }),

/***/ "./src/app/distributor/movie/movie-list/movie-list.component.html":
/*!************************************************************************!*\
  !*** ./src/app/distributor/movie/movie-list/movie-list.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<ul class=\"movie\">\n\t<ul>\n\t<button mat-raised-button color=\"primary\" routerLink=\"./new\" routerLinkActive=\"active\">Ajouter un film</button>\n</ul>\n<mat-form-field>\n\n\t\t<input matInput type=\"text\" (keyup)=\"doFilter($event.target.value)\" placeholder=\"Search movies\" #input >\n</mat-form-field>\n\n\n<table mat-table [dataSource]=\"dataSource\" matSort>\n\n\t\t<ng-container matColumnDef=\"affiche\">\n\t\t\t\t<th mat-header-cell *matHeaderCellDef class=\"onglet\">Affiche</th>\n\t\t\t\t<td mat-cell *matCellDef=\"let movie\">\n\t\t\t\t\t\t<img width=\"80px;\" height=\"110px;\" src=\"assets/img/img-{{movie.id}}.jpg\"  onerror=\"this.src='/assets/img/default.png'\"/>\n\t\t\t\t</td>\n\t\t</ng-container>\n\n\n          <ng-container matColumnDef=\"id\">     \t\n               <th mat-header-cell class=\"onglet\" *matHeaderCellDef mat-sort-header>Id</th>           \n                <td mat-cell *matCellDef= \"let movie\">{{movie.id}}</td>\n           </ng-container>\n\n\n\n\t\t<ng-container matColumnDef=\"title\" >  \n\t\t\t\t<th mat-header-cell *matHeaderCellDef class=\"onglet\"  mat-sort-header>Titre</th>\n\t\t\t\t<td  mat-cell *matCellDef= \"let movie\">\n\t\t\t\t\t\t<a [routerLink]=\"[movie.id]\">{{movie.title}}</a></td>\n\t\t</ng-container>\n\n\t\t<ng-container matColumnDef=\"releasedate\">\n\t\t\t\t<th mat-header-cell *matHeaderCellDef class=\"onglet\" mat-sort-header>Date de Sortie</th>\n\t\t\t\t<td  mat-cell *matCellDef=\"let movie\">\n\t\t\t\t\t\t{{movie.releasedate | date : 'dd MMM y'}}\n\t\t\t\t</td>\n\t\t</ng-container>\n\n\n\n\t\t<ng-container matColumnDef=\"FTR\">    \n\t\t\t\t<th mat-header-cell *matHeaderCellDef class=\"onglet\">FTR disponible(s)</th>\n\t\t\t\t<td mat-cell *matCellDef= \"let movie\" [ngClass]=\"{ 'positive' : movie.nb_valid_ftr > 0,'negative' : movie.nb_valid_ftr == 0 }\">{{movie.nb_valid_ftr}}</td>\n\t\t</ng-container>\n\n\n\t\t<ng-container matColumnDef=\"TLR\">   \n\t\t\t\t<th mat-header-cell *matHeaderCellDef class=\"onglet\">TLR disponible(s)</th>\n\t\t\t\t<td mat-cell *matCellDef= \"let movie\" [ngClass]=\"{ 'positive' : movie.nb_valid_tlr > 0,'negative' : movie.nb_valid_tlr == 0 }\">\n\t\t\t\t\t\t{{movie.nb_valid_tlr}}\n\t\t\t\t</td>\n\t\t</ng-container>\n\n\n\n\t\t<ng-container matColumnDef=\"action\">\n\t\t\t\t<th mat-header-cell *matHeaderCellDef class=\"onglet\">Action</th>\n\t\t\t\t<td mat-cell *matCellDef= \"let movie\">\n\t\t\t\t\t\t<button mat-raised-button color=\"primary\">Distribuer</button>\n\t\t\t\t</td>\n\t\t</ng-container>\n\t\t<tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n\t\t<tr mat-row *matRowDef=\"let movie; columns: displayedColumns;\"></tr>\n</table>\n\n\n<mat-paginator       \n\t\t[pageIndex]=\"0\"\n\t\t[pageSize]=\"50\"\n\t\t[pageSizeOptions]=\"[10, 25, 50, 100]\">\n</mat-paginator>\n\n</ul>\n"

/***/ }),

/***/ "./src/app/distributor/movie/movie-list/movie-list.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/distributor/movie/movie-list/movie-list.component.ts ***!
  \**********************************************************************/
/*! exports provided: MovieListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MovieListComponent", function() { return MovieListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var app_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/_services */ "./src/app/_services/index.ts");
/* harmony import */ var app_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/_models */ "./src/app/_models/index.ts");
/* harmony import */ var app_auth_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/auth/auth.service */ "./src/app/auth/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MovieListComponent = /** @class */ (function () {
    function MovieListComponent(distributorsService, route, authService) {
        var _this = this;
        this.distributorsService = distributorsService;
        this.route = route;
        this.authService = authService;
        this.displayedColumns = ['affiche', 'id', 'title', 'releasedate', 'FTR', 'TLR', 'action'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.doFilter = function (value) {
            _this.dataSource.filter = value.trim().toLocaleLowerCase();
        };
    }
    MovieListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.currentUser.subscribe(function (user) { return _this.currentUser = user; });
        this.distributor = this.currentUser.distributor;
        this.movieMap$ = this.route.paramMap.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["switchMap"])(function (params) {
            _this.selectedId = +params.get('id');
            return _this.distributorsService.getMovies(_this.currentUser.distributor.id);
        }));
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.getMoviesDistributors();
    };
    MovieListComponent.prototype.getMoviesDistributors = function () {
        var _this = this;
        this.distributorsService.getMovies(this.distributor.id)
            .subscribe(function (movie) { return (_this.dataSource.data = movie); });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", app_models__WEBPACK_IMPORTED_MODULE_5__["Distributor"])
    ], MovieListComponent.prototype, "distributor", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], MovieListComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], MovieListComponent.prototype, "sort", void 0);
    MovieListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'movie-list',
            template: __webpack_require__(/*! ./movie-list.component.html */ "./src/app/distributor/movie/movie-list/movie-list.component.html"),
            styles: [__webpack_require__(/*! ./movie-list.component.css */ "./src/app/distributor/movie/movie-list/movie-list.component.css")]
        }),
        __metadata("design:paramtypes", [app_services__WEBPACK_IMPORTED_MODULE_4__["DistributorsService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            app_auth_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthenticationService"]])
    ], MovieListComponent);
    return MovieListComponent;
}());



/***/ }),

/***/ "./src/app/distributor/movie/movie-new/movie-new.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/distributor/movie/movie-new/movie-new.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "label {\n  display: block;\n  width: 6em;\n  margin: .5em 0;\n\n}\n\ninput .inputText {\n  flex-basis: 66.6667%;\n  font-size: 1em;\n  padding-left: .4em;\n  margin-left: 10px;\n\n}\n\nmat-form-field {    \n    top: 10px;\n    width: 280px;\n    margin-bottom:  5px;\n}\n\nbutton {\n  font-family: Arial;\n  background-color: #eee;\n  border: none;\n  padding: 5px 10px;\n  border-radius: 4px;\n  cursor: pointer;\n}\n\nbutton:hover {\n  background-color: #cfd8dc;\n}\n\nbutton:disabled {\n  background-color: #eee;\n  color: #ccc;\n  cursor: auto;\n}\n\nlabel.Field{\n    text-align: right;\n    clear: both;\n    float: left;\n    margin-left: 500px;\n    margin-right: 30px;\n    padding-top: 22px;\n}\n\ntextarea {\n      top: 10px;\n    width: 250px;\n    margin-bottom:  5px;\n}"

/***/ }),

/***/ "./src/app/distributor/movie/movie-new/movie-new.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/distributor/movie/movie-new/movie-new.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <form [formGroup]=\"movieForm\" (ngSubmit)=\"handleSubmit()\">\n\n   <div>\n      <label class=\"Field\">Titre:</label>\n      <mat-form-field>\n      <input class=\"inputText\" matInput formControlName=\"title\"\n      required=\"required\">\n      </mat-form-field>\n    </div>\n\n    <div>\n      <label class=\"Field\">Date:</label>\n      <mat-form-field>\n      <input class=\"inputText\" matInput type=\"date\" formControlName=\"releasedate\" required=\"required\">\n    </mat-form-field>\n    </div>\n\n    <div>\n      <label class=\"Field\">Directeur:</label>\n      <mat-form-field>\n      <input class=\"inputText\" matInput type=\"text\" formControlName=\"director\" required=\"required\">\n    </mat-form-field>\n    </div>\n\n   <div>\n     <label class=\"Field\">Scénario:</label>\n      <mat-form-field>\n      <input class=\"inputText\" matInput type=\"text\" formControlName=\"scenario\" required=\"required\">\n    </mat-form-field>\n  </div>\n\n   <div>\n    <label class=\"Field\">Acteur:</label>\n      <mat-form-field>\n      <input class=\"inputText\" matInput type=\"text\" formControlName=\"actor\" required=\"required\">\n    </mat-form-field>\n  </div>\n\n   <div>\n      <label class=\"Field\">Pays:</label>\n      <mat-form-field>\n      <input class=\"inputText\" matInput type=\"text\" formControlName=\"country\" required=\"required\">\n    </mat-form-field>\n  </div>\n\n     <div>\n      <label class=\"Field\">Synopsis:</label>\n      <mat-form-field>\n      <textarea matInput type=\"text\" formControlName=\"synopsis\" required=\"required\"></textarea>\n    </mat-form-field>\n  </div>\n\n\n\n    <div class=\"button\" style=\"margin-left: 520px;\">\n      <button mat-raised-button color=\"primary\" type=\"submit\" [disabled]=\"movieForm.pristine\" (click)=\"save()\">Save</button>\n      <button mat-raised-button color=\"warn\" (click)=\"cancel()\" >Cancel</button>\n    </div>\n  </form>\n\n  <p>\n    Form Value: {{ movieForm.value | json }}\n  </p>\n\n\n  <p>\n    Form Status: {{ movieForm.status }}\n  </p>\n"

/***/ }),

/***/ "./src/app/distributor/movie/movie-new/movie-new.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/distributor/movie/movie-new/movie-new.component.ts ***!
  \********************************************************************/
/*! exports provided: MovieNewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MovieNewComponent", function() { return MovieNewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var app_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/_services */ "./src/app/_services/index.ts");
/* harmony import */ var app_message_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/message.service */ "./src/app/message.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MovieNewComponent = /** @class */ (function () {
    function MovieNewComponent(route, router, formBuilder, movieService, dialogServicse) {
        this.route = route;
        this.router = router;
        this.formBuilder = formBuilder;
        this.movieService = movieService;
        this.dialogServicse = dialogServicse;
    }
    MovieNewComponent.prototype.ngOnInit = function () {
        this.createFromGroup();
    };
    MovieNewComponent.prototype.createFromGroup = function () {
        this.movieForm = this.formBuilder.group({
            //'id': this.formBuilder.control(),
            'title': this.formBuilder.control(''),
            'releasedate': this.formBuilder.control(''),
            'director': this.formBuilder.control(''),
            'scenario': this.formBuilder.control(''),
            'actor': this.formBuilder.control(''),
            'country': this.formBuilder.control(''),
            'synopsis': this.formBuilder.control('')
        });
    };
    MovieNewComponent.prototype.handleSubmit = function () {
        console.log(this.movieForm.value);
    };
    MovieNewComponent.prototype.cancel = function () {
        this.gotoMovies();
    };
    MovieNewComponent.prototype.save = function () {
        var _this = this;
        this.movieService
            .addMovie(this.movieForm.value)
            .subscribe(function () { return _this.gotoMovies(); });
    };
    MovieNewComponent.prototype.gotoMovies = function () {
        // Pass along the movie id if available
        // so that the CrisisListComponent can select that movie.
        // Add a totally useless `foo` parameter for kicks.
        // Relative navigation back to the  movies
        this.router.navigate(['../'], { relativeTo: this.route });
    };
    MovieNewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-movie-new',
            template: __webpack_require__(/*! ./movie-new.component.html */ "./src/app/distributor/movie/movie-new/movie-new.component.html"),
            styles: [__webpack_require__(/*! ./movie-new.component.css */ "./src/app/distributor/movie/movie-new/movie-new.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            app_services__WEBPACK_IMPORTED_MODULE_3__["MovieService"],
            app_message_service__WEBPACK_IMPORTED_MODULE_4__["MessageService"]])
    ], MovieNewComponent);
    return MovieNewComponent;
}());



/***/ })

}]);
//# sourceMappingURL=distributor-distributor-module.js.map