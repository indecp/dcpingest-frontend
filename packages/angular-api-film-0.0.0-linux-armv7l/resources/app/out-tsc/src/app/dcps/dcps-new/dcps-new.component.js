var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatPaginator, MatSort } from '@angular/material';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { Dcp } from '../dcps';
import { DcpsService } from '../dcps.service';
import { MoviesService } from '../../movies/movies.service';
import { MoviesDataSource } from '../../movies/movies.datasource';
var DcpsNewComponent = /** @class */ (function () {
    function DcpsNewComponent(formBuilder, dcpsService, http, movieService) {
        this.formBuilder = formBuilder;
        this.dcpsService = dcpsService;
        this.http = http;
        this.movieService = movieService;
        this.CtrlTitle = new FormControl();
        this.nbMovies = 80;
        this.TypeDCP = ['FTR', 'TLR'];
        this.formData = new FormData();
        this.signDcps = this.createFromGroup();
    }
    DcpsNewComponent.prototype.createFromGroup = function () {
        return new FormGroup({
            name: new FormControl(),
            contentkind: new FormControl(),
            size: new FormControl(),
            moviesList: new FormControl(),
        });
    };
    ; //Fin createFromGroup
    DcpsNewComponent.prototype.createFormGroupWithBuilderAndModel = function (formBuilder) {
        return formBuilder.group({
            DataDCP: formBuilder.group(new Dcp()),
        });
    }; //FIN   createFormGroupWithBuilderAndModel
    DcpsNewComponent.prototype.onSubmit = function () {
        var _this = this;
        this.dcpsService.addDcp(this.signDcps.value)
            .subscribe({ complete: function () { return _this.formData; } });
    }; //FIN onSubmit
    DcpsNewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataSourceMovie = new MoviesDataSource(this.movieService);
        this.dataSourceMovie.loadMovies('', 'asc', 1, 50);
        this.dataSourceMovie.moviesTotal.subscribe(function (total) { _this.nbMovies = total; });
    };
    DcpsNewComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.signDcps
            .get('moviesList')
            .valueChanges
            .pipe(debounceTime(150), distinctUntilChanged(), tap(function () {
            _this.loadMoviesPage();
        }))
            .subscribe();
    };
    DcpsNewComponent.prototype.loadMoviesPage = function () {
        this.dataSourceMovie.loadMovies(this.signDcps.get('moviesList').value, 'asc', 1, 50);
    };
    __decorate([
        ViewChild('input'),
        __metadata("design:type", ElementRef)
    ], DcpsNewComponent.prototype, "input", void 0);
    __decorate([
        ViewChild(MatSort),
        __metadata("design:type", MatSort)
    ], DcpsNewComponent.prototype, "sort", void 0);
    __decorate([
        ViewChild(MatPaginator),
        __metadata("design:type", MatPaginator)
    ], DcpsNewComponent.prototype, "paginator", void 0);
    DcpsNewComponent = __decorate([
        Component({
            selector: 'app-dcps-new',
            templateUrl: './dcps-new.component.html',
            styleUrls: ['./dcps-new.component.css']
        }),
        __metadata("design:paramtypes", [FormBuilder,
            DcpsService,
            HttpClient,
            MoviesService])
    ], DcpsNewComponent);
    return DcpsNewComponent;
}()); //Fin
export { DcpsNewComponent };
//# sourceMappingURL=dcps-new.component.js.map