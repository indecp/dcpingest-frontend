var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from '../message.service';
var httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
var DcpsService = /** @class */ (function () {
    function DcpsService(http, message_Service) {
        this.http = http;
        this.message_Service = message_Service;
        this.DcpsUrl = 'http://localhost:5000/ingestapi/dcps';
    }
    DcpsService.prototype.findDcps = function (filter, sortOrder, pageNumber, pageSize) {
        if (filter === void 0) { filter = ''; }
        if (sortOrder === void 0) { sortOrder = 'asc'; }
        if (pageNumber === void 0) { pageNumber = 1; }
        if (pageSize === void 0) { pageSize = 50; }
        return this.http.get(this.DcpsUrl, {
            params: new HttpParams()
                .set('filter', filter)
                .set('sortOrder', sortOrder)
                .set('pageNumber', pageNumber.toString())
                .set('pageSize', pageSize.toString())
        }).pipe(map(function (res) { return res; }));
    }; //Fin
    DcpsService.prototype.getDcps = function () {
        var _this = this;
        return this.http
            .get(this.DcpsUrl)
            .pipe(tap(function (dcps) { return _this.log('fetched dcps'); }), catchError(this.handleError('getDcps', [])));
    }; //Fin getDcps
    DcpsService.prototype.getDcp = function (id) {
        var _this = this;
        var url = this.DcpsUrl + "/" + id;
        return this.http.get(url)
            .pipe(tap(function (_) { return _this.log("fetched dcps id=" + id); }), catchError(this.handleError("getDcps id=" + id)));
    }; //Fin getDcp
    DcpsService.prototype.getShortCheck = function () {
        var _this = this;
        return this.http
            .get(this.DcpsUrl)
            .pipe(tap(function (check) { return _this.log('fetched check'); }), catchError(this.handleError('getShortCheckStatus', [])));
    }; //Fin getShortCheck
    DcpsService.prototype.getShortCheckStatus = function (id) {
        var _this = this;
        var url = this.DcpsUrl + "/" + id + "/short_check_status";
        return this.http.get(url)
            .pipe(tap(function (_) { return _this.log("fetched check id=" + id); }), catchError(this.handleError("getShortCheckStatus id=" + id)));
    }; //Fin getShortCheckStatus
    DcpsService.prototype.getLongCheck = function () {
        var _this = this;
        return this.http
            .get(this.DcpsUrl)
            .pipe(tap(function (check_long) { return _this.log('fetched check_long'); }), catchError(this.handleError('getLongCheckStatus', [])));
    }; //Fin getLongCheck
    /* getLongCheckStatus(id : number):Observable<Dcp>{
        const url = `${this.DcpsUrl}/${id}/hash_verification_state_name`;
        return this.http.get<Dcp>(url)
        .pipe(tap(_=>this.log(`fetched check_long id=${id}`)),
        catchError(this.handleError<Dcp>(`getLongCheckStatus id=${id}`)));
   }//Fin getLongCheckStatus
 */
    DcpsService.prototype.getDateCheck = function () {
        var _this = this;
        return this.http
            .get(this.DcpsUrl)
            .pipe(tap(function (check_date) { return _this.log('fetched check_date'); }), catchError(this.handleError('getDateCheckStatus', [])));
    }; //Fin getLongCheck
    DcpsService.prototype.getDateCheckStatus = function (id) {
        var _this = this;
        var url = this.DcpsUrl + "/" + id + "/hash_verification_date";
        return this.http.get(url)
            .pipe(tap(function (_) { return _this.log("fetched check_date id=" + id); }), catchError(this.handleError("getDateCheckStatus id=" + id)));
    }; //Fin getDateCheckStatus
    DcpsService.prototype.getLongCheckStatus = function (id) {
        var _this = this;
        var url = this.DcpsUrl + "/" + id + "/do_check_long";
        return this.http.post(url, '', httpOptions)
            .pipe(tap(function (_) { return _this.log("fetched check_long id=" + id); }), catchError(this.handleError("getLongCheckStatus id=" + id)));
    };
    DcpsService.prototype.getTorrent = function () {
        var _this = this;
        return this.http
            .get(this.DcpsUrl)
            .pipe(tap(function (torrent) { return _this.log('fetched torrent'); }), catchError(this.handleError('getTheTorrent', [])));
    }; //Fin getTorrent
    DcpsService.prototype.getTheTorrent = function (id) {
        var _this = this;
        var url = this.DcpsUrl + "/" + id + "/torrent_creation_state_name";
        return this.http.get(url)
            .pipe(tap(function (_) { return _this.log("fetched torrent id=" + id); }), catchError(this.handleError("getTheTorrent id=" + id)));
    }; //Fin getTheTorrent
    DcpsService.prototype.addDcp = function (dcps_s) {
        var _this = this;
        return this.http.post(this.DcpsUrl, dcps_s, httpOptions)
            .pipe(tap(function (dcps_s) { return _this.log("added dcps_s w/ id=" + dcps_s.id); }), catchError(this.handleError('addDcp')));
    }; //Fin addDcps
    DcpsService.prototype.deleteDcps = function (dcps_s) {
        var _this = this;
        var id = typeof dcps_s === 'number' ? dcps_s : dcps_s.id;
        var url = this.DcpsUrl + "/" + id;
        return this.http.delete(url, httpOptions)
            .pipe(tap(function (_) { return _this.log("delete dcps_s id=" + id); }), catchError(this.handleError('deleteDcps')));
    }; //Fin deleteDcps
    DcpsService.prototype.updateDcps = function (dcps_s) {
        var _this = this;
        return this.http.put(this.DcpsUrl, dcps_s, httpOptions)
            .pipe(tap(function (_) { return _this.log("updated dcps_s id=" + dcps_s.id); }), catchError(this.handleError('updateDcps')));
    }; //Fin updateDcps
    DcpsService.prototype.handleError = function (operation, result) {
        var _this = this;
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            console.error(error);
            _this.log(operation + " failed: " + error.message);
            return of(result);
        };
    }; //fin handleError
    DcpsService.prototype.log = function (message) {
        this.message_Service.add("DcpsService: " + message);
    }; //Fin log
    DcpsService = __decorate([
        Injectable({ providedIn: 'root' }),
        __metadata("design:paramtypes", [HttpClient,
            MessageService])
    ], DcpsService);
    return DcpsService;
}()); //Fin
export { DcpsService };
//# sourceMappingURL=dcps.service.js.map