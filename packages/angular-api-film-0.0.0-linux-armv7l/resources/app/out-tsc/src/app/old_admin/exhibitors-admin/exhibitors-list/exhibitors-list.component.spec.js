import { async, TestBed } from '@angular/core/testing';
import { ExhibitorsListComponent } from './exhibitors-list.component';
describe('ExhibitorsListComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [ExhibitorsListComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(ExhibitorsListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=exhibitors-list.component.spec.js.map