var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { merge } from "rxjs/observable/merge";
import { ROLE } from 'app/_models';
import { UserService } from 'app/_services';
import { DcpService } from '../dcps.service';
import { DcpsDataSource } from '../dcps.datasource';
var DcpsListComponent = /** @class */ (function () {
    function DcpsListComponent(dcpService, userService) {
        var _this = this;
        this.dcpService = dcpService;
        this.userService = userService;
        this.isLogged = false;
        this.nbDcps = 80;
        this.displayedColumns = ['id', 'name', 'delete'];
        this.userSubscription = this.userService.user$.subscribe(function (user) {
            _this.isLogged = user ? true : false;
            _this.user = user;
        });
    }
    DcpsListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataSource = new DcpsDataSource(this.dcpService);
        this.dataSource.loadDcps('', 'asc', 1, 50);
        this.dataSource.dcpsTotal.subscribe(function (total) { _this.nbDcps = total; });
    };
    DcpsListComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 1; });
        fromEvent(this.input.nativeElement, 'keyup')
            .pipe(debounceTime(150), distinctUntilChanged(), tap(function () {
            _this.paginator.pageIndex = 0;
            _this.loadDcpsPage();
        }))
            .subscribe();
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(tap(function () { return _this.loadDcpsPage(); }))
            .subscribe();
    };
    DcpsListComponent.prototype.loadDcpsPage = function () {
        this.dataSource.loadDcps(this.input.nativeElement.value, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
    };
    DcpsListComponent.prototype.getRole = function (user) {
        if (!user || !user.ID) {
            console.debug('No user');
            return;
        }
        return ROLE[user.role];
    };
    __decorate([
        ViewChild(MatPaginator),
        __metadata("design:type", MatPaginator)
    ], DcpsListComponent.prototype, "paginator", void 0);
    __decorate([
        ViewChild(MatSort),
        __metadata("design:type", MatSort)
    ], DcpsListComponent.prototype, "sort", void 0);
    __decorate([
        ViewChild('input'),
        __metadata("design:type", ElementRef)
    ], DcpsListComponent.prototype, "input", void 0);
    DcpsListComponent = __decorate([
        Component({
            selector: 'app-dcps-list',
            templateUrl: './dcps-list.component.html',
            styleUrls: ['./dcps-list.component.css']
        }),
        __metadata("design:paramtypes", [DcpService,
            UserService])
    ], DcpsListComponent);
    return DcpsListComponent;
}());
export { DcpsListComponent };
/*	delete(dcps : Dcp): void{
        //this.displayedColumns.filter(m => m !== movie);
        this.dcpService.deleteDcps(dcps).subscribe();
    }//Fin delete

}//Fin
*/ 
//# sourceMappingURL=dcps-list.component.js.map