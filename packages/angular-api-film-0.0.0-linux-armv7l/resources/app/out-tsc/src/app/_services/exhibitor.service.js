var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { MessageService } from '../message.service';
var httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
var ExhibitorsService = /** @class */ (function () {
    function ExhibitorsService(http, message_Service) {
        this.http = http;
        this.message_Service = message_Service;
        this.ExhibitorsUrl = 'http://localhost:5000/ingestapi/exhibitors';
    }
    ExhibitorsService.prototype.getExhibitors = function () {
        var _this = this;
        return this.http
            .get(this.ExhibitorsUrl)
            .pipe(tap(function (Exhibitor) { return _this.log('fetched Exhibitor'); }), catchError(this.handleError('getExhibitors', [])));
    }; //Fin GetExhibitors
    ExhibitorsService.prototype.getExhibitorsContent = function (id) {
        var _this = this;
        var url = this.ExhibitorsUrl + "/" + id;
        return this.http.get(url)
            .pipe(tap(function (_) { return _this.log("fetched Exhibitors_s id=" + id); }), catchError(this.handleError("getExhibitors id=" + id)));
    }; //Fin getFilm
    ExhibitorsService.prototype.searchExhibitors = function (term) {
        var _this = this;
        if (!term.trim()) {
            return of([]);
        }
        return this.http.get(this.ExhibitorsUrl + "/?name=" + term)
            .pipe(tap(function (_) { return _this.log("found Exhibitor matching \"$(term)\""); }), catchError(this.handleError('searchExhibitors', [])));
    }; //Fin searchExhibitorss
    ExhibitorsService.prototype.addExhibitors = function (Exhibitors_s) {
        var _this = this;
        return this.http.post(this.ExhibitorsUrl, Exhibitors_s, httpOptions)
            .pipe(tap(function (Exhibitors_s) { return _this.log("added Exhibitors_s w/ id=" + Exhibitors_s.id); }), catchError(this.handleError('addExhibitors')));
    }; //Fin addExhibitors
    ExhibitorsService.prototype.deleteExhibitors = function (Exhibitors_s) {
        var _this = this;
        var id = typeof Exhibitors_s === 'number' ? Exhibitors_s : Exhibitors_s.id;
        var url = this.ExhibitorsUrl + "/" + id;
        return this.http.delete(url, httpOptions)
            .pipe(tap(function (_) { return _this.log("delete Exhibitors_s id=" + id); }), catchError(this.handleError('deleteExhibitors')));
    }; //Fin deleteExhibitors
    ExhibitorsService.prototype.updateExhibitors = function (Exhibitors_s) {
        var _this = this;
        return this.http.put(this.ExhibitorsUrl, Exhibitors_s, httpOptions)
            .pipe(tap(function (_) { return _this.log("updated Exhibitors_s id=" + Exhibitors_s.id); }), catchError(this.handleError('updateExhibitors')));
    }; //Fin updateExhibitorss
    ExhibitorsService.prototype.handleError = function (operation, result) {
        var _this = this;
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            console.error(error);
            _this.log(operation + " failed: " + error.message);
            return of(result);
        };
    }; //fin handleError
    ExhibitorsService.prototype.log = function (message) {
        this.message_Service.add('ExhibitorsService: ${message}');
    }; //Fin log
    ExhibitorsService = __decorate([
        Injectable({ providedIn: 'root' }),
        __metadata("design:paramtypes", [HttpClient,
            MessageService])
    ], ExhibitorsService);
    return ExhibitorsService;
}()); //Fin
export { ExhibitorsService };
//# sourceMappingURL=exhibitor.service.js.map