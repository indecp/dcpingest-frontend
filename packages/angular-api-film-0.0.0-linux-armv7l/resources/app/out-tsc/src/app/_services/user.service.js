var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'app/_models';
import { ReplaySubject } from 'rxjs/ReplaySubject';
var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
        this.user$ = new ReplaySubject(1);
        this.isLogged$ = new ReplaySubject(1);
    }
    UserService.prototype.register = function (user) {
        return this.http.post('http://127.0.0.1:500/users/register', user);
    };
    UserService.prototype.delete = function (id) {
        return this.http.delete("http://127.0.0.1:500/users/" + id);
    };
    UserService.prototype.setAccount = function (user, store) {
        if (store === void 0) { store = true; }
        if (user && !(user instanceof User)) {
            user = new User(user);
        }
        this.user = user;
        this.user$.next(this.user);
    };
    UserService.prototype.getAccount = function () {
        return this.user;
    };
    UserService.prototype.get = function (userId) {
        var url = "http://127.0.0.1:5000/ingestapi/login/" + userId;
        return this.http.get(url, {})
            .toPromise()
            .then(function (data) {
            if (!data) {
                return null;
            }
            return data;
        });
    };
    UserService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient])
    ], UserService);
    return UserService;
}());
export { UserService };
//# sourceMappingURL=user.service.js.map