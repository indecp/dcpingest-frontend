var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DistributorHomeComponent } from './distributor-home/distributor-home.component';
import { DistributorComponent } from './distributor/distributor.component';
import { MovieDetailComponent, MovieListComponent, MovieNewComponent } from './movie/';
import { DcpDetailComponent, DcpListComponent, DcpNewComponent } from './dcp/';
import { MovieDetailResolverService } from './movie-detail-resolver.service';
import { DcpDetailResolverService } from './dcp/dcp-detail-resolver.service';
var distributorRoutes = [
    {
        path: '',
        component: DistributorComponent,
        children: [
            {
                path: 'dcp',
                component: DcpListComponent,
                data: { animation: 'movies' },
            },
            { path: 'dcp/new',
                component: DcpNewComponent,
                data: { animation: 'movie' }
            },
            { path: 'dcp/:id',
                component: DcpDetailComponent,
                resolve: {
                    dcp: DcpDetailResolverService
                },
                data: { animation: 'movie' }
            },
            {
                path: 'film',
                component: MovieListComponent,
                data: { animation: 'movies' }
            },
            { path: 'film/new',
                component: MovieNewComponent,
            },
            { path: 'film/:id',
                component: MovieDetailComponent,
                resolve: {
                    movie: MovieDetailResolverService
                },
                data: { animation: 'movie' }
            },
            {
                path: '',
                component: DistributorHomeComponent
            }
        ]
    }
];
/*
const distributorRoutes: Routes = [
  {
    path: '',
    component: DistributorComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        children: [
          { path: 'film',  component: DistributorMovieListComponent, data: { animation: 'movies' } },
          { path: 'film/:id',
            component:  MovieDetailComponent,
            resolve: {
              movie: MovieDetailResolverService
            },
            data: { animation: 'movie' }
          },
        ]
      }
    ]
  },
];
*/
var DistributorRoutingModule = /** @class */ (function () {
    function DistributorRoutingModule() {
    }
    DistributorRoutingModule = __decorate([
        NgModule({
            imports: [
                RouterModule.forChild(distributorRoutes)
            ],
            exports: [
                RouterModule
            ]
        })
    ], DistributorRoutingModule);
    return DistributorRoutingModule;
}());
export { DistributorRoutingModule };
//# sourceMappingURL=distributor-routing.module.js.map