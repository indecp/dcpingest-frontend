var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AuthenticationService } from '../../user/auth.service';
import { UserService } from '../../user/user.service';
var MoviesViewDistributorComponent = /** @class */ (function () {
    function MoviesViewDistributorComponent(authenticationService, userService) {
        var _this = this;
        this.authenticationService = authenticationService;
        this.userService = userService;
        this.currentUserSubscription = this.authenticationService.currentUser.subscribe(function (user) {
            _this.currentUser = user;
        });
    }
    MoviesViewDistributorComponent.prototype.ngOnInit = function () {
        console.log('ngOnInit MoviesViewDistributorComponent: ', this.currentUser);
    }; //Fin ngOnInit
    MoviesViewDistributorComponent.prototype.ngOnDestroy = function () {
        this.currentUserSubscription.unsubscribe();
    };
    MoviesViewDistributorComponent = __decorate([
        Component({
            selector: 'app-movies-view-distributor',
            templateUrl: './movies-view-distributor.component.html',
            styleUrls: ['./movies-view-distributor.component.css']
        }),
        __metadata("design:paramtypes", [AuthenticationService,
            UserService])
    ], MoviesViewDistributorComponent);
    return MoviesViewDistributorComponent;
}());
export { MoviesViewDistributorComponent };
//# sourceMappingURL=movies-view-distributor.component.js.map