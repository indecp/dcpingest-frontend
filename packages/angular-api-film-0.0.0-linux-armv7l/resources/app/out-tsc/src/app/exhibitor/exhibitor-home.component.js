var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { Location } from '@angular/common';
import { User } from '../user/user.model';
import { ExhibitorsService } from '../_services';
var ExhibitorHomeComponent = /** @class */ (function () {
    function ExhibitorHomeComponent(exhibitorsService, location) {
        this.exhibitorsService = exhibitorsService;
        this.location = location;
    }
    ExhibitorHomeComponent.prototype.ngOnInit = function () {
        this.getExhibitorsContent();
    }; //Fin ngOnInit
    ExhibitorHomeComponent.prototype.getExhibitorsContent = function () {
        var _this = this;
        this.exhibitorsService.getExhibitorsContent(this.exhibitorid)
            .subscribe(function (exhibitor) { return _this.exhibitor = exhibitor; });
    }; //Fin getDistributorsContent
    ExhibitorHomeComponent.prototype.goBack = function () {
        this.location.back();
    }; //Fin goBack
    ExhibitorHomeComponent.prototype.save = function () {
        var _this = this;
        this.exhibitorsService.updateExhibitors(this.exhibitor)
            .subscribe(function () { return _this.goBack(); });
    }; //Fin save
    __decorate([
        Input(),
        __metadata("design:type", Number)
    ], ExhibitorHomeComponent.prototype, "exhibitorid", void 0);
    __decorate([
        Input(),
        __metadata("design:type", User)
    ], ExhibitorHomeComponent.prototype, "currentUser", void 0);
    ExhibitorHomeComponent = __decorate([
        Component({
            selector: 'app-exhibitor-home',
            templateUrl: './exhibitor-home.component.html',
            styleUrls: ['./exhibitor-home.component.css']
        }),
        __metadata("design:paramtypes", [ExhibitorsService,
            Location])
    ], ExhibitorHomeComponent);
    return ExhibitorHomeComponent;
}()); //Fin
export { ExhibitorHomeComponent };
//# sourceMappingURL=exhibitor-home.component.js.map