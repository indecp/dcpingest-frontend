var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { MovieService } from 'app/_services';
import { Movie, Distributor } from 'app/_models';
var MovieDetailComponent = /** @class */ (function () {
    function MovieDetailComponent(route, movieService, location, router) {
        this.route = route;
        this.movieService = movieService;
        this.location = location;
        this.router = router;
        this.nbMovies = 80;
    }
    MovieDetailComponent.prototype.ngOnInit = function () {
        this.getFilm();
    }; //Fin ngOnInit
    MovieDetailComponent.prototype.getFilm = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.movieService.getMovie(id)
            .subscribe(function (movie) { return _this.movie = movie; });
    }; //Fin getFilm
    MovieDetailComponent.prototype.goBack = function () {
        this.location.back();
    }; //Fin goBack
    __decorate([
        Input(),
        __metadata("design:type", Distributor)
    ], MovieDetailComponent.prototype, "distributor", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Movie)
    ], MovieDetailComponent.prototype, "movie", void 0);
    MovieDetailComponent = __decorate([
        Component({
            selector: 'app-movie-detail',
            templateUrl: './movie-detail.component.html',
            styleUrls: ['./movie-detail.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute,
            MovieService,
            Location,
            Router])
    ], MovieDetailComponent);
    return MovieDetailComponent;
}()); //END
export { MovieDetailComponent };
//# sourceMappingURL=movie-detail.component.js.map