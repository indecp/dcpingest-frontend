var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DistributorsService } from '../distributors.service';
import { Location } from '@angular/common';
import { ROLE } from '../../../user/user.model';
import { UserService } from '../../../user/user.service';
import { ActivatedRoute, Router } from '@angular/router';
var EditDistributorsComponent = /** @class */ (function () {
    function EditDistributorsComponent(formBuilder, distributorsService, http, location, userService, route, router) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.distributorsService = distributorsService;
        this.http = http;
        this.location = location;
        this.userService = userService;
        this.route = route;
        this.router = router;
        this.Partenaire = ['OUI', 'NON'];
        this.isLogged = false;
        this.distributorsForm = this.createFromGroup();
        this.userSubscription = this.userService.user$.subscribe(function (user) {
            _this.isLogged = user ? true : false;
            _this.user = user;
        });
    }
    EditDistributorsComponent.prototype.createFromGroup = function () {
        return new FormGroup({
            id: new FormControl(),
            distributorname: new FormControl(),
            partners: new FormControl(),
            mail: new FormControl(),
            user_contact: new FormControl(),
            number_contact: new FormControl(),
            CNC_Code: new FormControl(),
            user_Id: new FormControl(),
            mail_as_user_id: new FormControl()
        });
    };
    ; //Fin createFromGroup
    EditDistributorsComponent.prototype.goBack = function () {
        this.location.back();
    };
    EditDistributorsComponent.prototype.onSubmit = function () {
        var _this = this;
        this.distributorsService
            .updateDistributors(this.distributorsForm.value)
            .subscribe(function () { return _this.goBack(); });
    };
    EditDistributorsComponent.prototype.ngOnInit = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.distributorsService.getDistributorsContent(id)
            .subscribe(function (distrib) {
            _this.distributorsForm.patchValue(distrib);
        });
    };
    EditDistributorsComponent.prototype.getRole = function (user) {
        if (!user || !user.ID) {
            console.debug('No user');
            return;
        }
        return ROLE[user.role];
    };
    EditDistributorsComponent = __decorate([
        Component({
            selector: 'app-edit-distributors',
            templateUrl: './edit-distributors.component.html',
            styleUrls: ['./edit-distributors.component.css']
        }),
        __metadata("design:paramtypes", [FormBuilder,
            DistributorsService,
            HttpClient,
            Location,
            UserService,
            ActivatedRoute,
            Router])
    ], EditDistributorsComponent);
    return EditDistributorsComponent;
}());
export { EditDistributorsComponent };
//# sourceMappingURL=edit-distributors.component.js.map