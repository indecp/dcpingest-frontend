var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MoviesService } from '../admin/movies-admin/movies.service';
var NavigationComponent = /** @class */ (function () {
    function NavigationComponent(movies_Service) {
        this.movies_Service = movies_Service;
        this.MyMov = new FormControl();
        this.onSelectedOption = new EventEmitter();
    }
    NavigationComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], NavigationComponent.prototype, "onSelectedOption", void 0);
    NavigationComponent = __decorate([
        Component({
            selector: 'app-navigation',
            templateUrl: './navigation.component.html',
            styleUrls: ['./navigation.component.css']
        }),
        __metadata("design:paramtypes", [MoviesService])
    ], NavigationComponent);
    return NavigationComponent;
}());
export { NavigationComponent };
//# sourceMappingURL=navigation.component.js.map