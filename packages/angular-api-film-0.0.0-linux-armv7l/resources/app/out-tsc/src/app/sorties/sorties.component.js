var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MoviesService } from '../movies.service';
var SortiesComponent = /** @class */ (function () {
    function SortiesComponent(movies_Service) {
        this.movies_Service = movies_Service;
        this.movie_s = [];
    }
    SortiesComponent.prototype.ngOnInit = function () {
        this.getMovies();
    };
    SortiesComponent.prototype.getMovies = function () {
        var _this = this;
        this.movies_Service.getMovies()
            .subscribe(function (movie_s) { return _this.movie_s = movie_s; });
    };
    SortiesComponent = __decorate([
        Component({
            selector: 'app-sorties',
            templateUrl: './sorties.component.html',
            styleUrls: ['./sorties.component.css']
        }),
        __metadata("design:paramtypes", [MoviesService])
    ], SortiesComponent);
    return SortiesComponent;
}());
export { SortiesComponent };
//# sourceMappingURL=sorties.component.js.map