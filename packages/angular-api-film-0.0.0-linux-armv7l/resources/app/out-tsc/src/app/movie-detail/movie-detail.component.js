var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, ViewChild } from '@angular/core';
import { Movie } from '../movies';
import { MoviesService } from '../movies.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { MOVIES_List } from '../mock-movies';
var MovieDetailComponent = /** @class */ (function () {
    function MovieDetailComponent(route, movies_Service, location) {
        this.route = route;
        this.movies_Service = movies_Service;
        this.location = location;
        //@Input() dcps: DCPS;
        this.displayedColumns = ['id_dcps', 'name_dcps', 'contentkind', 'size'];
        this.dataSource = new MatTableDataSource(MOVIES_List);
    }
    MovieDetailComponent.prototype.ngOnInit = function () {
        this.getFilm();
        this.dataSource.paginator = this.paginator;
    };
    MovieDetailComponent.prototype.getFilm = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.movies_Service.getFilm(id)
            .subscribe(function (movie) { return _this.movie = movie; });
    };
    MovieDetailComponent.prototype.goBack = function () {
        this.location.back();
    };
    MovieDetailComponent.prototype.save = function () {
        var _this = this;
        this.movies_Service.updateMovies(this.movie)
            .subscribe(function () { return _this.goBack(); });
    };
    __decorate([
        Input(),
        __metadata("design:type", Movie)
    ], MovieDetailComponent.prototype, "movie", void 0);
    __decorate([
        ViewChild(MatPaginator),
        __metadata("design:type", MatPaginator)
    ], MovieDetailComponent.prototype, "paginator", void 0);
    MovieDetailComponent = __decorate([
        Component({
            selector: 'app-movie-detail',
            templateUrl: './movie-detail.component.html',
            styleUrls: ['./movie-detail.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute,
            MoviesService,
            Location])
    ], MovieDetailComponent);
    return MovieDetailComponent;
}());
export { MovieDetailComponent };
//# sourceMappingURL=movie-detail.component.js.map