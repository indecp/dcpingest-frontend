var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component } from '@angular/core';
import { TableDcpDataSource } from './table-dcp-datasource';
//import { MOVIES_List } from '../mock-movies';
var TableDcpComponent = /** @class */ (function () {
    function TableDcpComponent() {
        /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
        //displayedColumns = ['id', 'name'];
        this.displayedColumns = ['id', 'name'];
        this.dataSource = new TableDcpDataSource(MOVIES_List);
    }
    TableDcpComponent.prototype.ngOnInit = function () {
        //this.dataSource = new TableDcpDataSource(this.paginator, this.sort, this.movies_Service);
        this.dataSource.paginator = this.paginator;
    };
    TableDcpComponent = __decorate([
        Component({
            selector: 'app-table-dcp',
            templateUrl: './table-dcp.component.html',
            styleUrls: ['./table-dcp.component.css']
        })
    ], TableDcpComponent);
    return TableDcpComponent;
}());
export { TableDcpComponent };
var MOVIES_List = [
    { id: 11, name: 'Vertigo', synop: 'TEST' },
    { id: 12, name: 'Pulp Fiction', synop: 'TEST' },
    { id: 13, name: 'Reservoirs Dogs', synop: 'TEST' },
    { id: 14, name: 'STAR WARS EPISODE IV', synop: 'TEST' },
    { id: 15, name: 'Fours Lions', synop: 'TEST' },
    { id: 16, name: 'Gouffre au chimère', synop: 'TEST' },
    { id: 17, name: 'Le quatuor à cornes', synop: 'TEST' },
    { id: 18, name: 'Sauvage', synop: 'TEST' },
    { id: 19, name: 'Rafiki', synop: 'TEST' }
];
//# sourceMappingURL=table-dcp.component.js.map