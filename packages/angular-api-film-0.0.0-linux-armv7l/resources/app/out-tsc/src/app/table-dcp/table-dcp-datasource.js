var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { DataSource } from '@angular/cdk/collections';
import { map } from 'rxjs/operators';
import { of as observableOf, merge } from 'rxjs';
//import { MOVIES_List } from '../mock-movies';
//import { Movie } from '../movies';
// TODO: Replace this with your own data model type
// TODO: replace this with real data from your application
//const MOVIES_List: TableDcpItem[] = [
/*  {id2: 1, name2: 'Hydrogen'},
  {id2: 2, name2: 'Helium'},
  {id2: 3, name2: 'Lithium'},
  {id2: 4, name2: 'Beryllium'},
  {id2: 5, name2: 'Boron'},
  {id2: 6, name2: 'Carbon'},
  {id2: 7, name2: 'Nitrogen'},
  {id2: 8, name2: 'Oxygen'},
  {id2: 9, name2: 'Fluorine'},
  {id2: 10, name2: 'Neon'},
  {id2: 11, name2: 'Sodium'},
  {id2: 12, name2: 'Magnesium'},
  {id2: 13, name2: 'Aluminum'},
  {id2: 14, name2: 'Silicon'},
  {id2: 15, name2: 'Phosphorus'},
  {id2: 16, name2: 'Sulfur'},
  {id2: 17, name2: 'Chlorine'},
  {id2: 18, name2: 'Argon'},
  {id2: 19, name2: 'Potassium'},
  {id2: 20, name2: 'Calcium'},
];*/
var Movie = /** @class */ (function () {
    function Movie() {
    }
    return Movie;
}());
export { Movie };
export var MOVIES_List = [
    { id: 11, name: 'Vertigo', synop: 'TEST' },
    { id: 12, name: 'Pulp Fiction', synop: 'TEST' },
    { id: 13, name: 'Reservoirs Dogs', synop: 'TEST' },
    { id: 14, name: 'STAR WARS EPISODE IV', synop: 'TEST' },
    { id: 15, name: 'Fours Lions', synop: 'TEST' },
    { id: 16, name: 'Gouffre au chimère', synop: 'TEST' },
    { id: 17, name: 'Le quatuor à cornes', synop: 'TEST' },
    { id: 18, name: 'Sauvage', synop: 'TEST' },
    { id: 19, name: 'Rafiki', synop: 'TEST' }
];
/**
 * Data source for the TableDcp view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
var TableDcpDataSource = /** @class */ (function (_super) {
    __extends(TableDcpDataSource, _super);
    function TableDcpDataSource(paginator, sort, movies_Service) {
        var _this = _super.call(this) || this;
        _this.paginator = paginator;
        _this.sort = sort;
        _this.movies_Service = movies_Service;
        _this.data = MOVIES_List;
        return _this;
    }
    /**
     * Connect this data source to the table. The table will only update when
     * the returned stream emits new items.
     * @returns A stream of the items to be rendered.
     */
    TableDcpDataSource.prototype.connect = function () {
        var _this = this;
        // Combine everything that affects the rendered data into one update
        // stream for the data-table to consume.
        var dataMutations = [
            observableOf(this.data),
            this.paginator.page,
            this.sort.sortChange,
            this.movies_Service.getMovies
        ];
        // Set the paginators length
        this.paginator.length = this.data.length;
        return merge.apply(void 0, dataMutations).pipe(map(function () {
            return _this.getPagedData(_this.getSortedData(_this.data.slice()));
        }));
    };
    /**
     *  Called when the table is being destroyed. Use this function, to clean up
     * any open connections or free any held resources that were set up during connect.
     */
    TableDcpDataSource.prototype.disconnect = function () { };
    /**
     * Paginate the data (client-side). If you're using server-side pagination,
     * this would be replaced by requesting the appropriate data from the server.
     */
    TableDcpDataSource.prototype.getPagedData = function (data) {
        var startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        return data.splice(startIndex, this.paginator.pageSize);
    };
    TableDcpDataSource.prototype.getMovies = function () {
        var _this = this;
        this.movies_Service.getMovies()
            .subscribe(function (movies) { return _this.movies$ = movies; });
    };
    /**
     * Sort the data (client-side). If you're using server-side sorting,
     * this would be replaced by requesting the appropriate data from the server.
     */
    TableDcpDataSource.prototype.getSortedData = function (data) {
        var _this = this;
        if (!this.sort.active || this.sort.direction === '') {
            return data;
        }
        return data.sort(function (a, b) {
            var isAsc = _this.sort.direction === 'asc';
            switch (_this.sort.active) {
                case 'name': return compare(a.name, b.name, isAsc);
                case 'id': return compare(+a.id, +b.id, isAsc);
                default: return 0;
            }
        });
    };
    return TableDcpDataSource;
}(DataSource));
export { TableDcpDataSource };
/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
//# sourceMappingURL=table-dcp-datasource.js.map