import { fakeAsync, TestBed } from '@angular/core/testing';
import { TableDcpComponent } from './table-dcp.component';
describe('TableDcpComponent', function () {
    var component;
    var fixture;
    beforeEach(fakeAsync(function () {
        TestBed.configureTestingModule({
            declarations: [TableDcpComponent]
        })
            .compileComponents();
        fixture = TestBed.createComponent(TableDcpComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));
    it('should compile', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=table-dcp.component.spec.js.map