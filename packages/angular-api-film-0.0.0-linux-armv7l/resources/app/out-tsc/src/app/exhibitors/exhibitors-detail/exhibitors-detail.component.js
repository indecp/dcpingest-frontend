var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Exhibitors } from '../exhibitors';
import { ExhibitorsService } from '../exhibitors.service';
var ExhibitorsDetailComponent = /** @class */ (function () {
    function ExhibitorsDetailComponent(route, Exhibitors_Service, location) {
        this.route = route;
        this.Exhibitors_Service = Exhibitors_Service;
        this.location = location;
        this.displayedColumns = ['name', 'CNC_Code_autorised',
            'log', 'mail', 'number_contact',
            'name_contact', 'address_contact',
            'ZIP_Code', 'town', 'department',
            'Nb_screen', 'ip_tinc', 'free_space'];
        this.dataSource = new MatTableDataSource();
    }
    ExhibitorsDetailComponent.prototype.ngOnInit = function () {
        this.getExhibitorsContent();
        //this.dataSource.paginator = this.paginator;
    }; //Fin ngOnInit
    ExhibitorsDetailComponent.prototype.getExhibitorsContent = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.Exhibitors_Service.getExhibitorsContent(id)
            .subscribe(function (exhib) { return _this.exhibitors = exhib; });
    }; //Fin getFilm
    ExhibitorsDetailComponent.prototype.goBack = function () {
        this.location.back();
    }; //Fin goBack
    __decorate([
        Input(),
        __metadata("design:type", Exhibitors)
    ], ExhibitorsDetailComponent.prototype, "exhibitors", void 0);
    __decorate([
        ViewChild(MatPaginator),
        __metadata("design:type", MatPaginator)
    ], ExhibitorsDetailComponent.prototype, "paginator", void 0);
    ExhibitorsDetailComponent = __decorate([
        Component({
            selector: 'app-exhibitors-detail',
            templateUrl: './exhibitors-detail.component.html',
            styleUrls: ['./exhibitors-detail.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute,
            ExhibitorsService,
            Location])
    ], ExhibitorsDetailComponent);
    return ExhibitorsDetailComponent;
}()); //Fin
export { ExhibitorsDetailComponent };
//# sourceMappingURL=exhibitors-detail.component.js.map