var Exhibitor = /** @class */ (function () {
    function Exhibitor() {
        this.name = '';
        this.valid = '';
        this.contact = '';
        this.iptinc = '';
        this.city = '';
        this.address = '';
    }
    return Exhibitor;
}());
export { Exhibitor };
//# sourceMappingURL=exhibitor.js.map