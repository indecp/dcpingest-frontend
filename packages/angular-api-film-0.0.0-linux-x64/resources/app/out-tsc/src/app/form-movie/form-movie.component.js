var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
var FormMovieComponent = /** @class */ (function () {
    function FormMovieComponent() {
    }
    FormMovieComponent.prototype.ngOnInit = function () {
        this.type_film = ['TLR', 'FTR'];
        this.signMovie = new FormGroup({
            name: new FormControl('', Validators.required),
            synop: new FormControl('', Validators.required)
        });
    };
    FormMovieComponent = __decorate([
        Component({
            selector: 'app-form-movie',
            templateUrl: './form-movie.component.html',
            styleUrls: ['./form-movie.component.css']
        })
    ], FormMovieComponent);
    return FormMovieComponent;
}());
export { FormMovieComponent };
//# sourceMappingURL=form-movie.component.js.map