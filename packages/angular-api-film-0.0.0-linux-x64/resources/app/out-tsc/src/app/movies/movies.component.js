//ANCIENNE VERSION SOUS LE NOM Table-dcp à ne pas supprimer
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/*import { Component, OnInit } from '@angular/core';
import { Movie } from '../movies';
import { MoviesService } from '../movies.service';

import { Observable} from 'rxjs';

import {MOVIES_List} from '../mock-movies';
@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})

export class MoviesComponent implements OnInit {

    movies$ : Movie[];


  constructor(private movies_Service: MoviesService) { }

  ngOnInit() {

    
    this.getMovies();

  }


    getMovies(): void {

        this.movies_Service.getMovies()
    .subscribe(movies => this.movies$ = movies);
    
  }

  add(name: string): void{
    name = name.trim();
    if(!name)
        {return;}
    this.movies_Service.addMovie({name} as Movie)
    .subscribe(para_film => {this.movies$.push(para_film);
    });
  }

  delete(para_film : Movie): void{
    this.movies$ = this.movies$.filter(m => m !== para_film );
    this.movies_Service.deleteMovie(para_film).subscribe();
  }
}

*/
//NOUVELLE VERSION
import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MOVIES_List } from '../mock-movies';
var MoviesComponent = /** @class */ (function () {
    function MoviesComponent() {
        this.displayedColumns = ['affiche', 'id', 'name'];
        this.dataSource = new MatTableDataSource(MOVIES_List);
    }
    MoviesComponent.prototype.ngOnInit = function () {
        this.dataSource.paginator = this.paginator;
    };
    __decorate([
        ViewChild(MatPaginator),
        __metadata("design:type", MatPaginator)
    ], MoviesComponent.prototype, "paginator", void 0);
    MoviesComponent = __decorate([
        Component({
            selector: 'app-movies',
            templateUrl: './movies.component.html',
            styleUrls: ['./movies.component.css']
        })
    ], MoviesComponent);
    return MoviesComponent;
}());
export { MoviesComponent };
//# sourceMappingURL=movies.component.js.map