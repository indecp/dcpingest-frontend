import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { catchError, finalize } from "rxjs/operators";
import { of } from "rxjs/observable/of";
var MoviesDataSource = /** @class */ (function () {
    function MoviesDataSource(moviesService) {
        this.moviesService = moviesService;
        this.moviesSubject = new BehaviorSubject([]);
        this.moviesTotalSubject = new BehaviorSubject(0);
        this.moviesTotal = this.moviesTotalSubject.asObservable();
        this.loadingSubject = new BehaviorSubject(false);
        this.loading$ = this.loadingSubject.asObservable();
    }
    MoviesDataSource.prototype.loadMovies = function (filter, sortDirection, pageIndex, pageSize) {
        var _this = this;
        this.loadingSubject.next(true);
        this.moviesService.findMovies(filter, sortDirection, pageIndex, pageSize).pipe(catchError(function () { return of([]); }), finalize(function () { return _this.loadingSubject.next(false); }))
            .subscribe(function (movies) {
            _this.moviesSubject.next(movies['payload']);
            _this.moviesTotalSubject.next(movies['total']);
        });
    };
    MoviesDataSource.prototype.connect = function (collectionViewer) {
        console.log("Connecting data source");
        return this.moviesSubject.asObservable();
    };
    MoviesDataSource.prototype.disconnect = function (collectionViewer) {
        this.moviesSubject.complete();
        this.moviesTotalSubject.complete();
        this.loadingSubject.complete();
    };
    return MoviesDataSource;
}());
export { MoviesDataSource };
//# sourceMappingURL=movies.datasource.js.map