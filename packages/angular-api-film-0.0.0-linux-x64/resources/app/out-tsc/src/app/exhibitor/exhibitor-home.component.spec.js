import { async, TestBed } from '@angular/core/testing';
import { ExhibitorsDetailComponent } from './exhibitors-detail.component';
describe('ExhibitorsDetailComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [ExhibitorsDetailComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(ExhibitorsDetailComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=exhibitor-home.component.spec.js.map