var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExhibitorRoutingModule } from './exhibitor-routing.module';
import { ExhibitorComponent } from './exhibitor/exhibitor.component';
import { MatToolbarModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatTableModule, MatPaginatorModule, MatSortModule, MatOptionModule, MatCheckboxModule, MatSelectModule, MatCardModule } from '@angular/material';
import { MovieListComponent, MovieDetailComponent } from './movie/';
import { ExhibitorHomeComponent } from './exhibitor-home/exhibitor-home.component';
var ExhibitorModule = /** @class */ (function () {
    function ExhibitorModule() {
    }
    ExhibitorModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                MatToolbarModule,
                MatButtonModule,
                MatInputModule,
                MatFormFieldModule,
                MatTableModule,
                MatPaginatorModule,
                ExhibitorRoutingModule,
                MatSortModule,
                MatOptionModule,
                MatCheckboxModule,
                MatSelectModule,
                MatCardModule
            ],
            declarations: [
                ExhibitorComponent,
                MovieListComponent,
                ExhibitorHomeComponent,
                MovieDetailComponent
            ]
        })
    ], ExhibitorModule);
    return ExhibitorModule;
}());
export { ExhibitorModule };
//# sourceMappingURL=exhibitor.module.js.map