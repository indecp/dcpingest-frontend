var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ExhibitorComponent } from './exhibitor/exhibitor.component';
import { ExhibitorHomeComponent } from './exhibitor-home/exhibitor-home.component';
import { MovieListComponent, MovieDetailComponent } from './movie/';
var routes = [
    {
        path: '',
        component: ExhibitorComponent,
        children: [
            {
                path: 'film',
                component: MovieListComponent,
                data: { animation: 'movie' }
            },
            {
                path: 'film/:id',
                component: MovieDetailComponent,
                data: { animation: 'movie' }
            },
            {
                path: '',
                component: ExhibitorHomeComponent,
            },
        ]
    }
];
var ExhibitorRoutingModule = /** @class */ (function () {
    function ExhibitorRoutingModule() {
    }
    ExhibitorRoutingModule = __decorate([
        NgModule({
            imports: [RouterModule.forChild(routes)],
            exports: [RouterModule]
        })
    ], ExhibitorRoutingModule);
    return ExhibitorRoutingModule;
}());
export { ExhibitorRoutingModule };
//# sourceMappingURL=exhibitor-routing.module.js.map