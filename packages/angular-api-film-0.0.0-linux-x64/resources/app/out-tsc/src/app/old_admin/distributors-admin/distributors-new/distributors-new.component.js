var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DistributorsService } from '../distributors.service';
import { DISTRIBUTORS } from '../distributors';
import { ROLE } from 'app/_models';
import { UserService } from 'app/_services';
import { ActivatedRoute, Router } from '@angular/router';
var DistributorsNewComponent = /** @class */ (function () {
    function DistributorsNewComponent(formBuilder, distrib_Service, http, userService, route, router) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.distrib_Service = distrib_Service;
        this.http = http;
        this.userService = userService;
        this.route = route;
        this.router = router;
        this.Partenaire = ['OUI', 'NON'];
        this.dataSource = new FormData();
        this.isLogged = false;
        this.signDistrib = this.createFromGroup();
        this.userSubscription = this.userService.user$.subscribe(function (user) {
            _this.isLogged = user ? true : false;
            _this.user = user;
        });
    }
    DistributorsNewComponent.prototype.createFromGroup = function () {
        return new FormGroup({
            name: new FormControl(),
            user_contact: new FormControl(),
            partners: new FormControl(),
            number_contact: new FormControl(),
            mail: new FormControl(),
            CNC_Code: new FormControl()
        });
    };
    ; //Fin createFromGroup
    DistributorsNewComponent.prototype.createFormGroupWithBuilderAndModel = function (formBuilder) {
        return formBuilder.group({
            DataDistrib: formBuilder.group(new DISTRIBUTORS()),
        });
    }; //FIN   createFormGroupWithBuilderAndModel
    DistributorsNewComponent.prototype.ngOnInit = function () { };
    DistributorsNewComponent.prototype.onSubmit = function () {
        var _this = this;
        this.distrib_Service.addDistributors(this.signDistrib.value)
            .subscribe({ complete: function () { return _this.dataSource; } });
    };
    DistributorsNewComponent.prototype.getRole = function (user) {
        if (!user || !user.ID) {
            console.debug('No user');
            return;
        }
        return ROLE[user.role];
    };
    DistributorsNewComponent = __decorate([
        Component({
            selector: 'app-distributors-new',
            templateUrl: './distributors-new.component.html',
            styleUrls: ['./distributors-new.component.css']
        }),
        __metadata("design:paramtypes", [FormBuilder,
            DistributorsService,
            HttpClient,
            UserService,
            ActivatedRoute,
            Router])
    ], DistributorsNewComponent);
    return DistributorsNewComponent;
}()); //Fin
export { DistributorsNewComponent };
//# sourceMappingURL=distributors-new.component.js.map