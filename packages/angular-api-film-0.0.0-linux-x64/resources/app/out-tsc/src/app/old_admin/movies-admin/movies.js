var Movie = /** @class */ (function () {
    function Movie() {
        this.title = '';
        this.synopsis = '';
        this.director = '';
        this.scenario = '';
        this.actor = '';
        this.country = '';
    }
    return Movie;
}());
export { Movie };
//# sourceMappingURL=movies.js.map