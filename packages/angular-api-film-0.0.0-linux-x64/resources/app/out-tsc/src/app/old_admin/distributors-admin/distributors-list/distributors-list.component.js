var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ROLE } from 'app/_models';
import { UserService } from 'app/_services';
import { DistributorsService } from '../distributors.service';
var DistributorsListComponent = /** @class */ (function () {
    function DistributorsListComponent(distrib_services, userService, router, route) {
        var _this = this;
        this.distrib_services = distrib_services;
        this.userService = userService;
        this.router = router;
        this.route = route;
        this.isLogged = false;
        this.displayedColumns = ['distributorname', 'cnc_code', 'contact', 'dcpbay', 'delete'];
        this.dataSource = new MatTableDataSource();
        this.userSubscription = this.userService.user$.subscribe(function (user) {
            _this.isLogged = user ? true : false;
            _this.user = user;
        });
    }
    DistributorsListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.distrib_services
            .getDistributors()
            .subscribe(function (distributors) { return (_this.dataSource.data = distributors); }, function (error) { return (_this.error = error); });
        this.dataSource.paginator = this.paginator;
    }; //Fin ngOnInit
    DistributorsListComponent.prototype.delete = function (distrib) {
        //this.displayedColumns.filter(m => m !== movie);
        this.distrib_services.deleteDistributors(distrib).subscribe();
    }; //Fin delete
    DistributorsListComponent.prototype.getRole = function (user) {
        if (!user || !user.ID) {
            console.debug('No user');
            return;
        }
        return ROLE[user.role];
    };
    __decorate([
        ViewChild(MatPaginator),
        __metadata("design:type", MatPaginator)
    ], DistributorsListComponent.prototype, "paginator", void 0);
    __decorate([
        ViewChild(MatSort),
        __metadata("design:type", MatSort)
    ], DistributorsListComponent.prototype, "sort", void 0);
    DistributorsListComponent = __decorate([
        Component({
            selector: 'app-distributors-list',
            templateUrl: './distributors-list.component.html',
            styleUrls: ['./distributors-list.component.css']
        }),
        __metadata("design:paramtypes", [DistributorsService,
            UserService,
            Router,
            ActivatedRoute])
    ], DistributorsListComponent);
    return DistributorsListComponent;
}()); //Fin
export { DistributorsListComponent };
//# sourceMappingURL=distributors-list.component.js.map