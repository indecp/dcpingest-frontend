var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Movie } from '../movies';
import { MoviesService } from '../movies.service';
import { DistributorsService } from '../../distributors-admin/distributors.service';
import { ROLE } from 'app/_models';
import { UserService } from 'app/_services';
import { ActivatedRoute, Router } from '@angular/router';
var FormMovieComponent = /** @class */ (function () {
    function FormMovieComponent(formBuilder, movieService, distributorsService, http, userService, route, router) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.movieService = movieService;
        this.distributorsService = distributorsService;
        this.http = http;
        this.userService = userService;
        this.route = route;
        this.router = router;
        this.dataSource = new FormData();
        this.isLogged = false;
        this.signMovie = this.createFromGroup();
        this.userSubscription = this.userService.user$.subscribe(function (user) {
            _this.isLogged = user ? true : false;
            _this.user = user;
        });
    }
    FormMovieComponent.prototype.createFromGroup = function () {
        return new FormGroup({
            title: new FormControl(),
            synopsis: new FormControl(),
            director: new FormControl(),
            scenario: new FormControl(),
            actor: new FormControl(),
            country: new FormControl(),
            distributorsList: new FormControl()
        });
    };
    ; //Fin createFromGroup
    FormMovieComponent.prototype.createFormGroupWithBuilder = function (formBuilder) {
        return formBuilder.group({
            title: '',
            synopsis: '',
            director: '',
            scenario: '',
            actor: '',
            country: '',
            distributorsList: ''
        });
    };
    ; //FIN  createFormGroupWithBuilder
    FormMovieComponent.prototype.createFormGroupWithBuilderAndModel = function (formBuilder) {
        return formBuilder.group({
            DataMovie: formBuilder.group(new Movie()),
        });
    }; //FIN  createFormGroupWithBuilderAndMode
    FormMovieComponent.prototype.onSubmit = function () {
        var _this = this;
        this.movieService.addMovie(this.signMovie.value)
            .subscribe({ complete: function () { return _this.dataSource; } });
    }; //FIN onSubmit
    FormMovieComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.distributorsService
            .getDistributors()
            .subscribe(function (distrib) { return (_this.dataSourceDistributors = distrib); });
    }; //Fin ngOnInit
    FormMovieComponent.prototype.getRole = function (user) {
        if (!user || !user.ID) {
            console.debug('No user');
            return;
        }
        return ROLE[user.role];
    };
    FormMovieComponent = __decorate([
        Component({
            selector: 'app-form-movie',
            templateUrl: './movie_new.component.html',
            styleUrls: ['./movie_new.component.css']
        }),
        __metadata("design:paramtypes", [FormBuilder,
            MoviesService,
            DistributorsService,
            HttpClient,
            UserService,
            ActivatedRoute,
            Router])
    ], FormMovieComponent);
    return FormMovieComponent;
}()); //Fin
export { FormMovieComponent };
//# sourceMappingURL=movie_new.component.js.map