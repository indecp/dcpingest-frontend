import { async, TestBed } from '@angular/core/testing';
import { DistributeurDistributeComponent } from './distributeur-distribute.component';
describe('DistributeurDistributeComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [DistributeurDistributeComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(DistributeurDistributeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=distributeur-distribute.component.spec.js.map