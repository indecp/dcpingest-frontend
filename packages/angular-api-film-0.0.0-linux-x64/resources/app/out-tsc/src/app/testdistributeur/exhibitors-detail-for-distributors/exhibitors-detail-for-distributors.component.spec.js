import { async, TestBed } from '@angular/core/testing';
import { ExhibitorsDetailForDistributorsComponent } from './exhibitors-detail-for-distributors.component';
describe('ExhibitorsDetailForDistributorsComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [ExhibitorsDetailForDistributorsComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(ExhibitorsDetailForDistributorsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=exhibitors-detail-for-distributors.component.spec.js.map