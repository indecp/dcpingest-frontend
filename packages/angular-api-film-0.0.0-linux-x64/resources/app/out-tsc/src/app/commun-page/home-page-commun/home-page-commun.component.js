var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { merge } from "rxjs/observable/merge";
import { ActivatedRoute, Router } from '@angular/router';
import { ROLE } from '../../user/user.model';
import { UserService } from '../../user/user.service';
import { MoviesService } from '../../admin/movies-admin/movies.service';
import { MoviesDataSource } from '../../admin/movies-admin/movies.datasource';
import { DcpsService } from '../../admin/dcps-admin/dcps.service';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';
registerLocaleData(localeFr, 'fr-FR', localeFrExtra);
var HomePageCommunComponent = /** @class */ (function () {
    function HomePageCommunComponent(dcpService, movieService, userService, router, route) {
        var _this = this;
        this.dcpService = dcpService;
        this.movieService = movieService;
        this.userService = userService;
        this.router = router;
        this.route = route;
        this.nbMovies = 80;
        this.nbAll = 80;
        this.isLogged = false;
        //  displayedColumns: string[] = ['affiche','id', 'name','date', 'FTR', 'TLR', 'delete','demander'];
        this.displayedColumns = ['affiche', 'demander', 'name', 'date', 'FTR', 'TLR'];
        this.displayedColumns2 = ['affiche', 'id', 'name', 'date', 'FTR', 'TLR', 'delete'];
        this.userSubscription = this.userService.user$.subscribe(function (user) {
            _this.isLogged = user ? true : false;
            _this.user = user;
        });
    }
    HomePageCommunComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataSource = new MoviesDataSource(this.movieService);
        this.dataSource.loadMovies('', 'desc', 1, 50);
        this.dataSource.moviesTotal.subscribe(function (total) { _this.nbMovies = total; });
        /* this.dataSource2 = new MoviesDataSource(this.movieService);
         this.dataSource2.loadMovies('', 'desc', 1, 50),
         this.dataSource2.moviesTotal.subscribe
       ( total => { this.nbAll= total } );
   */
    };
    HomePageCommunComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 1; });
        fromEvent(this.input.nativeElement, 'keyup')
            .pipe(debounceTime(150), distinctUntilChanged(), tap(function () {
            _this.paginator.pageIndex = 0;
            _this.loadMoviesAll();
            _this.loadMoviesPage();
        }))
            .subscribe();
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(tap(function () { return _this.loadMoviesPage(); }))
            .subscribe();
    };
    HomePageCommunComponent.prototype.loadMoviesPage = function () {
        this.dataSource.loadMovies(this.input.nativeElement.value, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
    };
    HomePageCommunComponent.prototype.loadMoviesAll = function () {
        this.dataSource.loadMovies(this.input.nativeElement.value, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
    };
    HomePageCommunComponent.prototype.getRole = function (user) {
        if (!user || !user.ID) {
            console.debug('No user');
            return;
        }
        return ROLE[user.role];
    };
    __decorate([
        ViewChild(MatPaginator),
        __metadata("design:type", MatPaginator)
    ], HomePageCommunComponent.prototype, "paginator", void 0);
    __decorate([
        ViewChild(MatSort),
        __metadata("design:type", MatSort)
    ], HomePageCommunComponent.prototype, "sort", void 0);
    __decorate([
        ViewChild('input'),
        __metadata("design:type", ElementRef)
    ], HomePageCommunComponent.prototype, "input", void 0);
    HomePageCommunComponent = __decorate([
        Component({
            selector: 'app-home-page-commun',
            templateUrl: './home-page-commun.component.html',
            styleUrls: ['./home-page-commun.component.css']
        }),
        __metadata("design:paramtypes", [DcpsService,
            MoviesService,
            UserService,
            Router,
            ActivatedRoute])
    ], HomePageCommunComponent);
    return HomePageCommunComponent;
}());
export { HomePageCommunComponent };
//# sourceMappingURL=home-page-commun.component.js.map