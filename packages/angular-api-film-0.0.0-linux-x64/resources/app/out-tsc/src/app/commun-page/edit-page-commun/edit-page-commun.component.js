var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { MoviesService } from '../../admin/movies-admin/movies.service';
import { Location } from '@angular/common';
import { ROLE } from '../../user/user.model';
import { UserService } from '../../user/user.service';
var EditPageCommunComponent = /** @class */ (function () {
    function EditPageCommunComponent(formBuilder, movieService, userService, http, router, location, route) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.movieService = movieService;
        this.userService = userService;
        this.http = http;
        this.router = router;
        this.location = location;
        this.route = route;
        this.isLogged = false;
        this.movieForm = this.createFromGroup();
        this.userSubscription = this.userService.user$.subscribe(function (user) {
            _this.isLogged = user ? true : false;
            _this.user = user;
        });
    }
    EditPageCommunComponent.prototype.createFromGroup = function () {
        return new FormGroup({
            id: new FormControl(),
            title: new FormControl(),
            synopsis: new FormControl(),
            director: new FormControl(),
            scenario: new FormControl(),
            actor: new FormControl(),
            country: new FormControl(),
        });
    };
    ; //Fin createFromGroup
    EditPageCommunComponent.prototype.goBack = function () {
        this.location.back();
    }; //Fin goBack
    EditPageCommunComponent.prototype.onSubmit = function () {
        var _this = this;
        this.movieService
            .updateMovies(this.movieForm.value)
            .subscribe(function () { return _this.goBack(); });
    }; //Fin onSubmit
    EditPageCommunComponent.prototype.ngOnInit = function () {
        var _this = this;
        var id = +this.route.snapshot.paramMap.get('id');
        this.movieService.getMovie(id)
            .subscribe(function (movie) {
            _this.movieForm.patchValue(movie);
        });
    }; //Fin ngOnInit
    EditPageCommunComponent.prototype.getRole = function (user) {
        if (!user || !user.ID) {
            console.debug('No user');
            return;
        }
        return ROLE[user.role];
    };
    EditPageCommunComponent = __decorate([
        Component({
            selector: 'app-edit-page-commun',
            templateUrl: './edit-page-commun.component.html',
            styleUrls: ['./edit-page-commun.component.css']
        }),
        __metadata("design:paramtypes", [FormBuilder,
            MoviesService,
            UserService,
            HttpClient,
            Router,
            Location,
            ActivatedRoute])
    ], EditPageCommunComponent);
    return EditPageCommunComponent;
}()); //Fin
export { EditPageCommunComponent };
//# sourceMappingURL=edit-page-commun.component.js.map