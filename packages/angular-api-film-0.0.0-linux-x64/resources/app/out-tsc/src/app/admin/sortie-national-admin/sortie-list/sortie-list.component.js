var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { merge } from "rxjs/observable/merge";
import { ActivatedRoute, Router } from '@angular/router';
import { ROLE } from '../../../user/user.model';
import { UserService } from '../../../user/user.service';
import { MoviesService } from '../../movies-admin/movies.service';
import { MoviesDataSource } from '../../movies-admin/movies.datasource';
var SortieListComponent = /** @class */ (function () {
    function SortieListComponent(movieService, userService, router, route) {
        var _this = this;
        this.movieService = movieService;
        this.userService = userService;
        this.router = router;
        this.route = route;
        this.nbMovies = 80;
        this.displayedColumns = ['affiche', 'id', 'name', 'delete'];
        this.isLogged = false;
        this.userSubscription = this.userService.user$.subscribe(function (user) {
            _this.isLogged = user ? true : false;
            _this.user = user;
        });
    }
    SortieListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataSource = new MoviesDataSource(this.movieService);
        this.dataSource.loadMovies('', 'asc', 1, 50);
        this.dataSource.moviesTotal.subscribe(function (total) { _this.nbMovies = total; });
    };
    SortieListComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 1; });
        fromEvent(this.input.nativeElement, 'keyup')
            .pipe(debounceTime(150), distinctUntilChanged(), tap(function () {
            _this.paginator.pageIndex = 0;
            _this.loadMoviesPage();
        }))
            .subscribe();
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(tap(function () { return _this.loadMoviesPage(); }))
            .subscribe();
    };
    SortieListComponent.prototype.loadMoviesPage = function () {
        this.dataSource.loadMovies(this.input.nativeElement.value, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize);
    };
    SortieListComponent.prototype.getRole = function (user) {
        if (!user || !user.ID) {
            console.debug('No user');
            return;
        }
        return ROLE[user.role];
    };
    __decorate([
        ViewChild(MatPaginator),
        __metadata("design:type", MatPaginator)
    ], SortieListComponent.prototype, "paginator", void 0);
    __decorate([
        ViewChild(MatSort),
        __metadata("design:type", MatSort)
    ], SortieListComponent.prototype, "sort", void 0);
    __decorate([
        ViewChild('input'),
        __metadata("design:type", ElementRef)
    ], SortieListComponent.prototype, "input", void 0);
    SortieListComponent = __decorate([
        Component({
            selector: 'app-sortie-list',
            templateUrl: './sortie-list.component.html',
            styleUrls: ['./sortie-list.component.css']
        }),
        __metadata("design:paramtypes", [MoviesService,
            UserService,
            Router,
            ActivatedRoute])
    ], SortieListComponent);
    return SortieListComponent;
}());
export { SortieListComponent };
//# sourceMappingURL=sortie-list.component.js.map