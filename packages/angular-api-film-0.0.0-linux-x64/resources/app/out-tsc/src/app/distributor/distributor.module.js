var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatToolbarModule, MatButtonModule, MatInputModule, MatFormFieldModule, MatTableModule, MatPaginatorModule, MatCardModule, MatSortModule } from '@angular/material';
import { FileSizeModule } from 'ngx-filesize';
import { DistributorHomeComponent } from './distributor-home/distributor-home.component';
import { DistributorComponent } from './distributor/distributor.component';
import { MovieDetailComponent, MovieListComponent, MovieNewComponent } from './movie/';
import { DcpDetailComponent, DcpListComponent, DcpNewComponent } from './dcp/';
import { DistributorRoutingModule } from './distributor-routing.module';
var DistributorModule = /** @class */ (function () {
    function DistributorModule() {
    }
    DistributorModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                MatToolbarModule,
                MatButtonModule,
                MatInputModule,
                MatFormFieldModule,
                MatTableModule,
                MatPaginatorModule,
                DistributorRoutingModule,
                MatSortModule,
                MatCardModule,
                FileSizeModule
            ],
            declarations: [
                DistributorHomeComponent,
                MovieListComponent,
                MovieDetailComponent,
                MovieNewComponent,
                DcpListComponent,
                DcpDetailComponent,
                DcpNewComponent,
                DistributorComponent,
            ]
        })
    ], DistributorModule);
    return DistributorModule;
}());
export { DistributorModule };
//# sourceMappingURL=distributor.module.js.map