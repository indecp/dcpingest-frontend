var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ElementRef, ViewChild, Input } from '@angular/core';
import { Distributor } from '../../_models';
import { DistributorsService } from '../../_services';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
//import { MoviesDataSource }  from '../../admin/movies-admin/movies.datasource' ;
import { MoviesService } from '../../admin/movies-admin/movies.service';
import { ActivatedRoute } from '@angular/router';
var DistributorMoviesComponent = /** @class */ (function () {
    function DistributorMoviesComponent(route, distributorsService, movieService) {
        var _this = this;
        this.route = route;
        this.distributorsService = distributorsService;
        this.movieService = movieService;
        this.displayedColumns = ['affiche', 'name', 'date', 'FTR', 'TLR', 'action'];
        this.dataSource = new MatTableDataSource();
        this.elements = [];
        this.doFilter = function (value) {
            _this.dataSource.filter = value.trim().toLocaleLowerCase();
        };
    }
    DistributorMoviesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataSource.sort = this.sort;
        this.distributorsService.getMovies(this.distributor.id)
            .subscribe(function (movie) { return (_this.dataSource.data = movie); });
        this.getDisributorMovies();
    };
    DistributorMoviesComponent.prototype.ngAfterViewInit = function () {
    };
    DistributorMoviesComponent.prototype.getDisributorMovies = function () {
    };
    __decorate([
        Input(),
        __metadata("design:type", Distributor)
    ], DistributorMoviesComponent.prototype, "distributor", void 0);
    __decorate([
        ViewChild(MatPaginator),
        __metadata("design:type", MatPaginator)
    ], DistributorMoviesComponent.prototype, "paginator", void 0);
    __decorate([
        ViewChild(MatSort),
        __metadata("design:type", MatSort)
    ], DistributorMoviesComponent.prototype, "sort", void 0);
    __decorate([
        ViewChild('input'),
        __metadata("design:type", ElementRef)
    ], DistributorMoviesComponent.prototype, "input", void 0);
    DistributorMoviesComponent = __decorate([
        Component({
            selector: 'app-distributor-movies',
            templateUrl: 'distributor-movies.component.html',
            styleUrls: ['./distributor-movies.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute,
            DistributorsService, MoviesService])
    ], DistributorMoviesComponent);
    return DistributorMoviesComponent;
}());
export { DistributorMoviesComponent };
//# sourceMappingURL=distributor-movies.component.js.map