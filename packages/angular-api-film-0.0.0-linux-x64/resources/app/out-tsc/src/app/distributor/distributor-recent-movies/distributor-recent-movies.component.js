var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { Distributor } from 'app/_models';
import { DistributorsService } from 'app/_services';
var DistributorRecentMoviesComponent = /** @class */ (function () {
    function DistributorRecentMoviesComponent(distributorsService) {
        this.distributorsService = distributorsService;
    }
    DistributorRecentMoviesComponent.prototype.ngOnInit = function () {
        this.getDisributorNewest();
    };
    DistributorRecentMoviesComponent.prototype.getDisributorNewest = function () {
        var _this = this;
        this.distributorsService.getNewest(this.distributor.id)
            .subscribe(function (movies) { return _this.movies = movies; });
    };
    __decorate([
        Input(),
        __metadata("design:type", Distributor)
    ], DistributorRecentMoviesComponent.prototype, "distributor", void 0);
    DistributorRecentMoviesComponent = __decorate([
        Component({
            selector: 'app-distributor-recent-movies',
            templateUrl: './distributor-recent-movies.component.html',
            styleUrls: ['./distributor-recent-movies.component.css']
        }),
        __metadata("design:paramtypes", [DistributorsService])
    ], DistributorRecentMoviesComponent);
    return DistributorRecentMoviesComponent;
}());
export { DistributorRecentMoviesComponent };
//# sourceMappingURL=distributor-recent-movies.component.js.map