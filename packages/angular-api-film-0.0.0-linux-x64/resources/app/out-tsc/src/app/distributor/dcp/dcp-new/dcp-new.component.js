var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { DcpService } from 'app/_services';
import { MessageService } from 'app/message.service';
var DcpNewComponent = /** @class */ (function () {
    function DcpNewComponent(route, router, formBuilder, dcpService, dialogServicse) {
        this.route = route;
        this.router = router;
        this.formBuilder = formBuilder;
        this.dcpService = dcpService;
        this.dialogServicse = dialogServicse;
    }
    DcpNewComponent.prototype.ngOnInit = function () {
        this.createFromGroup();
    };
    DcpNewComponent.prototype.createFromGroup = function () {
        this.dcpForm = this.formBuilder.group({
            //'id': this.formBuilder.control(),
            'name': this.formBuilder.control(''),
            'contentkind': this.formBuilder.control(''),
            'size': this.formBuilder.control(''),
        });
    };
    DcpNewComponent.prototype.handleSubmit = function () {
        console.log(this.dcpForm.value);
    };
    DcpNewComponent.prototype.cancel = function () {
        this.gotoDcps();
    };
    DcpNewComponent.prototype.save = function () {
        var _this = this;
        this.dcpService
            .addDcp(this.dcpForm.value)
            .subscribe(function () { return _this.gotoDcps(); });
    };
    DcpNewComponent.prototype.gotoDcps = function () {
        // Pass along the dcp id if available
        // so that the CrisisListComponent can select that dcp.
        // Add a totally useless `foo` parameter for kicks.
        // Relative navigation back to the  dcps
        this.router.navigate(['../'], { relativeTo: this.route });
    };
    DcpNewComponent = __decorate([
        Component({
            selector: 'app-dcp-new',
            templateUrl: './dcp-new.component.html',
            styleUrls: ['./dcp-new.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute,
            Router,
            FormBuilder,
            DcpService,
            MessageService])
    ], DcpNewComponent);
    return DcpNewComponent;
}());
export { DcpNewComponent };
//# sourceMappingURL=dcp-new.component.js.map