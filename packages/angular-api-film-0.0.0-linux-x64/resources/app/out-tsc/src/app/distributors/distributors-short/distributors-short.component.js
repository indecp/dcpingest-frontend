var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { DistributorsService } from '../distributors.service';
var DistributorsShortComponent = /** @class */ (function () {
    function DistributorsShortComponent(distributorsService) {
        this.distributorsService = distributorsService;
    }
    DistributorsShortComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.distributorsService.getDistributorsContent(this.distributorid)
            .subscribe(function (distributor) { return _this.distributor = distributor; });
    };
    __decorate([
        Input(),
        __metadata("design:type", Number)
    ], DistributorsShortComponent.prototype, "distributorid", void 0);
    DistributorsShortComponent = __decorate([
        Component({
            selector: 'app-distributors-short',
            templateUrl: './distributors-short.component.html',
            styleUrls: ['./distributors-short.component.css']
        }),
        __metadata("design:paramtypes", [DistributorsService])
    ], DistributorsShortComponent);
    return DistributorsShortComponent;
}());
export { DistributorsShortComponent };
//# sourceMappingURL=distributors-short.component.js.map