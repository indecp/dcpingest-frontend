var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { MessageService } from '../message.service';
var httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
var DistributorsService = /** @class */ (function () {
    function DistributorsService(http, message_Service) {
        this.http = http;
        this.message_Service = message_Service;
        this.DistributorsUrl = 'api/tdistributors';
    }
    DistributorsService.prototype.getDistributors = function () {
        var _this = this;
        return this.http
            .get(this.DistributorsUrl)
            .pipe(tap(function (distributors) { return _this.log('fetched distributors'); }), catchError(this.handleError('getDistributors', [])));
    }; //Fin getDistributors
    DistributorsService.prototype.getDistributorsContent = function (id) {
        var _this = this;
        var url = this.DistributorsUrl + "/" + id;
        return this.http.get(url)
            .pipe(tap(function (_) { return _this.log("fetched distrib_s id=" + id); }), catchError(this.handleError("getDistributorsContent id=" + id)));
    }; //Fin getDistributorsContent
    DistributorsService.prototype.searchDistributors = function (term) {
        var _this = this;
        if (!term.trim()) {
            return of([]);
        }
        return this.http.get(this.DistributorsUrl + "/?name=" + term)
            .pipe(tap(function (_) { return _this.log("found distributors matching \"$(term)\""); }), catchError(this.handleError('searchDistributors', [])));
    }; //Fin searchDistributors
    DistributorsService.prototype.addDistributors = function (distrib_s) {
        var _this = this;
        return this.http.post(this.DistributorsUrl, distrib_s, httpOptions)
            .pipe(tap(function (distrib_s) { return _this.log("added distrib_s w/ id=" + distrib_s.id); }), catchError(this.handleError('addDistributors')));
    }; //Fin addDistributors
    DistributorsService.prototype.deleteDistributors = function (distrib_s) {
        var _this = this;
        var id = typeof distrib_s === 'number' ? distrib_s : distrib_s.id;
        var url = this.DistributorsUrl + "/" + id;
        return this.http.delete(url, httpOptions)
            .pipe(tap(function (_) { return _this.log("delete distrib_s id=" + id); }), catchError(this.handleError('deleteDistributors')));
    }; //Fin deleteDistributors
    DistributorsService.prototype.updateDistributors = function (distrib_s) {
        var _this = this;
        return this.http.put(this.DistributorsUrl, distrib_s, httpOptions)
            .pipe(tap(function (_) { return _this.log("updated distrib_s: id=" + distrib_s.id); }), catchError(this.handleError('updateDistributors')));
    }; //Fin updateDistributors
    DistributorsService.prototype.handleError = function (operation, result) {
        var _this = this;
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            //	console.error(error);
            _this.log(operation + " failed: " + error.message);
            return of(result);
        };
    }; //fin handleError
    DistributorsService.prototype.log = function (message) {
        this.message_Service.add('DistributorsService: ${message}');
    }; //Fin log
    DistributorsService = __decorate([
        Injectable({ providedIn: 'root' }),
        __metadata("design:paramtypes", [HttpClient,
            MessageService])
    ], DistributorsService);
    return DistributorsService;
}()); //Fin
export { DistributorsService };
//# sourceMappingURL=distributors.service.js.map