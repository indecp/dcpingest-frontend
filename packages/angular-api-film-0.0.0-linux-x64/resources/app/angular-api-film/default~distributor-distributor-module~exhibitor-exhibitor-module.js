(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~distributor-distributor-module~exhibitor-exhibitor-module"],{

/***/ "./node_modules/rxjs-compat/_esm5/ReplaySubject.js":
/*!*********************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/ReplaySubject.js ***!
  \*********************************************************/
/*! exports provided: ReplaySubject */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ReplaySubject", function() { return rxjs__WEBPACK_IMPORTED_MODULE_0__["ReplaySubject"]; });


//# sourceMappingURL=ReplaySubject.js.map

/***/ }),

/***/ "./src/app/_models/dcp.ts":
/*!********************************!*\
  !*** ./src/app/_models/dcp.ts ***!
  \********************************/
/*! exports provided: Dcp */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Dcp", function() { return Dcp; });
var Dcp = /** @class */ (function () {
    function Dcp() {
    }
    return Dcp;
}());



/***/ }),

/***/ "./src/app/_models/distributor.ts":
/*!****************************************!*\
  !*** ./src/app/_models/distributor.ts ***!
  \****************************************/
/*! exports provided: Distributor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Distributor", function() { return Distributor; });
var Distributor = /** @class */ (function () {
    function Distributor() {
        this.name = '';
        this.contact = '';
        this.cnc_code = '';
        this.isdcpbay = '';
    }
    return Distributor;
}());



/***/ }),

/***/ "./src/app/_models/exhibitor.ts":
/*!**************************************!*\
  !*** ./src/app/_models/exhibitor.ts ***!
  \**************************************/
/*! exports provided: Exhibitor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Exhibitor", function() { return Exhibitor; });
var Exhibitor = /** @class */ (function () {
    function Exhibitor() {
        this.name = '';
        this.valid = '';
        this.contact = '';
        this.iptinc = '';
        this.city = '';
        this.address = '';
    }
    return Exhibitor;
}());



/***/ }),

/***/ "./src/app/_models/index.ts":
/*!**********************************!*\
  !*** ./src/app/_models/index.ts ***!
  \**********************************/
/*! exports provided: Distributor, Exhibitor, Movie, ROLE, User, Dcp */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _distributor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./distributor */ "./src/app/_models/distributor.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Distributor", function() { return _distributor__WEBPACK_IMPORTED_MODULE_0__["Distributor"]; });

/* harmony import */ var _exhibitor__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./exhibitor */ "./src/app/_models/exhibitor.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Exhibitor", function() { return _exhibitor__WEBPACK_IMPORTED_MODULE_1__["Exhibitor"]; });

/* harmony import */ var _movie__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./movie */ "./src/app/_models/movie.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Movie", function() { return _movie__WEBPACK_IMPORTED_MODULE_2__["Movie"]; });

/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user */ "./src/app/_models/user.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ROLE", function() { return _user__WEBPACK_IMPORTED_MODULE_3__["ROLE"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "User", function() { return _user__WEBPACK_IMPORTED_MODULE_3__["User"]; });

/* harmony import */ var _dcp__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dcp */ "./src/app/_models/dcp.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Dcp", function() { return _dcp__WEBPACK_IMPORTED_MODULE_4__["Dcp"]; });








/***/ }),

/***/ "./src/app/_models/movie.ts":
/*!**********************************!*\
  !*** ./src/app/_models/movie.ts ***!
  \**********************************/
/*! exports provided: Movie */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Movie", function() { return Movie; });
var Movie = /** @class */ (function () {
    function Movie() {
        this.title = '';
        this.original_tilte = '';
        this.synopsis = '';
        this.director = '';
        this.scenario = '';
        this.actor = '';
        this.country = '';
    }
    return Movie;
}());



/***/ }),

/***/ "./src/app/_models/user.ts":
/*!*********************************!*\
  !*** ./src/app/_models/user.ts ***!
  \*********************************/
/*! exports provided: ROLE, User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROLE", function() { return ROLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
var ROLE = {
    1: 'admin',
    2: 'distributor',
    3: 'exhibitor'
};
var User = /** @class */ (function () {
    function User(data) {
        Object.assign(this, data);
    }
    User.prototype.isAdministrator = function () {
        return this.role === 1;
    };
    User.prototype.isDistributor = function () {
        return this.role === 2;
    };
    User.prototype.isExhibitor = function () {
        return this.role === 3;
    };
    User.prototype.getRoleTxt = function () {
        return [this.role];
    };
    return User;
}());

;


/***/ }),

/***/ "./src/app/_services/alert.service.ts":
/*!********************************************!*\
  !*** ./src/app/_services/alert.service.ts ***!
  \********************************************/
/*! exports provided: AlertService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return AlertService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AlertService = /** @class */ (function () {
    function AlertService(router) {
        var _this = this;
        this.router = router;
        this.subject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.keepAfterNavigationChange = false;
        // clear alert message on route change
        router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationStart"]) {
                if (_this.keepAfterNavigationChange) {
                    // only keep for a single location change
                    _this.keepAfterNavigationChange = false;
                }
                else {
                    // clear alert
                    _this.subject.next();
                }
            }
        });
    }
    AlertService.prototype.success = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'success', text: message });
    };
    AlertService.prototype.error = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'error', text: message });
    };
    AlertService.prototype.getMessage = function () {
        return this.subject.asObservable();
    };
    AlertService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({ providedIn: 'root' }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AlertService);
    return AlertService;
}());



/***/ }),

/***/ "./src/app/_services/dcp.service.ts":
/*!******************************************!*\
  !*** ./src/app/_services/dcp.service.ts ***!
  \******************************************/
/*! exports provided: DcpService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DcpService", function() { return DcpService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var app_message_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/message.service */ "./src/app/message.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' })
};
var DcpService = /** @class */ (function () {
    function DcpService(http, message_Service) {
        this.http = http;
        this.message_Service = message_Service;
        this.DcpsUrl = 'http://localhost:5000/ingestapi/dcps';
    }
    DcpService.prototype.findDcps = function (filter, sortOrder, pageNumber, pageSize) {
        if (filter === void 0) { filter = ''; }
        if (sortOrder === void 0) { sortOrder = 'asc'; }
        if (pageNumber === void 0) { pageNumber = 1; }
        if (pageSize === void 0) { pageSize = 50; }
        return this.http.get(this.DcpsUrl, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]()
                .set('filter', filter)
                .set('sortOrder', sortOrder)
                .set('pageNumber', pageNumber.toString())
                .set('pageSize', pageSize.toString())
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    }; //Fin
    DcpService.prototype.getDcps = function () {
        var _this = this;
        return this.http
            .get(this.DcpsUrl)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (dcps) { return _this.log('fetched dcps'); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('getDcps', [])));
    }; //Fin getDcps
    DcpService.prototype.getDcp = function (id) {
        var _this = this;
        var url = this.DcpsUrl + "/" + id;
        return this.http.get(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("fetched dcps id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError("getDcps id=" + id)));
    }; //Fin getDcp
    DcpService.prototype.getShortCheck = function () {
        var _this = this;
        return this.http
            .get(this.DcpsUrl)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (check) { return _this.log('fetched check'); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('getShortCheckStatus', [])));
    }; //Fin getShortCheck
    DcpService.prototype.getShortCheckStatus = function (id) {
        var _this = this;
        var url = this.DcpsUrl + "/" + id + "/short_check_status";
        return this.http.get(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("fetched check id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError("getShortCheckStatus id=" + id)));
    }; //Fin getShortCheckStatus
    DcpService.prototype.getLongCheck = function () {
        var _this = this;
        return this.http
            .get(this.DcpsUrl)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (check_long) { return _this.log('fetched check_long'); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('getLongCheckStatus', [])));
    }; //Fin getLongCheck
    /* getLongCheckStatus(id : number):Observable<Dcp>{
        const url = `${this.DcpsUrl}/${id}/hash_verification_state_name`;
        return this.http.get<Dcp>(url)
        .pipe(tap(_=>this.log(`fetched check_long id=${id}`)),
        catchError(this.handleError<Dcp>(`getLongCheckStatus id=${id}`)));
   }//Fin getLongCheckStatus
 */
    DcpService.prototype.getDateCheck = function () {
        var _this = this;
        return this.http
            .get(this.DcpsUrl)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (check_date) { return _this.log('fetched check_date'); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('getDateCheckStatus', [])));
    }; //Fin getLongCheck
    DcpService.prototype.getDateCheckStatus = function (id) {
        var _this = this;
        var url = this.DcpsUrl + "/" + id + "/hash_verification_date";
        return this.http.get(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("fetched check_date id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError("getDateCheckStatus id=" + id)));
    }; //Fin getDateCheckStatus
    DcpService.prototype.getLongCheckStatus = function (id) {
        var _this = this;
        var url = this.DcpsUrl + "/" + id + "/do_check_long";
        return this.http.post(url, '', httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("fetched check_long id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError("getLongCheckStatus id=" + id)));
    };
    DcpService.prototype.getTorrent = function () {
        var _this = this;
        return this.http
            .get(this.DcpsUrl)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (torrent) { return _this.log('fetched torrent'); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('getTheTorrent', [])));
    }; //Fin getTorrent
    DcpService.prototype.getTheTorrent = function (id) {
        var _this = this;
        var url = this.DcpsUrl + "/" + id + "/torrent_creation_state_name";
        return this.http.get(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("fetched torrent id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError("getTheTorrent id=" + id)));
    }; //Fin getTheTorrent
    DcpService.prototype.addDcp = function (dcps_s) {
        var _this = this;
        return this.http.post(this.DcpsUrl, dcps_s, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (dcps_s) { return _this.log("added dcps_s w/ id=" + dcps_s.id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('addDcp')));
    }; //Fin addDcps
    DcpService.prototype.deleteDcps = function (dcps_s) {
        var _this = this;
        var id = typeof dcps_s === 'number' ? dcps_s : dcps_s.id;
        var url = this.DcpsUrl + "/" + id;
        return this.http.delete(url, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("delete dcps_s id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('deleteDcps')));
    }; //Fin deleteDcps
    DcpService.prototype.updateDcp = function (dcp) {
        var _this = this;
        return this.http.put(this.DcpsUrl + ("/" + dcp.id), dcp, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("updated dcp id=" + dcp.id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('updateDcp')));
    }; //Fin updateDcps
    DcpService.prototype.handleError = function (operation, result) {
        var _this = this;
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            console.error(error);
            _this.log(operation + " failed: " + error.message);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(result);
        };
    }; //fin handleError
    DcpService.prototype.log = function (message) {
        this.message_Service.add("DcpService: " + message);
    }; //Fin log
    DcpService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({ providedIn: 'root' }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            app_message_service__WEBPACK_IMPORTED_MODULE_4__["MessageService"]])
    ], DcpService);
    return DcpService;
}()); //Fin



/***/ }),

/***/ "./src/app/_services/distributor.service.ts":
/*!**************************************************!*\
  !*** ./src/app/_services/distributor.service.ts ***!
  \**************************************************/
/*! exports provided: DistributorsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DistributorsService", function() { return DistributorsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var app_message_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/message.service */ "./src/app/message.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' })
};
var DistributorsService = /** @class */ (function () {
    function DistributorsService(http, message_Service) {
        this.http = http;
        this.message_Service = message_Service;
        this.DistributorsUrl = 'http://localhost:5000/ingestapi/distributors';
    }
    DistributorsService.prototype.findMovies = function (filter, sortOrder, pageNumber, pageSize) {
        if (filter === void 0) { filter = ''; }
        if (sortOrder === void 0) { sortOrder = 'asc'; }
        if (pageNumber === void 0) { pageNumber = 1; }
        if (pageSize === void 0) { pageSize = 50; }
        return this.http.get(this.DistributorsUrl, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]()
                .set('filter', filter)
                .set('sortOrder', sortOrder)
                .set('pageNumber', pageNumber.toString())
                .set('pageSize', pageSize.toString())
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    }; //Fin findMovies
    DistributorsService.prototype.getDistributors = function () {
        var _this = this;
        return this.http
            .get(this.DistributorsUrl)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (distributors) { return _this.log('fetched distributors'); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('getDistributors', [])));
    }; //Fin getDistributors
    DistributorsService.prototype.getDistributorsContent = function (id) {
        var _this = this;
        var url = this.DistributorsUrl + "/" + id;
        return this.http.get(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("fetched distrib_s id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError("getDistributorsContent id=" + id)));
    }; //Fin getDistributorsContent
    DistributorsService.prototype.getNewest = function (id) {
        var _this = this;
        var url = this.DistributorsUrl + "/" + id + "/movies_newest";
        return this.http.get(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("fetched distrib_s id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError("getNewest id=" + id)));
    };
    DistributorsService.prototype.getMovies = function (id) {
        var _this = this;
        var url = this.DistributorsUrl + "/" + id + "/movies";
        return this.http.get(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("fetched distrib_s id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError("getMovies id=" + id)));
    };
    DistributorsService.prototype.getDcps = function (id) {
        var _this = this;
        var url = this.DistributorsUrl + "/" + id + "/dcps";
        return this.http.get(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("fetched distrib id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError("getDcps id=" + id)));
    };
    DistributorsService.prototype.searchDistributors = function (term) {
        var _this = this;
        if (!term.trim()) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])([]);
        }
        return this.http.get(this.DistributorsUrl + "/?name=" + term)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("found distributors matching \"$(term)\""); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('searchDistributors', [])));
    }; //Fin searchDistributors
    DistributorsService.prototype.addDistributors = function (distrib_s) {
        var _this = this;
        return this.http.post(this.DistributorsUrl, distrib_s, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (distrib_s) { return _this.log("added distrib_s w/ id=" + distrib_s.name); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('addDistributors')));
    }; //Fin addDistributors
    DistributorsService.prototype.deleteDistributors = function (distrib_s) {
        var _this = this;
        var id = typeof distrib_s === 'number' ? distrib_s : distrib_s.name;
        var url = this.DistributorsUrl + "/" + id;
        return this.http.delete(url, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("delete distrib_s id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('deleteDistributors')));
    }; //Fin deleteDistributors
    DistributorsService.prototype.updateDistributors = function (distrib_s) {
        var _this = this;
        return this.http.put(this.DistributorsUrl, distrib_s, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("updated distrib_s: id=" + distrib_s.name); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('updateDistributors')));
    }; //Fin updateDistributors
    DistributorsService.prototype.handleError = function (operation, result) {
        var _this = this;
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            //  console.error(error);
            _this.log(operation + " failed: " + error.message);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(result);
        };
    }; //fin handleError
    DistributorsService.prototype.log = function (message) {
        this.message_Service.add("DistributorsService: " + message);
    }; //Fin log
    DistributorsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({ providedIn: 'root' }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            app_message_service__WEBPACK_IMPORTED_MODULE_4__["MessageService"]])
    ], DistributorsService);
    return DistributorsService;
}()); //Fin



/***/ }),

/***/ "./src/app/_services/exhibitor.service.ts":
/*!************************************************!*\
  !*** ./src/app/_services/exhibitor.service.ts ***!
  \************************************************/
/*! exports provided: ExhibitorsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExhibitorsService", function() { return ExhibitorsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _message_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../message.service */ "./src/app/message.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' })
};
var ExhibitorsService = /** @class */ (function () {
    function ExhibitorsService(http, message_Service) {
        this.http = http;
        this.message_Service = message_Service;
        this.ExhibitorsUrl = 'http://localhost:5000/ingestapi/exhibitors';
    }
    ExhibitorsService.prototype.getExhibitors = function () {
        var _this = this;
        return this.http
            .get(this.ExhibitorsUrl)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (Exhibitor) { return _this.log('fetched Exhibitor'); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('getExhibitors', [])));
    }; //Fin GetExhibitors
    ExhibitorsService.prototype.getExhibitorsContent = function (id) {
        var _this = this;
        var url = this.ExhibitorsUrl + "/" + id;
        return this.http.get(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("fetched Exhibitors_s id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError("getExhibitors id=" + id)));
    }; //Fin getFilm
    ExhibitorsService.prototype.searchExhibitors = function (term) {
        var _this = this;
        if (!term.trim()) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])([]);
        }
        return this.http.get(this.ExhibitorsUrl + "/?name=" + term)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("found Exhibitor matching \"$(term)\""); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('searchExhibitors', [])));
    }; //Fin searchExhibitorss
    ExhibitorsService.prototype.addExhibitors = function (Exhibitors_s) {
        var _this = this;
        return this.http.post(this.ExhibitorsUrl, Exhibitors_s, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (Exhibitors_s) { return _this.log("added Exhibitors_s w/ id=" + Exhibitors_s.id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('addExhibitors')));
    }; //Fin addExhibitors
    ExhibitorsService.prototype.deleteExhibitors = function (Exhibitors_s) {
        var _this = this;
        var id = typeof Exhibitors_s === 'number' ? Exhibitors_s : Exhibitors_s.id;
        var url = this.ExhibitorsUrl + "/" + id;
        return this.http.delete(url, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("delete Exhibitors_s id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('deleteExhibitors')));
    }; //Fin deleteExhibitors
    ExhibitorsService.prototype.updateExhibitors = function (Exhibitors_s) {
        var _this = this;
        return this.http.put(this.ExhibitorsUrl, Exhibitors_s, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("updated Exhibitors_s id=" + Exhibitors_s.id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('updateExhibitors')));
    }; //Fin updateExhibitorss
    ExhibitorsService.prototype.handleError = function (operation, result) {
        var _this = this;
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            console.error(error);
            _this.log(operation + " failed: " + error.message);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(result);
        };
    }; //fin handleError
    ExhibitorsService.prototype.log = function (message) {
        this.message_Service.add('ExhibitorsService: ${message}');
    }; //Fin log
    ExhibitorsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({ providedIn: 'root' }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _message_service__WEBPACK_IMPORTED_MODULE_4__["MessageService"]])
    ], ExhibitorsService);
    return ExhibitorsService;
}()); //Fin



/***/ }),

/***/ "./src/app/_services/index.ts":
/*!************************************!*\
  !*** ./src/app/_services/index.ts ***!
  \************************************/
/*! exports provided: MovieService, AlertService, DistributorsService, ExhibitorsService, UserService, DcpService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _alert_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./alert.service */ "./src/app/_services/alert.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return _alert_service__WEBPACK_IMPORTED_MODULE_0__["AlertService"]; });

/* harmony import */ var _distributor_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./distributor.service */ "./src/app/_services/distributor.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DistributorsService", function() { return _distributor_service__WEBPACK_IMPORTED_MODULE_1__["DistributorsService"]; });

/* harmony import */ var _exhibitor_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./exhibitor.service */ "./src/app/_services/exhibitor.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ExhibitorsService", function() { return _exhibitor_service__WEBPACK_IMPORTED_MODULE_2__["ExhibitorsService"]; });

/* harmony import */ var _movie_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./movie.service */ "./src/app/_services/movie.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "MovieService", function() { return _movie_service__WEBPACK_IMPORTED_MODULE_3__["MovieService"]; });

/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user.service */ "./src/app/_services/user.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return _user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]; });

/* harmony import */ var _dcp_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dcp.service */ "./src/app/_services/dcp.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DcpService", function() { return _dcp_service__WEBPACK_IMPORTED_MODULE_5__["DcpService"]; });









/***/ }),

/***/ "./src/app/_services/movie.service.ts":
/*!********************************************!*\
  !*** ./src/app/_services/movie.service.ts ***!
  \********************************************/
/*! exports provided: MovieService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MovieService", function() { return MovieService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _message_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../message.service */ "./src/app/message.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' })
};
var MovieService = /** @class */ (function () {
    function MovieService(http, message_Service) {
        this.http = http;
        this.message_Service = message_Service;
        this.MoviesUrl = 'http://localhost:5000/ingestapi/movies';
    }
    MovieService.prototype.findMovies = function (filter, sortOrder, pageNumber, pageSize) {
        if (filter === void 0) { filter = ''; }
        if (sortOrder === void 0) { sortOrder = 'asc'; }
        if (pageNumber === void 0) { pageNumber = 1; }
        if (pageSize === void 0) { pageSize = 50; }
        return this.http.get(this.MoviesUrl, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]()
                .set('filter', filter)
                .set('sortOrder', sortOrder)
                .set('pageNumber', pageNumber.toString())
                .set('pageSize', pageSize.toString())
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) { return res; }));
    }; //Fin findMovies
    //A GARDER
    MovieService.prototype.getMovies = function () {
        var _this = this;
        return this.http.
            get(this.MoviesUrl)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (movies) { return _this.log('fetched movies'); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('getMovies', [])));
    }; //Fin GetMovies
    MovieService.prototype.getMovie = function (id) {
        var _this = this;
        var url = this.MoviesUrl + "/" + id;
        return this.http.get(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("fetched movie_s id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError("getMovies id=" + id)));
    };
    MovieService.prototype.getFTR = function () {
        var _this = this;
        return this.http.
            get(this.MoviesUrl)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (movies) { return _this.log('fetched movies'); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('getFTR', [])));
    };
    MovieService.prototype.getFTRValid = function (id) {
        var _this = this;
        var url = this.MoviesUrl + "/" + id + "/nb_valid_ftr";
        return this.http.get(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("fetched movie_s id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError("getFTR id=" + id)));
    };
    MovieService.prototype.getTLR = function () {
        var _this = this;
        return this.http.
            get(this.MoviesUrl)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (movies) { return _this.log('fetched movies'); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('getTLR', [])));
    };
    MovieService.prototype.getTLRValid = function (id) {
        var _this = this;
        var url = this.MoviesUrl + "/" + id + "/nb_valid_tlr";
        return this.http.get(url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("fetched movie_s id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError("getTLR id=" + id)));
    };
    MovieService.prototype.addMovie = function (movie_s) {
        var _this = this;
        return this.http.post(this.MoviesUrl, movie_s, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (movie_s) { return _this.log("added movie_s w/ id=" + movie_s.id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('addMovie')));
    }; //Fin addMovie
    MovieService.prototype.deleteMovie = function (movie_s) {
        var _this = this;
        var id = typeof movie_s === 'number' ? movie_s : movie_s.id;
        var url = this.MoviesUrl + "/" + id;
        return this.http.delete(url, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("delete movie_s id=" + id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('deleteMovie')));
    }; //Fin deleteMovie
    MovieService.prototype.updateMovies = function (movie_s) {
        var _this = this;
        return this.http.put(this.MoviesUrl, movie_s, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("updated movie_s id=" + movie_s.id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('updateMovies')));
    }; //Fin updateMovies
    MovieService.prototype.updateMovie = function (movie) {
        var _this = this;
        return this.http.put(this.MoviesUrl + ("/" + movie.id), movie, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (_) { return _this.log("updated movie_s id=" + movie.id); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.handleError('updateMovies')));
    }; //Fin updateMovies
    MovieService.prototype.handleError = function (operation, result) {
        var _this = this;
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            console.error(error);
            _this.log(operation + " failed: " + error.message);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(result);
        };
    }; //fin handleError
    MovieService.prototype.log = function (message) {
        this.message_Service.add("MoviesService: " + message);
    }; //Fin log
    MovieService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({ providedIn: 'root' }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _message_service__WEBPACK_IMPORTED_MODULE_4__["MessageService"]])
    ], MovieService);
    return MovieService;
}());



/***/ }),

/***/ "./src/app/_services/user.service.ts":
/*!*******************************************!*\
  !*** ./src/app/_services/user.service.ts ***!
  \*******************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/_models */ "./src/app/_models/index.ts");
/* harmony import */ var rxjs_ReplaySubject__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/ReplaySubject */ "./node_modules/rxjs-compat/_esm5/ReplaySubject.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
        this.user$ = new rxjs_ReplaySubject__WEBPACK_IMPORTED_MODULE_3__["ReplaySubject"](1);
        this.isLogged$ = new rxjs_ReplaySubject__WEBPACK_IMPORTED_MODULE_3__["ReplaySubject"](1);
    }
    UserService.prototype.register = function (user) {
        return this.http.post('http://127.0.0.1:500/users/register', user);
    };
    UserService.prototype.delete = function (id) {
        return this.http.delete("http://127.0.0.1:500/users/" + id);
    };
    UserService.prototype.setAccount = function (user, store) {
        if (store === void 0) { store = true; }
        if (user && !(user instanceof app_models__WEBPACK_IMPORTED_MODULE_2__["User"])) {
            user = new app_models__WEBPACK_IMPORTED_MODULE_2__["User"](user);
        }
        this.user = user;
        this.user$.next(this.user);
    };
    UserService.prototype.getAccount = function () {
        return this.user;
    };
    UserService.prototype.get = function (userId) {
        var url = "http://127.0.0.1:5000/ingestapi/login/" + userId;
        return this.http.get(url, {})
            .toPromise()
            .then(function (data) {
            if (!data) {
                return null;
            }
            return data;
        });
    };
    UserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]])
    ], UserService);
    return UserService;
}());



/***/ })

}]);
//# sourceMappingURL=default~distributor-distributor-module~exhibitor-exhibitor-module.js.map